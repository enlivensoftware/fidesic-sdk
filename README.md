# Fidesic SDK

Fidesic allows you to automatically manage your Accounts Receivable and Accounts Payable processes so you can get back to managing what matters: your business.

## How to use

These instructions will get a copy of the project up and running on your local machine for development and testing purposes.

### Setup

Clone with SSH:

```
git@gitlab.com:enlivensoftware/fidesic-sdk.git
```

Clone with HTTPS:

```
https://gitlab.com/enlivensoftware/fidesic-sdk.git
```

### Usage

The "Sample" project contains both methods to connect to the SDK: by API keys or by your Fidesic username and password. Instructions to create API keys are below.

Debugging through Visual Studio should open the index page with links to samples for both company API key and username/pw.

There should also be a copy of the compiled DLLs in the repo for your usage. To use for your project, you will need the following DLLs:

1. Fidesic.dll

2. DevDefined.OAuth.wcf.dll

3. Newtonsoft.Json.dll

These should be in the "Samples > Includes" directory.

### Create Application API Keys

Application API keys are set in the LoginByAPIController.cs or LoginByUserNameController.cs files in the Samples folder.

When switching between production and staging environments, be sure to change the Environment in FidesicAPISettings in both files mentioned above.

For production application API keys, please contact Fidesic support at support@fidesic.com.

For staging application API keys, these are already set in the repository.

### Create Company API Keys

Log into the Fidesic Account you wish to view API keys from.

Paste this link into your browser Navigation: https://staging.fidesic.com/net/admin/createapikey.aspx

On this page you will have access to both the API Key and Password. For production, replace "staging" with "app" in the URL.

The company API keys should be added to the "agencies" list in the back end of each service in the Samples > Login by API Keys directory.