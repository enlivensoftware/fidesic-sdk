﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Globalization;
using System.Text;

using DotNetNuke;
using DotNetNuke.Common;
using DotNetNuke.Common.Utilities;
using DotNetNuke.Entities.Modules;
using DotNetNuke.Entities.Users;
using DotNetNuke.Services.Tokens;

using Fidesic;
using Fidesic.Data;

namespace Fidesic.Web.Dnn
{
    [Serializable]
    public class FidesicUserInfo : UserInfo, IPropertyAccess
    {
        //------// Properties \\--------------------------------------------\\
        public virtual FidesicUser FidesicUser { get; set; }


        public CacheLevel Cacheability
        {
            get { return CacheLevel.notCacheable; }
        }


        public static FidesicUserInfo Empty
        {
            get
            {
                FidesicUserInfo empty = new FidesicUserInfo();
                empty.Normalize();

                return empty;
            }
        }
        //------\\ Properties //--------------------------------------------//



        //------// Constructors \\------------------------------------------\\
        public FidesicUserInfo()
            : base()
        { }


        public FidesicUserInfo(FidesicUser fidesicUser)
            : base()
        {
            FidesicUser = fidesicUser;
        }
        //------\\ Constructors //------------------------------------------//



        //------// Methods \\-----------------------------------------------\\
        public virtual void Normalize()
        {
            if (FidesicUser == null) { FidesicUser = FidesicUser.Empty; }
        }


        public string GetProperty(string propertyName, string format, CultureInfo formatProvider, UserInfo accessingUser, Scope accessLevel, ref bool propertyNotFound)
        {
            string result = String.Empty;

            switch (propertyName.ToLower())
            {
                case "fidesicuserid":

                    result = (FidesicUser == null ? String.Empty : FidesicUser.UserID.ToString(formatProvider));
                    break;

                case "firstname":

                    result = (FidesicUser == null || FidesicUser.FirstName == null ? String.Empty : FidesicUser.FirstName.ToString(formatProvider));
                    break;

                case "lastname":

                    result = (FidesicUser == null || FidesicUser.LastName == null ? String.Empty : FidesicUser.LastName.ToString(formatProvider));
                    break;

                case "email":

                    result = (FidesicUser == null || FidesicUser.Email == null ? String.Empty : FidesicUser.Email.ToString(formatProvider));
                    break;

                case "emailid":

                    result = (FidesicUser == null || !FidesicUser.EmailID.HasValue ? String.Empty : FidesicUser.EmailID.Value.ToString(formatProvider));
                    break;

                case "phonenumber":

                    result = (FidesicUser == null ? String.Empty : FidesicUser.PhoneNumber.ToString(formatProvider));
                    break;

                case "faxnumber":

                    result = (FidesicUser == null ? String.Empty : FidesicUser.FaxNumber.ToString(formatProvider));
                    break;

                case "address1":

                    result = (FidesicUser == null || FidesicUser.AddressInfo == null ? String.Empty : FidesicUser.AddressInfo.Address1.ToString(formatProvider));
                    break;

                case "address2":

                    result = (FidesicUser == null || FidesicUser.AddressInfo == null ? String.Empty : FidesicUser.AddressInfo.Address2.ToString(formatProvider));
                    break;

                case "address3":

                    result = (FidesicUser == null || FidesicUser.AddressInfo == null ? String.Empty : FidesicUser.AddressInfo.Address3.ToString(formatProvider));
                    break;

                case "city":

                    result = (FidesicUser == null || FidesicUser.AddressInfo == null ? String.Empty : FidesicUser.AddressInfo.City.ToString(formatProvider));
                    break;

                case "state":

                    result = (FidesicUser == null || FidesicUser.AddressInfo == null ? String.Empty : FidesicUser.AddressInfo.State.ToString(formatProvider));
                    break;

                case "zip":

                    result = (FidesicUser == null || FidesicUser.AddressInfo == null ? String.Empty : FidesicUser.AddressInfo.Zip.ToString(formatProvider));
                    break;

                case "country":

                    result = (FidesicUser == null || FidesicUser.AddressInfo == null ? String.Empty : FidesicUser.AddressInfo.Country.ToString(formatProvider));
                    break;

                default:

                    return base.GetProperty(propertyName, format, formatProvider, accessingUser, accessLevel, ref propertyNotFound);
            }


            return result;
        }
        //------\\ Methods //-----------------------------------------------//
    }
}