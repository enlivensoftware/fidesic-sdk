﻿using System;
using System.Data;
using System.Configuration;
using System.Globalization;
using System.Web;

using DotNetNuke;
using DotNetNuke.Common;
using DotNetNuke.Common.Utilities;
using DotNetNuke.Entities.Modules;
using DotNetNuke.Entities.Portals;
using DotNetNuke.Entities.Users;
using DotNetNuke.Services.Tokens;

using Artemis.Data;
using Artemis.Web.Dnn;

using Fidesic.Data;

namespace Fidesic.Web.Dnn
{
    public class InvoiceTokenReplace : TokenReplace, IPropertyAccess
    {
        //------// Properties \\--------------------------------------------\\
        public CacheLevel Cacheability
        {
            get { return CacheLevel.notCacheable; }
        }


        private Invoice _source = null;
        public virtual Invoice Source
        {
            get { return _source; }
            set
            {
                _source = value;
                base.PropertySource["Invoice"] = this;
            }
        }
        //------\\ Properties //--------------------------------------------//



        //------// Constructors \\------------------------------------------\\
        public InvoiceTokenReplace(Invoice invoice)
        {
            Source = invoice;
        }


        public InvoiceTokenReplace(Invoice invoice, int moduleID)
            : base(moduleID)
        {
            Source = invoice;
        }


        public InvoiceTokenReplace(Invoice invoice, Scope accessLevel)
            : base(accessLevel)
        {
            Source = invoice;
        }


        public InvoiceTokenReplace(Invoice invoice, Scope accessLevel, int moduleID)
            : base(accessLevel, moduleID)
        {
            Source = invoice;
        }


        public InvoiceTokenReplace(Invoice invoice, Scope accessLevel, string language, PortalSettings portalSettings, UserInfo user)
            : base(accessLevel, language, portalSettings, user)
        {
            Source = invoice;
        }


        public InvoiceTokenReplace(Invoice invoice, Scope accessLevel, string language, PortalSettings portalSettings, UserInfo user, int moduleID)
            : base(accessLevel, language, portalSettings, user, moduleID)
        {
            Source = invoice;
        }
        //------\\ Constructors //------------------------------------------//



        //------// Methods \\-----------------------------------------------\\
        public string GetProperty(string propertyName, string format, CultureInfo formatProvider, UserInfo accessingUser, Scope accessLevel, ref bool propertyNotFound)
        {
            string result = String.Empty;

            if (Source == null)
            {
                propertyNotFound = true;
                return result;
            }


            switch (propertyName.ToLower())
            {
                case "invoiceid":

                    if (Source.InvoiceID.HasValue)
                    {
                        result = Source.InvoiceID.Value.ToString(format, formatProvider);
                    }
                    else
                    {
                        propertyNotFound = true;
                    }

                    break;

                case "customerid":

                    result = Source.CustomerID.ToString(format, formatProvider);
                    break;

                case "invoicenumber":

                    result = Source.InvoiceNumber.ToString(formatProvider);
                    break;

                case "invoicedate":

                    result = Source.InvoiceDate.ToString(format, formatProvider);
                    break;

                case "term":

                    result = Source.Term.ToString(formatProvider);
                    break;

                case "duedate":

                    result = Source.DueDate.ToString(format, formatProvider);
                    break;

                case "createddate":

                    result = Source.CreatedDate.ToString(format, formatProvider);
                    break;

                case "memo":

                    result = Source.Memo.ToString(formatProvider);
                    break;

                case "status":

                    result = Source.Status.ToString(format, formatProvider);
                    break;

                case "amount":

                    result = Source.Amount.ToString(format, formatProvider);
                    break;

                default:

                    propertyNotFound = true;
                    break;
            }

            return result;
        }
        //------\\ Methods //-----------------------------------------------//
    }
}