﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;

using DotNetNuke.Common;
using DotNetNuke.Common.Lists;
using DotNetNuke.Common.Utilities;
using DotNetNuke.Entities.Modules;
using DotNetNuke.Entities.Portals;
using DotNetNuke.Entities.Profile;
using DotNetNuke.Entities.Users;
using DotNetNuke.Services.Exceptions;

using Fidesic;
using Fidesic.Data;

namespace Fidesic.Web.Dnn
{
    public class FidesicDnnController : FidesicWebController
    {
        //------// Properties \\--------------------------------------------\\
        private ModuleInfo _moduleConfiguration = null;
        protected ModuleInfo ModuleConfiguration
        {
            get
            {
                if (_moduleConfiguration == null) { throw new InvalidOperationException("The instance of " + this.GetType().Name + " is not valid because it was not initialized properly and cannot be used."); }

                return _moduleConfiguration;
            }
            private set { _moduleConfiguration = value; }
        }


        private Hashtable _moduleSettings = null;
        protected Hashtable ModuleSettings
        {
            get
            {
                if (_moduleSettings == null)
                {
                    if (ModuleConfiguration != null)
                    {
                        _moduleSettings = new Hashtable(ModuleConfiguration.TabModuleSettings);

                        foreach (string moduleSettingKey in ModuleConfiguration.ModuleSettings.Keys)
                        {
                            _moduleSettings.Add(moduleSettingKey, ModuleConfiguration.ModuleSettings[moduleSettingKey]);
                        }
                    }
                    else
                    {
                        _moduleSettings = new Hashtable();
                    }
                }

                return _moduleSettings;
            }
        }


        public int PortalID
        {
            get { return ModuleConfiguration.PortalID; }
        }


        public int ModuleID
        {
            get { return ModuleConfiguration.ModuleID; }
        }


        public int TabID
        {
            get { return ModuleConfiguration.TabID; }
        }


        public int TabModuleID
        {
            get { return ModuleConfiguration.TabModuleID; }
        }


        private string _resourceFileRoot;
        public string ResourceFileRoot
        {
            get
            {
                if (_resourceFileRoot == null)
                {
                    _resourceFileRoot = "/DesktopModules/" + ModuleConfiguration.DesktopModule.FolderName + "/" + DotNetNuke.Services.Localization.Localization.LocalResourceDirectory + "/";
                }

                return _resourceFileRoot;
            }
        }


        private string _localResourceFile = null;
        public virtual string LocalResourceFile
        {
            get
            {
                if (_localResourceFile == null)
                {
                    _localResourceFile = ResourceFileRoot + Path.GetFileName(ModuleConfiguration.ModuleControl.ControlSrc) + ".resx";
                }

                return _localResourceFile;
            }
        }


        private FidesicUserInfo _currentUser = null;
        new public virtual FidesicUserInfo CurrentUser
        {
            get
            {
                if (_currentUser == null)
                {
                    _currentUser = GetCurrentUser(false);

                    if (_currentUser == null)
                    {
                        _currentUser = FidesicUserInfo.Empty;
                    }
                }

                return _currentUser;
            }
        }


        public virtual string LoginUrl { get; set; }
        //------\\ Properties //--------------------------------------------//



        //------// Fields \\------------------------------------------------\\
        public delegate UserInfo CreateUserAccount(FidesicUserInfo fidesicUser);
        //------\\ Fields //------------------------------------------------//



        //------// Constructors \\------------------------------------------\\
        public FidesicDnnController(int tabID, int moduleID, FidesicApiSettings apiSettings, bool automaticallyReconnectOnChange)
            : base(apiSettings, automaticallyReconnectOnChange)
        {
            ModuleInfo moduleConfig = new ModuleController().GetModule(tabID, moduleID);
            if (moduleConfig == null) { throw new ArgumentException("The provided TabID and ModuleID are invalid."); }

            ModuleConfiguration = moduleConfig;
        }


        public FidesicDnnController(int tabID, int moduleID, FidesicApiSettings apiSettings)
            : this(tabID, moduleID, apiSettings, true)
        { }


        public FidesicDnnController(ModuleInfo moduleConfiguration, FidesicApiSettings apiSettings, bool automaticallyReconnectOnChange)
            : base(apiSettings, automaticallyReconnectOnChange)
        {
            if (moduleConfiguration == null) { throw new ArgumentNullException("moduleConfiguration"); }

            ModuleConfiguration = moduleConfiguration;
        }


        public FidesicDnnController(ModuleInfo moduleConfiguration, FidesicApiSettings apiSettings)
            : this(moduleConfiguration, apiSettings, true)
        { }
        //------\\ Constructors //------------------------------------------//



        //------// Methods \\-----------------------------------------------\\
        new public virtual FidesicUserInfo GetCurrentUser()
        {
            bool validUserRequired = false;

            if (base.ConnectedToFidesic)
            {
                validUserRequired = true;
            }

            return GetCurrentUser(validUserRequired);
        }


        public virtual FidesicUserInfo GetCurrentUser(bool validUserRequired)
        {
            FidesicUserInfo currentUser = null;

            try
            {
                FidesicUser userObj = base.GetCurrentUser();
                if (userObj == null) { return null; }

                return GetDnnUserFromFidesicUser(userObj);

            }
            catch (FidesicException exception)
            {
                HandleException(exception);
                currentUser = null;
            }

            return currentUser;
        }


        /*
        public virtual FidesicUserInfo GetCurrentUser(string accessToken, bool validUserRequired)
        {
            FidesicUserInfo currentUser = null;

            try
            {                
                


                currentUser = FidesicUserInfo.Empty;
                currentUser.Inflate((IDictionary<string, object>)client.Get("me"));

                return currentUser;
            }
            catch (FacebookOAuthException exception)
            {
                Exceptions.LogException(exception);

                DeleteFacebookAuthCookie(validUserRequired);
                currentUser = null;
            }

            return currentUser;
        }
        */


        public static FidesicUserInfo GetDnnUserFromFidesicUser(FidesicUser fidesicUser)
        {
            if (fidesicUser == null) { throw new ArgumentNullException("fidesicUser"); }

            FidesicUserInfo dnnUser = new FidesicUserInfo(fidesicUser);

            dnnUser.FirstName = fidesicUser.FirstName;
            dnnUser.LastName = fidesicUser.LastName;
            dnnUser.DisplayName = fidesicUser.FirstName + " " + fidesicUser.LastName;
            dnnUser.Email = fidesicUser.Email;
            dnnUser.Username = dnnUser.Email;

            if (fidesicUser.AddressInfo != null)
            {
                dnnUser.Profile.Street = fidesicUser.AddressInfo.Address1;
                dnnUser.Profile.Unit = fidesicUser.AddressInfo.Address2;
                dnnUser.Profile.City = fidesicUser.AddressInfo.City;
                dnnUser.Profile.Region = fidesicUser.AddressInfo.State;
                dnnUser.Profile.PostalCode = fidesicUser.AddressInfo.Zip;
                dnnUser.Profile.Country = fidesicUser.AddressInfo.Country;
            }

            return dnnUser;
        }


        new protected virtual void LoginUser(UserInfo currentUser)
        {
            UserController.UserLogin(PortalID, currentUser, PortalController.GetCurrentPortalSettings().PortalName, HttpContext.Current.Request.UserHostAddress, true);
            ClearFidesicSessionStorage();
        }


        protected override void LogoutCurrentUser()
        {
            new DotNetNuke.Security.PortalSecurity().SignOut();
            HttpContext.Current.Response.Redirect(Globals.NavigateURL(PortalSettings.Current.LoginTabId), false);
        }


        protected override string GetCurrentUsername()
        {
            UserInfo currentDnnUser = GetCurrentDnnUser();

            if (currentDnnUser != null)
            {
                return currentDnnUser.Username;
            }

            return null;
        }


        protected virtual UserInfo GetCurrentDnnUser()
        {
            UserInfo currentDnnUser = null;

            if (HttpContext.Current != null && HttpContext.Current.Items["Artemis.Web.Dnn.DnnUtils.CurrentUser"] == null)
            {
                currentDnnUser = UserController.GetCurrentUserInfo();
            }
            else if (HttpContext.Current == null)
            {
                return UserController.GetCurrentUserInfo();
            }


            HttpContext.Current.Items["Artemis.Web.Dnn.DnnUtils.CurrentUser"] = currentDnnUser;
            return currentDnnUser;
        }


        protected override string DetermineEmailFromEnvironment()
        {
            UserInfo currentDnnUser = GetCurrentDnnUser();
            if (currentDnnUser == null) { return null; }

            return currentDnnUser.Email;
        }


        protected override string DetermineCompanyNameFromEnvironment()
        {
            UserInfo currentDnnUser = GetCurrentDnnUser();
            if (currentDnnUser == null) { return null; }

            return currentDnnUser.Profile.GetPropertyValue("CompanyName");
        }


        protected override Exception HandleException(Exception exception)
        {
            Exceptions.LogException(exception);

            if (exception is FidesicSessionExpiredException)
            {
                LogoutCurrentUser();
            }

            return base.HandleException(exception);
        }
        //------\\ Methods //-----------------------------------------------//
    }
}