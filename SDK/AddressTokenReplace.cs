﻿using System;
using System.Data;
using System.Configuration;
using System.Globalization;
using System.Web;

using DotNetNuke;
using DotNetNuke.Common;
using DotNetNuke.Common.Utilities;
using DotNetNuke.Entities.Modules;
using DotNetNuke.Entities.Portals;
using DotNetNuke.Entities.Users;
using DotNetNuke.Services.Tokens;

using Artemis.Data;
using Artemis.Web.Dnn;

using Fidesic.Data;

namespace Fidesic.Web.Dnn
{
    public class AddressTokenReplace : TokenReplace, IPropertyAccess
    {
        //------// Properties \\--------------------------------------------\\
        public CacheLevel Cacheability
        {
            get { return CacheLevel.notCacheable; }
        }


        private Address _source = null;
        public virtual Address Source
        {
            get { return _source; }
            set
            {
                _source = value;
                base.PropertySource["Address"] = this;
            }
        }
        //------\\ Properties //--------------------------------------------//



        //------// Constructors \\------------------------------------------\\
        public AddressTokenReplace(Address address)
        {
            Source = address;
        }


        public AddressTokenReplace(Address address, int moduleID)
            : base(moduleID)
        {
            Source = address;
        }


        public AddressTokenReplace(Address address, Scope accessLevel)
            : base(accessLevel)
        {
            Source = address;
        }


        public AddressTokenReplace(Address address, Scope accessLevel, int moduleID)
            : base(accessLevel, moduleID)
        {
            Source = address;
        }


        public AddressTokenReplace(Address address, Scope accessLevel, string language, PortalSettings portalSettings, UserInfo user)
            : base(accessLevel, language, portalSettings, user)
        {
            Source = address;
        }


        public AddressTokenReplace(Address address, Scope accessLevel, string language, PortalSettings portalSettings, UserInfo user, int moduleID)
            : base(accessLevel, language, portalSettings, user, moduleID)
        {
            Source = address;
        }
        //------\\ Constructors //------------------------------------------//



        //------// Methods \\-----------------------------------------------\\
        public string GetProperty(string propertyName, string format, CultureInfo formatProvider, UserInfo accessingUser, Scope accessLevel, ref bool propertyNotFound)
        {
            string result = String.Empty;

            if (Source == null)
            {
                propertyNotFound = true;
                return result;
            }


            switch (propertyName.ToLower())
            {
                case "addressid":

                    result = Source.AddressID.ToString(format, formatProvider);
                    break;

                case "address1":

                    result = Source.Address1.ToString(formatProvider);
                    break;

                case "address2":

                    result = Source.Address2.ToString(formatProvider);
                    break;

                case "address3":

                    result = Source.Address3.ToString(formatProvider);
                    break;

                case "city":

                    result = Source.City.ToString(formatProvider);
                    break;

                case "state":

                    result = Source.State.ToString(formatProvider);
                    break;

                case "zip":

                    result = Source.Zip.ToString(formatProvider);
                    break;

                case "country":

                    result = Source.Country.ToString(formatProvider);
                    break;

                case "type":

                    result = Source.Type.ToString(formatProvider);
                    break;

                default:

                    propertyNotFound = true;
                    break;
            }

            return result;
        }
        //------\\ Methods //-----------------------------------------------//
    }
}