﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;

using DevDefined.OAuth.Framework;

namespace Fidesic
{
    public static class Utils
    {
        //------// Properties \\--------------------------------------------\\

        //------\\ Properties //--------------------------------------------//



        //------// Methods \\-----------------------------------------------\\
        public static string BuildQueryString(Dictionary<string, string> parameters)
        {
            if (parameters == null || parameters.Count == 0) { return String.Empty; }

            StringBuilder resourcePath = new StringBuilder();
            resourcePath.Append('?');

            int parameterCounter = 1;
            foreach (KeyValuePair<string, string> parameter in parameters)
            {
                resourcePath.Append(parameter.Key).Append('=').Append(HttpUtility.UrlEncode(parameter.Value));

                if (parameterCounter < parameters.Count)
                {
                    resourcePath.Append('&');
                }

                parameterCounter++;
            }

            return resourcePath.ToString();
        }


        public static string BuildOAuthHttpBodyContent(Dictionary<string, object> contents)
        {
            if (contents == null || contents.Count == 0) { return String.Empty; }

            StringBuilder bodyContent = new StringBuilder();

            int itemCounter = 1;
            foreach (KeyValuePair<string, object> item in contents)
            {
                bodyContent.Append(item.Key).Append('=').Append(HttpUtility.UrlEncode(item.Value.ToString()));

                if (itemCounter < contents.Count)
                {
                    bodyContent.Append('&');
                }

                itemCounter++;
            }

            return bodyContent.ToString();
        }


        public static string BuildRequestUrl(string baseUrl, string operationName, Dictionary<string, string> parameters)
        {
            string resourcePath = Utils.BuildQueryString(parameters);

            StringBuilder uri = new StringBuilder();
            uri.Append(baseUrl.Trim('/', ' ')).Append('/').Append(operationName).Append(resourcePath);

            return uri.ToString();
        }


        public static Uri BuildRequestUri(Uri baseUri, string operationName, Dictionary<string, string> parameters)
        {
            return new Uri(BuildRequestUrl(baseUri.ToString(), operationName, parameters));
        }


        public static Uri BuildRequestUri(string baseUrl, string operationName, Dictionary<string, string> parameters)
        {
            return new Uri(BuildRequestUrl(baseUrl, operationName, parameters));
        }


        public static HttpWebRequest GenerateHttpRequest(string requestUrl, string httpMethod, int timeout, Uri proxyServerUri)
        {
            HttpWebRequest apiRequest = (HttpWebRequest)WebRequest.Create(requestUrl);
            apiRequest.Timeout = timeout;
            apiRequest.Method = httpMethod;
            apiRequest.ContentType = Parameters.HttpFormEncoded;

            if (proxyServerUri != null)
            {
                apiRequest.Proxy = new WebProxy(proxyServerUri);
            }

            return apiRequest;
        }
        //------\\ Methods //-----------------------------------------------//
    }
}
