﻿using System;
using System.Data;
using System.Configuration;
using System.Globalization;
using System.Web;

using DotNetNuke;
using DotNetNuke.Common;
using DotNetNuke.Common.Utilities;
using DotNetNuke.Entities.Modules;
using DotNetNuke.Entities.Portals;
using DotNetNuke.Entities.Users;
using DotNetNuke.Services.Tokens;

using Artemis.Data;
using Artemis.Web.Dnn;

using Fidesic.Data;

namespace Fidesic.Web.Dnn
{
    public class CustomerTokenReplace : TokenReplace, IPropertyAccess
    {
        //------// Properties \\--------------------------------------------\\
        public CacheLevel Cacheability
        {
            get { return CacheLevel.notCacheable; }
        }


        private Customer _source = null;
        public virtual Customer Source
        {
            get { return _source; }
            set
            {
                _source = value;
                base.PropertySource["Customer"] = this;
            }
        }
        //------\\ Properties //--------------------------------------------//



        //------// Constructors \\------------------------------------------\\
        public CustomerTokenReplace(Customer customer)
        {
            Source = customer;
        }


        public CustomerTokenReplace(Customer customer, int moduleID)
            : base(moduleID)
        {
            Source = customer;
        }


        public CustomerTokenReplace(Customer customer, Scope accessLevel)
            : base(accessLevel)
        {
            Source = customer;
        }


        public CustomerTokenReplace(Customer customer, Scope accessLevel, int moduleID)
            : base(accessLevel, moduleID)
        {
            Source = customer;
        }


        public CustomerTokenReplace(Customer customer, Scope accessLevel, string language, PortalSettings portalSettings, UserInfo user)
            : base(accessLevel, language, portalSettings, user)
        {
            Source = customer;
        }


        public CustomerTokenReplace(Customer customer, Scope accessLevel, string language, PortalSettings portalSettings, UserInfo user, int moduleID)
            : base(accessLevel, language, portalSettings, user, moduleID)
        {
            Source = customer;
        }
        //------\\ Constructors //------------------------------------------//



        //------// Methods \\-----------------------------------------------\\
        public string GetProperty(string propertyName, string format, CultureInfo formatProvider, UserInfo accessingUser, Scope accessLevel, ref bool propertyNotFound)
        {
            string result = String.Empty;

            if (Source == null)
            {
                propertyNotFound = true;
                return result;
            }


            switch (propertyName.ToLower())
            {
                case "customernumber":

                    result = Source.CustomerNumber.ToString(formatProvider);
                    break;

                case "contacttype":

                    result = Source.ContactType.ToString(formatProvider);
                    break;

                case "userid":

                    result = Source.UserID.ToString(format, formatProvider);
                    break;

                case "companyid":

                    if (Source.CompanyID.HasValue)
                    {
                        result = Source.CompanyID.Value.ToString(format, formatProvider);
                    }
                    break;

                case "firstname":

                    result = Source.FirstName.ToString(formatProvider);
                    break;

                case "lastname":

                    result = Source.LastName.ToString(formatProvider);
                    break;

                case "email":

                    result = Source.Email.ToString(formatProvider);
                    break;

                case "emailid":

                    if (Source.EmailID.HasValue)
                    {
                        result = Source.EmailID.Value.ToString(format, formatProvider);
                    }
                    break;

                case "defaultdepositaccountid":

                    if (Source.DefaultDepositAccountID.HasValue)
                    {
                        result = Source.DefaultDepositAccountID.Value.ToString(format, formatProvider);
                    }
                    break;

                case "defaultpaymentaccountid":

                    if (Source.DefaultPaymentAccountID.HasValue)
                    {
                        result = Source.DefaultPaymentAccountID.Value.ToString(format, formatProvider);
                    }
                    break;

                case "companyname":

                    result = Source.CompanyName.ToString(formatProvider);
                    break;

                case "faxnumber":

                    result = Source.FaxNumber.ToString(formatProvider);
                    break;

                case "phonenumber":

                    result = Source.PhoneNumber.ToString(formatProvider);
                    break;

                case "iscompany":

                    result = Source.IsCompany.ToString(formatProvider);
                    break;

                default:

                    propertyNotFound = true;
                    break;
            }

            return result;
        }
        //------\\ Methods //-----------------------------------------------//
    }
}