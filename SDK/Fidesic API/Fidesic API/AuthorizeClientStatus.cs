﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Fidesic
{
    public enum AuthorizeClientStatus : int
    {
        Unknown = 0,
        Failed = 1,
        Succeeded = 2,
        RequiredClientRedirection = 3,
        AlreadyAuthorized = 4
    }
}