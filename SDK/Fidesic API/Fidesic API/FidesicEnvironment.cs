﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Fidesic
{
    public enum FidesicEnvironment
    {
        Development,
        Staging,
        Live,
        /// <summary>
        /// Allow base URLs to be overridden.
        /// </summary>
        Custom
    }
}