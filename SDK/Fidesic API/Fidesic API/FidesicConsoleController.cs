﻿using System;
using System.Net;
using System.Collections.Generic;
using System.Web;
using System.Linq;
using DevDefined.OAuth;
using DevDefined.OAuth.Consumer;
using DevDefined.OAuth.Framework;

namespace Fidesic.Web
{
    public abstract class FidesicConsoleController : FidesicController
    {
        //------// Properties \\--------------------------------------------\\
        private HttpContext _theContext;
        public HttpContext theContext
        {
            get
            {
                if (_theContext == null)
                {
                    _theContext = HttpContext.Current;
                }
                return _theContext;
            }
        }
        private string cacheToken = "FidesicSDKToken";

        public override bool ConnectedToFidesic
        {
            get
            {
                try
                {
                    return FidesicAuthCookieExists() || CurrentUser != null;
                }
                catch
                {
                    return false;
                }
            }
        }

        public virtual bool LoggedInToEnvironment
        {
            get { return (theContext != null && theContext.Request != null && theContext.Request.IsAuthenticated); }
        }

        protected virtual string FidesicSessionKey
        {
            get { return "FidesicSession.CurrentUser"; }
        }
        //------\\ Properties //--------------------------------------------//


        //------// Constructors \\------------------------------------------\\
        public FidesicConsoleController(FidesicApiSettings apiSettings)
            : base(apiSettings)
        { }


        public FidesicConsoleController(FidesicApiSettings apiSettings, bool automaticallyReconnectOnChange)
            : base(apiSettings, automaticallyReconnectOnChange)
        { }
        //------\\ Constructors //------------------------------------------//



        //------// Methods \\-----------------------------------------------\\
        protected virtual void PersistFidesicAuthCookie(IToken accessToken, long expires, string username)
        {
            IToken authCookie = (IToken)GetFromCache(cacheToken);
            
            if (authCookie == null)
            {
                SetInCache(cacheToken, accessToken);
            }
        }

        protected virtual bool FidesicAuthCookieExists()
        {
            object authToken = GetFromCache(cacheToken);
            if (authToken != null && !string.IsNullOrEmpty(((IToken)authToken).Token))
            {
                return true;
            }

            return false;
        }

        protected virtual bool FidesicAuthCookieExists(out string accessToken)
        {
            IToken authToken = (IToken)GetFromCache(cacheToken);
            if (authToken != null && !string.IsNullOrEmpty(authToken.Token))
            {
                accessToken = authToken.Token;
                return true;
            }

            accessToken = null;
            return false;
        }


        protected virtual void DeleteFidesicAuthCookie()
        {
            SetInCache(cacheToken, null);
            LogoutCurrentUser();     
        }


        public virtual void DisconnectFromFidesic(bool logoutUser)
        {
            DisconnectFromFidesic();

            if (logoutUser)
            {
                LogoutCurrentUser();
            }
        }


        public override void DisconnectFromFidesic()
        {
            WebClient = null;
            Session = null;

            ClearSessionFromEnvironment();
            try
            {
                if (FidesicAuthCookieExists())
                {
                    DeleteFidesicAuthCookie();
                }
            }
            catch { }

            ClearFidesicSessionStorage();
        }

        protected virtual void LoginUser(string currentUsername)
        {
            System.Web.Security.FormsAuthentication.SetAuthCookie(currentUsername, false);
        }


        protected virtual void LogoutCurrentUser()
        {
            System.Web.Security.FormsAuthentication.SignOut();
        }


        protected virtual string GetCurrentUsername()
        {
            if (theContext.User != null && theContext.User.Identity != null)
            {
                return theContext.User.Identity.Name;
            }

            return null;
        }


        protected override void ProvisionSessionForRequest(bool isProtectedResource = true)
        {
            try
            {
                base.ProvisionSessionForRequest(isProtectedResource);

                if (Session == null && LoggedInToEnvironment)
                {
                    DisconnectFromFidesic(false); // Disconnect but don't logout.
                }
            }
            catch (System.Exception exception)
            {
                DisconnectFromFidesic(true);
                throw exception;
            }
        }

        protected override bool RestoreSessionFromEnvironment()
        {
            return false;
        }

        protected override void PersistSessionWithinEnvironment()
        {
        }

        protected virtual void ClearFidesicSessionStorage()
        {
            SetInCache(cacheToken, null);
        }


        protected override string DetermineAccessTokenFromEnvironment()
        {
            string accessToken = null;

            if (!FidesicAuthCookieExists(out accessToken))
            {
                return null;
            }

            return accessToken;
        }

        protected override System.Exception HandleException(System.Exception exception)
        {
            if (exception is OAuthException)
            {
                theContext.Session.Remove(FidesicSession_CacheKey);
            }


            return base.HandleException(exception);
        }

        #region Caching
        private object GetFromCache(string token)
        {
            try
            {
                return theContext.Cache.Get(token);
            }
            catch
            {
                return null;
            }
        }
        private bool SetInCache(string token, object theObject)
        {
            bool result = false;
            try
            {
                var x = theContext.Cache.Get(token);
                if (x != null && x != theObject)
                {
                    theContext.Cache.Remove(token);
                    theContext.Cache.Insert(token, theObject, null, 
                        System.Web.Caching.Cache.NoAbsoluteExpiration, TimeSpan.FromMinutes(20));
                }
                result = true;
            }
            catch(Exception ex)
            {
                return false;
            }
            return result;
        }
        #endregion
        //------\\ Methods //-----------------------------------------------//

    }
}