﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Fidesic
{
    [Serializable]
    public class AuthorizeClientResult
    {
        //------// Properties \\--------------------------------------------\\
        public virtual string AuthorizationUrl { get; set; }
        public virtual string VerificationCode { get; set; }
        public virtual bool Authorized { get; set; }


        private AuthorizeClientStatus _status = AuthorizeClientStatus.Unknown;
        public virtual AuthorizeClientStatus Status
        {
            get { return _status; }
            set { _status = value; }
        }


        private string _message = null;
        public virtual string Message
        {
            get
            {
                if (_message == null)
                {
                    return Status.ToString();
                }

                return _message;
            }
            set { _message = value; }
        }
        //------\\ Properties //--------------------------------------------//



        //------// Constructors \\------------------------------------------\\
        public AuthorizeClientResult()
        { }


        public AuthorizeClientResult(bool alreadyAuthorized)
        {
            Authorized = alreadyAuthorized;

            if (alreadyAuthorized)
            {
                Status = AuthorizeClientStatus.AlreadyAuthorized;
            }
        }
        //------\\ Constructors //------------------------------------------//



        //------// Methods \\-----------------------------------------------\\

        //------\\ Methods //-----------------------------------------------//
    }
}