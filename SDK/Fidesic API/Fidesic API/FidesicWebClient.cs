﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;

using DevDefined.OAuth;
using DevDefined.OAuth.Consumer;
using DevDefined.OAuth.Framework;
using DevDefined.OAuth.Utility;

namespace Fidesic
{
    public class FidesicWebClient
    {
        //------// Properties \\--------------------------------------------\\
        private FidesicApiSettings _settings = new FidesicApiSettings();
        public virtual FidesicApiSettings Settings
        {
            get { return _settings; }
            set { _settings = value; }
        }


        private IOAuthConsumerContext _consumerContext = null;
        public virtual IOAuthConsumerContext ConsumerContext
        {
            get { return _consumerContext; }
            set { _consumerContext = value; }
        }


        private IOAuthSession _oAuthSession = null;
        protected virtual IOAuthSession OAuthSession
        {
            get { return _oAuthSession; }
            set { _oAuthSession = value; }
        }
        //------\\ Properties //--------------------------------------------//



        //------// Fields \\------------------------------------------------\\
        protected static Regex EmailValidateRegex = new Regex(@"\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*", RegexOptions.IgnoreCase);
        //------\\ Fields //------------------------------------------------//



        //------// Constructors \\------------------------------------------\\
        public FidesicWebClient(string apiKey, string secretKey)
        {
            Settings.ApiKey = apiKey;
            Settings.SecretKey = secretKey;

            ValidateApiSettings(Settings);
        }


        public FidesicWebClient(string apiKey, string secretKey, bool consumerIsTrusted)
            : this(apiKey, secretKey)
        {
            Settings.ConsumerIsTrusted = consumerIsTrusted;
        }


        public FidesicWebClient(FidesicApiSettings settings)
        {
            ValidateApiSettings(settings);
            Settings = settings;
        }
        //------\\ Constructors //------------------------------------------//



        //------// Methods \\-----------------------------------------------\\
        protected virtual IOAuthConsumerContext CreateOAuthConsumerContext()
        {
            return new OAuthConsumerContext()
            {
                ConsumerKey = Settings.ApiKey,
                ConsumerSecret = Settings.SecretKey,
                SignatureMethod = SignatureMethod.HmacSha1,
                UseHeaderForOAuthParameters = true
            };
        }


        protected virtual IOAuthSession CreateOAuthSession(string companyName)
        {
            IOAuthSession oAuthSession = new OAuthSession(ConsumerContext, Settings.RequestTokenUrl, Settings.AuthorizeUrl, Settings.AccessTokenUrl).WithQueryParameters(new { scope = companyName ?? String.Empty });
            
            if (!String.IsNullOrWhiteSpace(Settings.ProxyServerUrl))
            {
                UriBuilder proxyServerUri = new UriBuilder(Settings.ProxyServerUrl);
                proxyServerUri.Port = Settings.ProxyServerPort;

                oAuthSession.ProxyServerUri = proxyServerUri.Uri;
            }

            return oAuthSession;
        }


        // Provisions the session between the Consumer and the API alone. This allows for the Consumer to communicate and access non-user resources that 
        // are accessible to it.
        protected virtual void ProvisionConsumerSession(FidesicSession session, bool switchAccount = false)
        {
            if (ConsumerContext == null)
            {
                ConsumerContext = CreateOAuthConsumerContext();
            }

            if ((switchAccount || OAuthSession == null) && ConsumerContext != null)
            {
                OAuthSession = CreateOAuthSession(session.CompanyName);
                OAuthSession.AccessToken = session.AccessToken;
            }


            // Connect the Consumer (this application that is accessing the 3rd party API on behalf of the user) to the API 
            // by retrieving a Request Token. The Request Token authenticates and authorizes the consumer with access to the API.
            AuthorizeConsumer(session);
        }


        // Meant to be called by GET, POST, PUT and DELETE methods to ensure the FidesicSession in its entirety is valid and has been provisioned. 
        protected internal virtual void ValidateAndProvisionFidesicSession(FidesicSession session, bool isProtectedResource)
        {
                ValidateFidesicSession(session);

                if (session.IsDirty)
                {
                    ConsumerContext = null;
                    OAuthSession = null;
                    session.RequestToken = null;
                    session.AccessToken = null;

                    session.IsDirty = false;
                }


                ProvisionConsumerSession(session);

            // Authorization and retrieval of the Access Token can be skipped if the resource of the current request is not protected.
            if (isProtectedResource)
            {
                AuthorizeClientResult authorizeResult = AuthorizeUser(session, null);

                if (authorizeResult.Status == AuthorizeClientStatus.RequiredClientRedirection)
                {
                    // If within a web context...
                    if (HttpContext.Current != null)
                    {
                        HttpContext.Current.Response.Redirect(authorizeResult.AuthorizationUrl, false);
                    }

                    throw new RedirectedForAuthorizationException("The user is required to authorize their access.");
                }
                else if (authorizeResult.Status == AuthorizeClientStatus.Failed)
                {
                    throw new OAuthException(authorizeResult.Message, null);
                }


                if (session.AccessToken == null && session.Authorized)
                {
                    session.AccessToken = OAuthSession.ExchangeRequestTokenForAccessToken(session.RequestToken, session.VerificationCode);
                }
            }
        }


        protected virtual void ValidateFidesicSession(FidesicSession session)
        {
            if (session == null)
            {
                throw new FidesicException("A valid, non-null FidesicSession object is required to perform operations against the Fidesic API.");
            }

            //ValidateEmailAddress(session.Email);
        }


        protected virtual void ValidateEmailAddress(string emailAddress)
        {
            if (String.IsNullOrWhiteSpace(emailAddress))
            {
                throw new FidesicException("A non-empty email address is required.");
            }


            if (!EmailValidateRegex.IsMatch(emailAddress))
            {
                throw new FidesicException("A valid email address is required.");
            }
        }


        protected virtual void ValidateCompanyName(string companyName, bool isRequired)
        {
            if (String.IsNullOrWhiteSpace(companyName) && isRequired)
            {
                throw new FidesicException("A non-empty company name is required.");
            }
        }


        protected virtual void ValidateApiSettings(FidesicApiSettings settings)
        {
            if (string.IsNullOrWhiteSpace(settings.ApiKey))
                throw new Exception("ApiKey cannot be empty.");
            if (string.IsNullOrWhiteSpace(settings.SecretKey))
                throw new Exception("API Password cannot be empty.");
            if (string.IsNullOrWhiteSpace(settings.RequestTokenUrl))
                throw new Exception("RequestTokenUrl cannot be empty.");
            if (string.IsNullOrWhiteSpace(settings.AccessTokenUrl))
                throw new Exception("AccessTokenUrl cannot be empty.");
            if (string.IsNullOrWhiteSpace(settings.AuthorizeUrl))
                throw new Exception("AuthorizeUrl cannot be empty.");
            if (string.IsNullOrWhiteSpace(settings.BaseApiServiceUrl))
                throw new Exception("BaseApiServiceUrl cannot be empty.");
            if (string.IsNullOrWhiteSpace(settings.BaseApiServiceUrl))
                throw new Exception("BaseApiServiceUrl cannot be empty.");
        }

        public virtual string GetResource(FidesicSession session, string resourceUrl, bool isProtectedResource = true, object anonymousClass = null)
        {
            IConsumerRequest request = null;

            try
            {
                if (String.IsNullOrWhiteSpace(resourceUrl)) { throw new ArgumentException("The URL of the resource being requested cannot be null or empty.", "resourceUrl"); }

                ValidateAndProvisionFidesicSession(session, isProtectedResource);

                if (!resourceUrl.StartsWith("/"))
                {
                    resourceUrl = "/" + resourceUrl;
                }


                string acceptHeader = GenerateAcceptHeader();

                if (anonymousClass != null)
                {
                    request = OAuthSession.Request(isProtectedResource).Get().ForUrl(Settings.BaseApiServiceUrl + resourceUrl).WithQueryParameters(anonymousClass).WithAcceptHeader(acceptHeader).WithRawContentType(acceptHeader);
                }
                else
                {
                    request = OAuthSession.Request(isProtectedResource).Get().ForUrl(Settings.BaseApiServiceUrl + resourceUrl).WithAcceptHeader(acceptHeader).WithRawContentType(acceptHeader);
                }


                if (request != null)
                {
                    return request.ToString();
                }

                return String.Empty;
            }
            catch (DevDefined.OAuth.Framework.OAuthException oauthException)
            {
                switch (oauthException.Report.Problem)
                {
                    case "An Access Token does not exist for the client.":

                        throw new FidesicSessionExpiredException("Your session with the Fidesic API has expired. Please re-authenticate and re-authorize with the API.", oauthException);

                    default:

                        throw oauthException;
                }
            }
            catch (RedirectedForAuthorizationException redirectedForAuthorizationException)
            {
                throw redirectedForAuthorizationException;
            }
            catch (System.Exception exception)
            {
                throw exception;
            }
        }

        public virtual string PostResource(FidesicSession session, string resourceUrl, string body = null, bool isProtectedResource = true, object anonymousClass = null)
        {
            IConsumerRequest request = null;

            try
            {
                if (String.IsNullOrWhiteSpace(resourceUrl)) { throw new ArgumentException("The URL of the resource being requested cannot be null or empty.", "resourceUrl"); }

                ValidateAndProvisionFidesicSession(session, isProtectedResource);

                if (!resourceUrl.StartsWith("/"))
                {
                    resourceUrl = "/" + resourceUrl;
                }


                string acceptHeader = GenerateAcceptHeader();

                if (anonymousClass != null)
                {
                    request = OAuthSession.Request(isProtectedResource).Post().ForUrl(Settings.BaseApiServiceUrl + resourceUrl).WithRawContent(body, Encoding.Default).WithRawContentType(acceptHeader).WithQueryParameters(anonymousClass).WithAcceptHeader(acceptHeader);
                }
                else
                {
                    request = OAuthSession.Request(isProtectedResource).Post().ForUrl(Settings.BaseApiServiceUrl + resourceUrl).WithRawContent(body, Encoding.Default).WithRawContentType(acceptHeader).WithAcceptHeader(acceptHeader);
                }


                if (request != null)
                {
                    return request.ToString();
                }

                return String.Empty;
            }
            catch (DevDefined.OAuth.Framework.OAuthException oauthException)
            {
                switch (oauthException.Report.Problem)
                {
                    case "An Access Token does not exist for the client.":

                        throw new FidesicSessionExpiredException("Your session with the Fidesic API has expired. Please re-authenticate and re-authorize with the API.", oauthException);

                    default:

                        throw oauthException;
                }
            }
            catch (RedirectedForAuthorizationException redirectedForAuthorizationException)
            {
                throw redirectedForAuthorizationException;
            }
            catch (System.Exception exception)
            {
                throw exception;
            }
        }

        public virtual string DeleteResource(FidesicSession session, string resourceUrl, bool isProtectedResource = true, object anonymousClass = null)
        {
            IConsumerRequest request = null;

            try
            {
                if (String.IsNullOrWhiteSpace(resourceUrl)) { throw new ArgumentException("The URL of the resource being requested cannot be null or empty.", "resourceUrl"); }

                ValidateAndProvisionFidesicSession(session, isProtectedResource);

                if (!resourceUrl.StartsWith("/"))
                {
                    resourceUrl = "/" + resourceUrl;
                }


                string acceptHeader = GenerateAcceptHeader();

                if (anonymousClass != null)
                {
                    request = OAuthSession.Request(isProtectedResource).Delete().ForUrl(Settings.BaseApiServiceUrl + resourceUrl).WithQueryParameters(anonymousClass).WithAcceptHeader(acceptHeader).WithRawContentType(acceptHeader);
                }
                else
                {
                    request = OAuthSession.Request(isProtectedResource).Delete().ForUrl(Settings.BaseApiServiceUrl + resourceUrl).WithAcceptHeader(acceptHeader).WithRawContentType(acceptHeader);
                }


                if (request != null)
                {
                    return request.ToString();
                }

                return String.Empty;
            }
            catch (DevDefined.OAuth.Framework.OAuthException oauthException)
            {
                switch (oauthException.Report.Problem)
                {
                    case "An Access Token does not exist for the client.":

                        throw new FidesicSessionExpiredException("Your session with the Fidesic API has expired. Please re-authenticate and re-authorize with the API.", oauthException);

                    default:

                        throw oauthException;
                }
            }
            catch (RedirectedForAuthorizationException redirectedForAuthorizationException)
            {
                throw redirectedForAuthorizationException;
            }
            catch (System.Exception exception)
            {
                throw exception;
            }
        }

        protected virtual string GenerateAcceptHeader()
        {
            string acceptHeader = String.Empty;

            switch (Settings.DataFormat)
            {
                case OperationDataFormat.Json:

                    acceptHeader = "application/json";
                    break;

                case OperationDataFormat.Xml:

                    acceptHeader = "application/xml";
                    break;

                default:

                    acceptHeader = "application/json";
                    break;
            }

            return acceptHeader;
        }
        // This "CreateSession" method is meant for sessions where the API will be making API specific requests where no protected user resources are being accessed.
        public virtual FidesicSession CreateSession(string emailAddress, string companyName)
        {
            return new FidesicSession(emailAddress, companyName);
        }


        // This "CreateSession" method is meant for sessions where the API will be making requests for protected user resources (password is an optional parameter 
        // in the case that the Consumer is trusted.
        public virtual FidesicSession CreateSession(string emailAddress, string companyName, string password)
        {
            if (String.IsNullOrWhiteSpace(password))
            {
                return CreateSession(emailAddress, companyName);
            }


            FidesicSession session = null;

            try
            {
                session = new FidesicSession(emailAddress, companyName);

                if (Settings.ConsumerIsTrusted)
                {
                    // We don't want to hold on to the user's password in memory...so if a password is provided, then we're going to attempt to provision the session 
                    // all the way through the "AuthorizeUser" step.
                    ProvisionConsumerSession(session);
                    AuthorizeClientResult authorizeResult = AuthorizeUser(session, password);


                    if (authorizeResult.Status == AuthorizeClientStatus.Failed)
                    {
                        throw new FidesicLoginException("The username/password credentials provided for the target user are invalid.");
                    }
                    else if (authorizeResult.Status == AuthorizeClientStatus.Unknown)
                    {
                        throw new FidesicException("An unknown error occurred.");
                    }
                    else if (authorizeResult.Status == AuthorizeClientStatus.RequiredClientRedirection)
                    {
                        // If within a web context...
                        if (HttpContext.Current != null)
                        {
                            HttpContext.Current.Response.Redirect(authorizeResult.AuthorizationUrl, false);
                        }
                    }


                    if (session.AccessToken == null && session.Authorized)
                    {
                        session.AccessToken = OAuthSession.ExchangeRequestTokenForAccessToken(session.RequestToken, session.VerificationCode);
                    }
                }


                return session;
            }
            catch (System.Exception exception)
            {
                HandleException(exception);
                throw exception;
            }
        }


        public virtual FidesicSession RestoreSession(string emailAddress, string companyName, string accessToken)
        {
            NameValueCollection tokenParameters = System.Web.HttpUtility.ParseQueryString(accessToken);


            string token = String.Empty;

            if (tokenParameters.AllKeys.Count(t => t == Parameters.OAuth_Token) > 0)
            {
                token = tokenParameters[Parameters.OAuth_Token];
            }


            string tokenSecret = String.Empty;

            if (tokenParameters.AllKeys.Count(t => t == Parameters.OAuth_Token_Secret) > 0)
            {
                tokenSecret = tokenParameters[Parameters.OAuth_Token_Secret];
            }


            return new FidesicSession(emailAddress, companyName)
            {
                AccessToken = new TokenBase()
                {
                    ConsumerKey = Settings.ApiKey,
                    Token = token,
                    TokenSecret = tokenSecret
                }
            };
        }


        /// <summary>Retrieves the Request Token for the consumer from the OAuth service.</summary>
        /// <param name="session"></param>
        protected virtual void AuthorizeConsumer(FidesicSession session)
        {
            // No need to try to authorize the consumer if request token and/or access token are already assigned.
            if (session.AccessToken != null && !String.IsNullOrWhiteSpace(session.AccessToken.Token)) { return; }
            if (session.RequestToken != null && !String.IsNullOrWhiteSpace(session.RequestToken.Token)) { return; }


            try
            {
                session.RequestToken = OAuthSession.GetRequestToken();

                if (session.RequestToken == null)
                {
                    // TODO: throw an exception.
                }
            }
            catch (DevDefined.OAuth.Framework.OAuthException oAuthException)
            {
                // TODO: handle the exception...possibly re-throw it.
            }
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="session"></param>
        /// <returns>The result of the attempt to authorize the client against the OAuth service. If the consumer is trusted, 
        /// the authorization code returned by the OAuth service will be provided within the reuslt. Otherwise, the authorization 
        /// URL will be provided within the result that the client application will need to direct the user to complete authorization.</returns>
        protected virtual AuthorizeClientResult AuthorizeUser(FidesicSession session, string password)
        {
            if (session.Authorized)
            {
                return new AuthorizeClientResult(true);
            }


            AuthorizeClientResult result = new AuthorizeClientResult();
            string authorizationUrl = null;

            if (!String.IsNullOrWhiteSpace(Settings.AuthorizationCallbackUrl))
            {
                authorizationUrl = OAuthSession.GetUserAuthorizationUrlForToken(session.RequestToken, Settings.AuthorizationCallbackUrl);
            }
            else
            {
                authorizationUrl = OAuthSession.GetUserAuthorizationUrlForToken(session.RequestToken);
            }


            if (Settings.ConsumerIsTrusted)
            {
                if (String.IsNullOrWhiteSpace(password))
                {
                    throw new PasswordRequiredException("A password is required for a trusted consumer to authorize a user.");
                }


                // Call the OAuth service and retrieve the authorization code.
                try
                {
                    HttpWebRequest request = Utils.GenerateHttpRequest(authorizationUrl, "POST", Settings.ApiTimeout, OAuthSession.ProxyServerUri);
                    using (Stream requestStream = request.GetRequestStream())
                    {
                        using (StreamWriter writer = new StreamWriter(requestStream, Encoding.UTF8))
                        {
                            Dictionary<string, object> requestContents = new Dictionary<string, object>();
                            requestContents.Add("username", session.Email);
                            requestContents.Add("password", password);

                            writer.Write(Utils.BuildOAuthHttpBodyContent(requestContents));
                        }
                    }


                    HttpWebResponse response = (HttpWebResponse)request.GetResponse();

                    using (Stream responseStream = response.GetResponseStream())
                    {
                        using (StreamReader reader = new StreamReader(responseStream))
                        {
                            result.VerificationCode = reader.ReadToEnd();

                            if (String.IsNullOrWhiteSpace(result.VerificationCode))
                            {
                                result.Status = AuthorizeClientStatus.Failed;
                                result.Message = "The OAuth service did not return a valid verification code.";
                            }
                            else
                            {
                                result.Status = AuthorizeClientStatus.Succeeded;
                                result.Message = "The OAuth service successfully returned a valid verification code.";

                                session.VerificationCode = result.VerificationCode;
                            }
                        }
                    }
                }
                catch (System.Exception exception)
                {
                    result.Status = AuthorizeClientStatus.Failed;
                    result.Message = exception.Message;
                }
            }
            else
            {
                result.AuthorizationUrl = authorizationUrl;
                result.Status = AuthorizeClientStatus.RequiredClientRedirection;
            }


            return result;
        }


        protected virtual void HandleException(System.Exception exception)
        {

        }
        //------\\ Methods //-----------------------------------------------//




        [Serializable]
        public class FidesicSession
        {
            //------// Properties \\--------------------------------------------\\
            private bool _isDirty = false;
            public virtual bool IsDirty
            {
                get { return _isDirty; }
                set { _isDirty = value; }
            }


            private string _email = null;
            public virtual string Email
            {
                get { return _email; }
                set
                {
                    if (!String.Equals(_email, value))
                    {
                        _email = value;
                        _isDirty = true;
                    }
                }
            }


            private string _companyName = null;
            public virtual string CompanyName
            {
                get { return _companyName; }
                set
                {
                    if (!String.Equals(_companyName, value))
                    {
                        _companyName = value;
                        _isDirty = true;
                    }
                }
            }


            private IToken _requestToken = null;
            public virtual IToken RequestToken
            {
                get { return _requestToken; }
                set { _requestToken = value; }
            }


            private string _verificationCode = null;
            public virtual string VerificationCode
            {
                get { return _verificationCode; }
                set { _verificationCode = value; }
            }


            private IToken _accessToken = null;
            public virtual IToken AccessToken
            {
                get { return _accessToken; }
                set { _accessToken = value; }
            }


            private string _password = null;
            public virtual string Password
            {
                get { return _password; }
                set { _password = value; }
            }


            public virtual bool Authorized
            {
                get { return ((AccessToken != null && !String.IsNullOrWhiteSpace(AccessToken.Token)) || (RequestToken != null && !String.IsNullOrWhiteSpace(RequestToken.Token) && !String.IsNullOrWhiteSpace(VerificationCode))); }
            }
            //------\\ Properties //--------------------------------------------//



            //------// Constructors \\------------------------------------------\\
            internal FidesicSession()
            { }


            internal FidesicSession(string email)
            {
                _email = email;
            }


            internal FidesicSession(string email, string companyName)
                : this(email)
            {
                _companyName = companyName;
            }
            //------\\ Constructors //------------------------------------------//



            //------// Methods \\-----------------------------------------------\\

            //------\\ Methods //-----------------------------------------------//
        }
    }
}
