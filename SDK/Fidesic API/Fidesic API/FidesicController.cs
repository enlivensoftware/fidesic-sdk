﻿using System;
using System.Collections.Generic;
using System.Web.Script.Serialization;
using Fidesic.Data;
using Newtonsoft.Json;

namespace Fidesic
{
    public abstract class FidesicController
    {
        #region Properties
        //------// Properties \\--------------------------------------------\\
        private bool _isDirty = false;
        protected virtual bool IsDirty
        {
            get { return _isDirty; }
            set { _isDirty = value; }
        }


        private FidesicApiSettings _apiSettings = new FidesicApiSettings();
        public virtual FidesicApiSettings ApiSettings
        {
            get { return _apiSettings; }
            protected set // Doing this to enforce the FidesicApiSettings requirement of the constructors. If this needs to be changed at any point, then a new FidesicController object will need to be instantiated. This ensures that FidesicSession objects aren't exposed to other FidesicSession objects of different users.
            {
                _apiSettings = value;

                if (WebClient != null)
                {
                    WebClient.Settings = _apiSettings;
                }
            }
        }


        private FidesicUser _currentUser = null;
        public virtual FidesicUser CurrentUser
        {
            get
            {
                if (_currentUser == null)
                {
                    _currentUser = GetCurrentUser();
                }

                return _currentUser;
            }
        }


        /// <summary>
        /// Queries environmental variables to determine if the current user is a connected to the Fidesic API at this moment.
        /// </summary>
        private bool? _connectedToFidesic = null;
        public virtual bool ConnectedToFidesic
        {
            get
            {
                if (!_connectedToFidesic.HasValue)
                {
                    _connectedToFidesic = (WebClient != null && Session != null && Session.AccessToken != null && !String.IsNullOrWhiteSpace(Session.AccessToken.Token));
                }

                return _connectedToFidesic.Value;
            }
        }


        private FidesicWebClient.FidesicSession _session = null;
        protected virtual FidesicWebClient.FidesicSession Session
        {
            get { return _session; }
            set { _session = value; }
        }


        private FidesicWebClient _webClient = null;
        protected virtual FidesicWebClient WebClient
        {
            get { return _webClient; }
            set { _webClient = value; }
        }


        private bool _automaticallyReconnectOnChange = true;
        public virtual bool AutomaticallyReconnectOnChange
        {
            get { return _automaticallyReconnectOnChange; }
            set { _automaticallyReconnectOnChange = value; }
        }
        //------\\ Properties //--------------------------------------------//
        #endregion

        #region Fields and Constructors
        //------// Fields \\------------------------------------------------\\
        protected readonly string FidesicSession_CacheKey = "Fidesic.FidesicController.Session";
        //------\\ Fields //------------------------------------------------//



        //------// Constructors \\------------------------------------------\\
        public FidesicController(FidesicApiSettings apiSettings)
        {
            ApiSettings = apiSettings;
        }


        public FidesicController(FidesicApiSettings apiSettings, bool automaticallyReconnectOnChange)
            : this(apiSettings)
        {
            AutomaticallyReconnectOnChange = automaticallyReconnectOnChange;
        }
        //------\\ Constructors //------------------------------------------//



        //------// Methods \\-----------------------------------------------\\
        #endregion

        #region API Methods
        #region "API Status Methods"
        public virtual Dictionary<string, object> CheckStatus()
        {
            try
            {
                string resourceAsString = GetResource("/API/Status/");
                return DeserializeFromJson<Dictionary<string, object>>(resourceAsString);
            }
            catch
            {
                return new Dictionary<string, object>();
            }
        }
        public virtual bool SendError(string errorMessage, string stackTrace)
        {
            try
            {
                string resourceAsString = SerializeToJson(new { errorMessage, stackTrace });
                resourceAsString = PostResource("/API/Error/", resourceAsString, false);
                return DeserializeFromJson<bool>(resourceAsString);
            }
            catch
            {
                return false;
            }
        }

        public virtual bool SendInfo(Dictionary<string, object> info)
        {   
            try
            {
                string resourceAsString = SerializeToJson(info);
                resourceAsString = PostResource("/API/Info/", resourceAsString, false);
                return DeserializeFromJson<bool>(resourceAsString);
            }
            catch
            {
                return false;
            }
        }
        #endregion
        #region "User Profile Methods"
        public virtual FidesicUser GetCurrentUser()
        {
            try
            {
                string resourceAsString = GetResource("/User/GetProfile/");
                return FidesicUser.DeserializeFromJson<FidesicUser>(resourceAsString);
            }
            catch
            {
                return null;
            }
        }

        public virtual void CreateUser(CreateUserData createUserData)
        {
            string resourceAsString = createUserData.SerializeToJson();
            PostResource("/User/CreateUser/", resourceAsString, false);
        }
        public virtual void CreateUser(CreateUserData createUserData, int linkUserID)
        {
            string resourceAsString = createUserData.SerializeToJson();
            PostResource("/User/CreateUser/", resourceAsString, false);
        }

        public virtual void UpdateUser(FidesicUser profile)
        {
            string resourceAsString = profile.SerializeToJson();
            PostResource("/User/UpdateProfile/", resourceAsString);
        }
        /// <summary>
        /// This will return Dictionary of UserID, FriendlyName
        /// </summary>
        /// <returns></returns>
        public virtual List<FidesicLogin> ListAccounts()
        {
            string resourceAsString = GetResource("/User/ListAccounts/");
            return DeserializeFromJson<List<FidesicLogin>>(resourceAsString);
        }
        /// <summary>
        /// Pass in a valid fidesic UserID to switch your current logged in account to.
        /// </summary>
        /// <param name="userID"></param>
        /// <returns></returns>
        public virtual bool SwitchAccount(int userID)
        {
            string resourceAsString = GetResource("/User/SwitchAccount/?userID=" + userID.ToString());
            return DeserializeFromJson<bool>(resourceAsString);
        }

        /// <summary>
        /// If a master account's email receives an invoice from another Fidesic user,
        /// that they are not linked to currently, this will attempt to resolve the linking.
        /// </summary>
        /// <param name="invoiceID"></param>
        /// <returns></returns>
        public virtual bool LinkAccountByInvoiceID(int invoiceID)
        {
            string resourceAsString = GetResource("/User/LinkAccountByInvoiceID/?invoiceID=" + invoiceID.ToString());
            return DeserializeFromJson<bool>(resourceAsString);
        }

        public virtual void ChangePassword(string oldPassword, string newPassword)
        {
            string resourceAsString = "{\"oldPassword\": \"" + oldPassword + "\", \"newPassword\": \"" + newPassword + "\"}";
            PostResource("/User/ChangePassword/", resourceAsString);
        }

        public virtual bool LostPasswordRequest(string emailAddress)
        {
            string resourceAsString = "{\"emailAddress\": \"" + emailAddress + "\"}";
            resourceAsString = PostResource("/User/LostPasswordRequest/", "\"" + emailAddress + "\"", false);
            return DeserializeFromJson<bool>(resourceAsString);
        }

        public virtual bool LostPasswordResetWithCode(string emailAddress, string resetCode, string newPassword)
        {
            string resourceAsString = "{\"emailAddress\": \"" + emailAddress + 
                "\", \"resetCode\": \"" + resetCode + "\", \"newPassword\": \"" + newPassword + "\"}";
            resourceAsString = PostResource("/User/LostPasswordResetWithCode/", resourceAsString, false);
            return DeserializeFromJson<bool>(resourceAsString);
        }

        public virtual Permissions GetUserPermissions()
        {
            string resourceAsString = GetResource("/User/GetUserPermissions/");
            return DeserializeFromJson<Permissions>(resourceAsString);
        }

        public virtual List<Attachment> GetAccountLogos()
        {
            string resourceAsString = GetResource("/User/GetAccountLogos/");
            return DeserializeFromJson<List<Attachment>>(resourceAsString);
        }
        #endregion
        
        #region Approval Methods
        public void UpdateApprovalStatus(int documentID, ApprovalType type, ApprovalStatus status)
        {
            GetResource("/Approval/UpdateApprovalStatus/?documentID=" + documentID.ToString() + "&type=" + type.ToString() + "&status=" + status.ToString());
        }
        public virtual List<Payment> GetUnapprovedPayments()
        {
            string resourceAsString = GetResource("/Approval/GetUnapprovedPayments/");
            return DeserializeFromJson<List<Payment>>(resourceAsString);
        }
        public virtual List<Invoice> GeUnapprovedInvoices()
        {
            string resourceAsString = GetResource("/Approval/GetUnapprovedInvoices/");
            return DeserializeFromJson<List<Invoice>>(resourceAsString);
        }
        #endregion

        #region "Bank Account Methods"
        public virtual void SetDefaultDepositAccount(int accountID)
        {
            if (accountID <= 0) { return; }

            PostResource("/PaymentAccount/SetDefaultDepositAccount/", accountID.ToString());
        }
        public virtual void SetDefaultPaymentAccount(int accountID)
        {
            if (accountID <= 0) { return; }

            PostResource("/PaymentAccount/SetDefaultPaymentAccount/", accountID.ToString());
        }

        public virtual List<BankAccount> GetAllBankAccounts()
        {
            string resourceAsString = GetResource("/PaymentAccount/GetAllBankAccounts/");
            return DeserializeFromJson<List<BankAccount>>(resourceAsString);
        }

        public virtual BankAccount GetBankAccountByName(string accountName)
        {
            if (String.IsNullOrWhiteSpace(accountName)) { return null; }

            string resourceAsString = GetResource("/PaymentAccount/GetBankAccountByName/?name=" + accountName);
            return DeserializeFromJson<BankAccount>(resourceAsString);
        }

        public virtual BankAccount GetBankAccount(int accountID)
        {
            if (accountID <= 0) { return null; }

            string resourceAsString = GetResource("/PaymentAccount/GetBankAccount/?id=" + accountID.ToString());
            return DeserializeFromJson<BankAccount>(resourceAsString);
        }

        public virtual int? AddBankAccount(BankAccount bankAccount)
        {
            if (bankAccount == null) { return null; }

            string resourceAsString = bankAccount.SerializeToJson();
            resourceAsString = PostResource("/PaymentAccount/AddBankAccount/", resourceAsString);

            int bankAccountID;
            if (Int32.TryParse(resourceAsString, out bankAccountID))
            {
                return bankAccountID;
            }

            return null;
        }

        public virtual void UpdateBankAccount(BankAccount bankAccount)
        {
            if (bankAccount == null) { return; }

            string resourceAsString = bankAccount.SerializeToJson();
            PostResource("/PaymentAccount/UpdateBankAccount/", resourceAsString);
        }

        public virtual void DeleteBankAccountByName(string accountName)
        {
            if (String.IsNullOrWhiteSpace(accountName)) { return; }

            PostResource("/PaymentAccount/DeleteBankAccountByName/", accountName);
        }

        public virtual void DeleteBankAccount(int accountID)
        {
            if (accountID <= 0) { return; }

            PostResource("/PaymentAccount/DeleteBankAccount/", accountID.ToString());
        }

        #endregion

        #region "Credit Card Account Methods"

        public virtual List<CreditCardAccount> GetAllCreditCardAccounts()
        {
            string resourceAsString = GetResource("/PaymentAccount/GetAllCreditCardAccounts/");
            return DeserializeFromJson<List<CreditCardAccount>>(resourceAsString);
        }

        public virtual CreditCardAccount GetCreditCardAccountByName(string accountName)
        {
            if (String.IsNullOrWhiteSpace(accountName)) { return null; }

            string resourceAsString = GetResource("/PaymentAccount/GetCreditCardAccountByName/?name=" + accountName);
            return DeserializeFromJson<CreditCardAccount>(resourceAsString);
        }

        public virtual CreditCardAccount GetCreditCardAccount(int accountID)
        {
            if (accountID <= 0) { return null; }

            string resourceAsString = GetResource("/PaymentAccount/GetCreditCardAccount/?id=" + accountID.ToString());
            return DeserializeFromJson<CreditCardAccount>(resourceAsString);
        }

        public virtual int? AddCreditCardAccount(CreditCardAccount creditCardAccount)
        {
            if (creditCardAccount == null) { return null; }

            string resourceAsString = creditCardAccount.SerializeToJson();
            resourceAsString = PostResource("/PaymentAccount/AddCreditCardAccount/", resourceAsString);


            int creditCardAccountID;

            if (Int32.TryParse(resourceAsString, out creditCardAccountID))
            {
                return creditCardAccountID;
            }


            return null;
        }

        public virtual void UpdateCreditCardAccount(CreditCardAccount creditCardAccount)
        {
            if (creditCardAccount == null) { return; }

            string resourceAsString = creditCardAccount.SerializeToJson();
            PostResource("/PaymentAccount/UpdateCreditCardAccount/", resourceAsString);
        }

        public virtual void DeleteCreditCardAccountByName(string accountName)
        {
            if (String.IsNullOrWhiteSpace(accountName)) { return; }

            PostResource("/PaymentAccount/DeleteCreditCardAccountByName/", accountName);
        }

        public virtual void DeleteCreditCardAccount(int accountID)
        {
            if (accountID <= 0) { return; }

            PostResource("/PaymentAccount/DeleteCreditCardAccount/", accountID.ToString());
        }

        #endregion

        #region "Invoice Methods"
        
        /// <summary>
        /// DEPRACATED
        /// </summary>
        public virtual List<Invoice> GetAllSentInvoices()
        {
            string resourceAsString = GetResource("/Invoice/GetAllSentInvoices/");
            return DeserializeFromJson<List<Invoice>>(resourceAsString);
        }
        /// <summary>
        /// DEPRACATED
        /// </summary>
        public virtual List<Invoice> GetSentInvoices(int pageNumber, int pageSize)
        {
            string resourceAsString = GetResource("/Invoice/GetSentInvoices/?pageNumber=" + pageNumber.ToString() + "&pageSize=" + pageSize.ToString());
            return DeserializeFromJson<List<Invoice>>(resourceAsString);
        }
        /// <summary>
        /// DEPRACATED
        /// </summary>
        public virtual List<Invoice> GetSentInvoices(int pageNumber, int pageSize, DateTime? startDate, DateTime? endDate,
            string customerName, InvoiceStatus? status = null, DateFilteringMode? dateFilteringMode = null)
        {
            string resourceAsString = GetResource("/Invoice/GetSentInvoices/?pageNumber=" + pageNumber.ToString() + "&pageSize=" + pageSize.ToString()
                + "&startDate=" + MakeDateTimeString(startDate) + "&endDate=" + MakeDateTimeString(endDate) + "&customerName=" + customerName
                + "&status=" + status.ToString() + "&dateFilteringMode=" + dateFilteringMode.ToString());
            return DeserializeFromJson<List<Invoice>>(resourceAsString);
        }

        public virtual InvoicesWithTotals GetSentInvoicesWithTotals(int pageNumber, int pageSize, DateTime? startDate, DateTime? endDate,
            string customerName, InvoiceStatus? status = null, DateFilteringMode? dateFilteringMode = null)
        {
            string resourceAsString = GetResource("/Invoice/GetSentInvoicesWithTotals/?pageNumber=" + pageNumber.ToString() + "&pageSize=" + pageSize.ToString()
                + "&startDate=" + MakeDateTimeString(startDate) + "&endDate=" + MakeDateTimeString(endDate) + "&customerName=" + customerName 
                + "&status=" + status.ToString() + "&dateFilteringMode=" + dateFilteringMode.ToString());
            return DeserializeFromJson<InvoicesWithTotals>(resourceAsString);
        }
        /// <summary>
        /// DEPRACATED
        /// </summary>
        public virtual List<Invoice> GetAllReceivedInvoices()
        {
            string resourceAsString = GetResource("/Invoice/GetAllReceivedInvoices/");
            return DeserializeFromJson<List<Invoice>>(resourceAsString);
        }

        /// <summary>
        /// DEPRACATED
        /// </summary>
        public virtual List<Invoice> GetReceivedInvoices(int pageNumber, int pageSize)
        {
            string resourceAsString = GetResource("/Invoice/GetReceivedInvoices/?pageNumber=" + pageNumber.ToString() + "&pageSize=" + pageSize.ToString());
            return DeserializeFromJson<List<Invoice>>(resourceAsString);
        }

        /// <summary>
        /// DEPRACATED
        /// </summary>
        public virtual List<Invoice> GetReceivedInvoices(int pageNumber, int pageSize, DateTime? startDate, DateTime? endDate,
            string vendorName, InvoiceStatus? status = null, DateFilteringMode? dateFilteringMode = null)
        {
            string resourceAsString = GetResource("/Invoice/GetReceivedInvoices/?pageNumber=" + pageNumber.ToString() + "&pageSize=" + pageSize.ToString()
                + "&startDate=" + MakeDateTimeString(startDate) + "&endDate=" + MakeDateTimeString(endDate) + "&vendorName=" + vendorName + "&status="
                + status.ToString() + "&dateFilteringMode=" + dateFilteringMode.ToString());
            return DeserializeFromJson<List<Invoice>>(resourceAsString);
        }

        public virtual InvoicesWithTotals GetReceivedInvoicesWithTotals(int pageNumber, int pageSize, DateTime? startDate, DateTime? endDate,
            string vendorName, InvoiceStatus? status = null, DateFilteringMode? dateFilteringMode = null)
        {
            string resourceAsString = GetResource("/Invoice/GetReceivedInvoicesWithTotals/?pageNumber=" + pageNumber.ToString() + "&pageSize=" + pageSize.ToString()
                + "&startDate=" + MakeDateTimeString(startDate) + "&endDate=" + MakeDateTimeString(endDate) + "&vendorName=" + vendorName
                + "&status=" + status.ToString() + "&dateFilteringMode=" + dateFilteringMode.ToString());
            return DeserializeFromJson<InvoicesWithTotals>(resourceAsString);
        }

        public virtual Invoice GetInvoice(int invoiceID)
        {
            string resourceAsString = GetResource("/Invoice/GetInvoice/?id=" + invoiceID.ToString());
            return DeserializeFromJson<Invoice>(resourceAsString);
        }

        public virtual Invoice GetSentInvoiceByInvoiceNumber(string invoiceNumber)
        {
            string resourceAsString = GetResource("/Invoice/GetSentInvoiceByInvoiceNumber/?invoiceNumber=" + invoiceNumber);
            return DeserializeFromJson<Invoice>(resourceAsString);
        }
        
        /// <summary>
        /// Pass in payment channel and Amount to get settlement details. 
        /// It will adjust amounts according to channel and amount being paid.
        /// If amount is passed in as 0 or null, it will return full unpaid amount
        /// </summary>
        /// <param name="invoiceNumber"></param>
        /// <param name="paymentGateway"></param>
        /// <param name="amount"></param>
        /// <returns></returns>
        public virtual Invoice GetInvoiceByInvoiceNumberWithAmount(string invoiceNumber, Data.DepositOption paymentGateway, decimal? amount)
        {
            string resourceAsString = GetResource("/Invoice/GetSentInvoiceByInvoiceNumber/?invoiceNumber=" + invoiceNumber +"&paymentGateway=" + paymentGateway.ToString() + "&amount=" + MakeDec(amount,4).ToString());
            return DeserializeFromJson<Invoice>(resourceAsString);
        }

        public virtual List<Data.Invoice> SyncAPInvoices()
        {
            string resourceAsString = GetResource("/Invoice/SyncAPInvoices/");
            return DeserializeFromJson<List<Data.Invoice>>(resourceAsString);
        }

        public virtual bool MarkInvoiceAsExported(string invoiceID)
        {
            string resourceAsString = GetResource("/Invoice/MarkInvoiceAsExported/?invoiceID=" + invoiceID);
            if (Boolean.TryParse(resourceAsString, out bool success))
            {
                return success;
            }

            return false;
        }

        public virtual bool MarkInvoicesAsExported(List<string> invoiceIDs)
        {
            string resourceAsString = GetResource("/Invoice/ClearSyncAPInvoices/?invoiceIDs=" + invoiceIDs);
            if (Boolean.TryParse(resourceAsString, out bool success))
            {
                return success;
            }

            return false;
        }

        public virtual List<InvoiceTerm> GetInvoiceTermOptions()
        {
            string resourceAsString = GetResource("/InvoiceTerms/");
            return DeserializeFromJson<List<Data.InvoiceTerm>>(resourceAsString);
        }

        public int AddTerm(InvoiceTerm term)
        {
            string resourceAsString = term.SerializeToJson();
            resourceAsString = PostResource("/InvoiceTerms/", resourceAsString);

            if (Int32.TryParse(resourceAsString, out int termID))
            {
                return termID;
            }

            return termID;
        }

        public bool DeleteTerm(string termDescription)
        {
            string resourceAsString = PostResource("/InvoiceTerms/Delete/", SerializeToJson(termDescription));

            if (Boolean.TryParse(resourceAsString, out bool success))
            {
                return success;
            }

            return false;
        }
        /// <summary>
        /// Required fields: InvoiceDate, Amount, InvoiceNumber, CustomerID, DueDate, Status
        /// Optional fields: Description, Memo, LineItems, Term
        /// 
        /// All other fields will be stripped and ignored.
        /// 
        /// Will return unique Fidesic InvoiceID.
        /// NOTE:
        /// To obtain the required CustomerID, grab from the GetAllCustomers method or first "AddCustomer" 
        /// and the ID will be returned.
        /// </summary>
        /// <param name="invoice"></param>
        /// <returns>InvoiceID</returns>
        public virtual int? AddInvoice(Invoice invoice)
        {
            string resourceAsString = invoice.SerializeToJson();
            resourceAsString = PostResource("/Invoice/AddInvoice/", resourceAsString);

            if (Int32.TryParse(resourceAsString, out int invoiceID))
            {
                return invoiceID;
            }

            return null;
        }

        /// <summary>
        /// Required Permission: ReceivablesSubmitExpense
        /// Required fields: Amount, InvoiceNumber
        /// Optional fields: Description, Memo, LineItems, Attachments
        /// 
        /// All other fields will be stripped and ignored.
        /// 
        /// </summary>
        /// <param name="invoice"></param>
        /// <returns>bool</returns>
        public virtual bool SubmitExpense(Invoice invoice)
        {
            string resourceAsString = invoice.SerializeToJson();
            resourceAsString = PostResource("/Invoice/SubmitExpense/", resourceAsString);

            return DeserializeFromJson<bool>(resourceAsString);
        }

        /// <summary>
        /// Required fields: InvoiceDate, Amount, InvoiceNumber, CustomerID, DueDate, Status
        /// Optional fields: Description, Memo, LineItems, Term, Remittance
        /// 
        /// All other fields will be stripped and ignored.
        /// 
        /// Will return unique Fidesic InvoiceID.
        /// NOTE:
        /// To obtain the required CustomerID, grab from the GetAllCustomers method or first "AddCustomer" 
        /// and the ID will be returned. OR Pass in a popluated Customer Object which should automatically 
        /// match.
        /// 
        /// Use REMITTANCE object for applying credit to open invoices.
        /// </summary>
        /// <param name="invoice"></param>
        /// <returns>InvoiceID</returns>
        public virtual int? AddCreditMemo(Invoice credit)
        {
            string resourceAsString = credit.SerializeToJson();
            resourceAsString = PostResource("/Invoice/AddCreditMemo/", resourceAsString);

            int creditID;
            if (Int32.TryParse(resourceAsString, out creditID))
            {
                return creditID;
            }

            return null;
        }

        public virtual void UpdateInvoice(Invoice invoice)
        {
            string resourceAsString = invoice.SerializeToJson();
            PostResource("/Invoice/UpdateInvoice/", resourceAsString);
        }

        public virtual bool VoidInvoice(int invoiceID, string reason)
        {
            string resourceAsString = GetResource("/Invoice/VoidInvoice/?invoiceID=" + invoiceID.ToString() + "&reason=" + reason);

            if (Boolean.TryParse(resourceAsString, out bool success))
            {
                return success;
            }

            return false;
        }

        public virtual void ResendInvoice(int invoiceID, string messageNotes)
        {
            GetResource("/Invoice/ResendInvoice/?invoiceID=" + invoiceID.ToString() + "&messageNotes=" + messageNotes);
        }

        public virtual void ClearSyncAPInvoices(List<int> invoiceIDs)
        {
            string resourceAsString = Newtonsoft.Json.JsonConvert.SerializeObject(invoiceIDs);
            PostResource("/Invoice/ClearSyncAPInvoices/", resourceAsString);
        }
        #endregion

        #region "Item Methods"
        public virtual List<ItemTemplate> GetAllItemTemplates()
        {
            string resourceAsString = GetResource("/Invoice/GetAllItemTemplates/");
            return DeserializeFromJson<List<ItemTemplate>>(resourceAsString);
        }

        public virtual ItemTemplate GetItemTemplate(int itemTemplateID)
        {
            string resourceAsString = GetResource("/Invoice/GetItemTemplate/?id=" + itemTemplateID.ToString());
            return DeserializeFromJson<ItemTemplate>(resourceAsString);
        }

        public virtual int AddItemTemplate(ItemTemplate itemTemplate)
        {
            string resourceAsString = itemTemplate.SerializeToJson();
            resourceAsString = PostResource("/Invoice/AddItemTemplate/", resourceAsString);

            int itemID;
            if (Int32.TryParse(resourceAsString, out itemID))
            {
                return itemID;
            }
            return 0;
        }

        public virtual void UpdateItemTemplate(ItemTemplate itemTemplate)
        {
            string resourceAsString = itemTemplate.SerializeToJson();
            PostResource("/Invoice/UpdateItemTemplate/", resourceAsString);
        }

        public virtual void DeleteItemTemplate(int itemTemplateID)
        {
            string resourceAsString = PostResource("/Invoice/DeleteItemTemplate/", itemTemplateID.ToString());
        }
        #endregion

        #region "Invoice Item Methods"
        public virtual List<InvoiceItem> GetAllInvoiceItems(int invoiceID)
        {
            string resourceAsString = GetResource("/Invoice/GetInvoiceItems/?invoiceID=" + invoiceID.ToString());
            return DeserializeFromJson<List<InvoiceItem>>(resourceAsString);
        }

        public virtual void AddInvoiceItem(InvoiceItem invoiceItem)
        {
            string resourceAsString = invoiceItem.SerializeToJson();
            PostResource("/Invoice/AddInvoiceItem/", resourceAsString);
        }

        public virtual void UpdateInvoiceItem(InvoiceItem invoiceItem)
        {
            string resourceAsString = invoiceItem.SerializeToJson();
            PostResource("/Invoice/UpdateInvoiceItem/", resourceAsString);
        }
        #endregion

        #region "Invoice Category Methods"
        public virtual List<InvoiceCategory> GetInvoiceCategories()
        {
            string resourceAsString = GetResource("/Invoice/GetInvoiceCategories/");
            return DeserializeFromJson<List<InvoiceCategory>>(resourceAsString);
        }
        /// <summary>
        /// Create
        /// </summary>
        /// <param name="category"></param>
        /// <returns></returns>
        public virtual int CreateInvoiceCategory(InvoiceCategory category)
        {
            string resourceAsString = category.SerializeToJson();
            resourceAsString = PostResource("/Invoice/CreateInvoiceCategory/", resourceAsString);

            int categoryID;
            if (Int32.TryParse(resourceAsString, out categoryID))
            {
                return categoryID;
            }
            return 0;
        }

        public virtual InvoiceCategory GetInvoiceCategory(int categoryID)
        {
            string resourceAsString = GetResource("/Invoice/GetInvoiceCategory/?categoryID=" + categoryID.ToString());
            return DeserializeFromJson<InvoiceCategory>(resourceAsString);
        }
        #endregion

        #region VendorLocation
        /// <summary>
        /// Returns Locations list per Vendor
        /// </summary>
        /// <returns></returns>
        public virtual List<VendorLocation> GetVendorLocations()
        {
            string resourceAsString = GetResource("/Invoice/GetVendorLocations/");
            return DeserializeFromJson<List<VendorLocation>>(resourceAsString);
        }

        /// <summary>
        /// Returns Locations list per Vendor
        /// </summary>
        /// <returns></returns>
        public virtual VendorLocation GetVendorLocationByCode(string LocationCode)
        {
            string resourceAsString = GetResource("/Invoice/GetVendorLocationByCode/?LocationCode=" + LocationCode);
            return new JavaScriptSerializer().Deserialize<VendorLocation>(resourceAsString);
        }
        

        /// <summary>
        /// Returns Fidesic unique ID for VendorLocation
        /// </summary>
        /// <returns></returns>
        public virtual int? AddVendorLocation(VendorLocation location)
        {
            string resourceAsString = location.SerializeToJson();
            resourceAsString = PostResource("/Invoice/AddVendorLocation/", resourceAsString);
            int vendorLocationID;
            if (Int32.TryParse(resourceAsString, out vendorLocationID))
            {
                return vendorLocationID;
            }

            return null;
        }
        #endregion

        #region "Customer Methods"
        public virtual List<Customer> GetAllCustomers(int page = 1, int limit = 40)
        {
            string resourceAsString = GetResource("/Customers/?page="+page+"&limit="+limit);
            return DeserializeFromJson<List<Customer>>(resourceAsString);
        }

        public virtual Customer GetCustomer(int customerUserID)
        {
            if (customerUserID <= 0) { throw new Exception("customerUserID must be greater than 0"); }

            string resourceAsString = GetResource("/Customers/" + customerUserID.ToString());
            return DeserializeFromJson<Customer>(resourceAsString);
        }

        /// <summary>
        /// Add customer to Fidesic. Will return unique Fidesic CustomerID
        /// </summary>
        /// <param name="customer"></param>
        /// <returns>CustomerID</returns>
        public virtual int AddCustomer(Customer customer)
        {
            string resourceAsString = customer.SerializeToJson();
            resourceAsString = PostResource("/Customers/", resourceAsString);

            int customerID;
            if (Int32.TryParse(resourceAsString, out customerID))
            {
                return customerID;
            }
            return 0;
        }

        /// <summary>
        /// Update Fidesic Customer record. Must have Fidesic customerID assigned.
        /// </summary>
        /// <param name="customer"></param>
        public virtual void UpdateCustomer(Customer customer)
        {
            if (customer.UserID <= 0) { throw new Exception("customer.UserID must be greater than 0"); }
            string resourceAsString = customer.SerializeToJson();
            PostResource("/Customers/" + customer.UserID, resourceAsString);
        }

        /// <summary>
        /// Merge 2 customers. new Customer will remain. Will return the customerID of the remaining customer record.
        /// </summary>
        /// <param name="oldCustomerNumber"></param>
        /// <param name="newCustomerNumber"></param>
        public virtual int MergeCustomers(string oldCustomerNumber, string newCustomerNumber)
        {
            if (string.IsNullOrWhiteSpace(oldCustomerNumber))
                throw new Exception("oldCustomerNumber must have a value");
            if (string.IsNullOrWhiteSpace(newCustomerNumber))
                throw new Exception("newCustomerNumber must have a value");

            string resourceAsString = GetResource("/Customers/Merge/" + oldCustomerNumber + "/" + newCustomerNumber);
            
            int customerID;
            if (Int32.TryParse(resourceAsString, out customerID))
            {
                return customerID;
            }
            return 0;
        }

        public virtual void DeleteCustomer(int customerUserID)
        {
            if (customerUserID <= 0) { throw new Exception("customerUserID must be greater than 0"); }

            DeleteResource("/Customers/" + customerUserID.ToString());
        }

        public virtual decimal GetCustomerBalance(string customerNumber)
        {
            if (string.IsNullOrWhiteSpace(customerNumber))
                throw new Exception("customerNumber must have a value");

            string resourceAsString = GetResource("/Customers/Balance/" + customerNumber);

            decimal balance;
            if (decimal.TryParse(resourceAsString, out balance))
            {
                return balance;
            }
            return 0;
        }
        #endregion

        #region "Vendor Methods"
        public virtual List<Vendor> GetAllVendors(int page = 1, int limit = 40)
        {
            string resourceAsString = GetResource("/Vendors/?page=" + page + "&limit=" + limit);
            return DeserializeFromJson<List<Vendor>>(resourceAsString);
        }

        public virtual Vendor GetVendor(int vendorUserID)
        {
            if (vendorUserID <= 0) { throw new Exception("vendorUserID must be greater than 0"); }

            string resourceAsString = GetResource("/Vendors/" + vendorUserID.ToString());
            return DeserializeFromJson<Vendor>(resourceAsString);
        }

        public virtual Vendor GetVendor(string vendorNumber)
        {
            if (string.IsNullOrWhiteSpace(vendorNumber)) { throw new Exception("vendorNumber must be set"); }

            string resourceAsString = GetResource("/Vendors/Number/" + vendorNumber);
            return DeserializeFromJson<Vendor>(resourceAsString);
        }

        /// <summary>
        /// Add vendor to Fidesic. Will return unique Fidesic VendorID
        /// </summary>
        /// <param name="vendor"></param>
        /// <returns>VendorID</returns>
        public virtual int AddVendor(Vendor vendor)
        {
            string resourceAsString = vendor.SerializeToJson();
            resourceAsString = PostResource("/Vendors/", resourceAsString);

            int vendorID;
            if (Int32.TryParse(resourceAsString, out vendorID))
            {
                return vendorID;
            }
            return 0;
        }

        /// <summary>
        /// Update Fidesic Vendor record. Must have Fidesic vendorID assigned.
        /// </summary>
        /// <param name="vendor"></param>
        public virtual void UpdateVendor(Vendor vendor)
        {
            if (vendor.UserID <= 0) { throw new Exception("vendor.UserID must be greater than 0"); }
            string resourceAsString = vendor.SerializeToJson();
            PostResource("/Vendors/" + vendor.UserID, resourceAsString);
        }

        public virtual void DeleteVendor(int vendorUserID)
        {
            if (vendorUserID <= 0) { throw new Exception("vendorUserID must be greater than 0"); }

            GetResource("/Vendors/Delete/" + vendorUserID.ToString());
        }

        public virtual void DeleteVendor(string vendorNumber)
        {
            if (string.IsNullOrWhiteSpace(vendorNumber)) { throw new Exception("vendorNumber must be set"); }

            GetResource("/Vendors/Delete/Number/" + vendorNumber);
        }
        #endregion

        #region "GLCode Methods"
        public virtual List<GLCode> GetAllGLCodes(int page = 1, int limit = 40)
        {
            string resourceAsString = GetResource("/GLCodes/?page=" + page + "&limit=" + limit);
            return DeserializeFromJson<List<GLCode>>(resourceAsString);
        }

        public virtual GLCode GetGLCode(int glcodeUserID)
        {
            if (glcodeUserID <= 0) { throw new Exception("glcodeUserID must be greater than 0"); }

            string resourceAsString = GetResource("/GLCodes/" + glcodeUserID.ToString());
            return DeserializeFromJson<GLCode>(resourceAsString);
        }

        /// <summary>
        /// Add glcode to Fidesic. Will return unique Fidesic GLCodeID
        /// </summary>
        /// <param name="glcode"></param>
        /// <returns>GLCodeID</returns>
        public virtual int AddGLCode(GLCode glcode)
        {
            string resourceAsString = glcode.SerializeToJson();
            resourceAsString = PostResource("/GLCodes/", resourceAsString);

            int glcodeID;
            if (Int32.TryParse(resourceAsString, out glcodeID))
            {
                return glcodeID;
            }
            return 0;
        }

        /// <summary>
        /// Update Fidesic GLCode record. Must have Fidesic glcodeID assigned.
        /// </summary>
        /// <param name="glcode"></param>
        public virtual void UpdateGLCode(GLCode glcode)
        {
            if (glcode.GLCodeID <= 0) { throw new Exception("glcode.UserID must be greater than 0"); }
            string resourceAsString = glcode.SerializeToJson();
            PostResource("/GLCodes/" + glcode.GLCodeID, resourceAsString);
        }

        public virtual bool DeleteGLCode(GLCode glcode)
        {
            string resourceAsString = glcode.SerializeToJson();
            resourceAsString = PostResource("/GLCodes/Delete/", resourceAsString);
            bool.TryParse(resourceAsString, out bool success);

            return success;
        }

        /// <summary>
        /// Add glcode to Fidesic. Will return unique Fidesic GLCodeID
        /// </summary>
        /// <param name="glclass"></param>
        /// <returns>GLClassID</returns>
        public virtual int AddGLCLass(GLClass glclass)
        {
            string resourceAsString = glclass.SerializeToJson();
            resourceAsString = PostResource("/GLCodes/Class/", resourceAsString);

            int glclassID;
            if (Int32.TryParse(resourceAsString, out glclassID))
            {
                return glclassID;
            }
            return 0;
        }
        public virtual int AddGLDepartment(GLDepartment gldepartment)
        {
            string resourceAsString = gldepartment.SerializeToJson();
            resourceAsString = PostResource("/GLCodes/Department/", resourceAsString);

            int gldepartmentID;
            if (Int32.TryParse(resourceAsString, out gldepartmentID))
            {
                return gldepartmentID;
            }
            return 0;
        }
        #endregion

        #region "Payment Methods"
        public virtual int? AddPayment(Payment payment)
        {
            string resourceAsString = payment.SerializeToJson();
            resourceAsString = PostResource("/Payment/AddPayment/", resourceAsString);

            int paymentID;

            if (Int32.TryParse(resourceAsString, out paymentID))
            {
                return paymentID;
            }
            return null;
        }

        public virtual int? SendPayablesPayment(Payment payment)
        {
            string resourceAsString = payment.SerializeToJson();
            resourceAsString = PostResource("/Payment/Send/", resourceAsString);

            int paymentID;

            if (Int32.TryParse(resourceAsString, out paymentID))
            {
                return paymentID;
            }
            return null;
        }
        public virtual int? SendPayablesPaymentByVendorNumber(Payment payment, Vendor vendor, BankAccount account)
        {
            string paymentString = payment.SerializeToJson();
            string vendorString = vendor.SerializeToJson();
            string accountString = account.SerializeToJson();
            string resourceAsString = "{\"payment\": " + paymentString + ",\"vendor\": " + vendorString + ",\"account\": " + accountString + "}";
            
            resourceAsString = PostResource("/Payment/SendByVendorNumber/", resourceAsString);

            int paymentID;

            if (Int32.TryParse(resourceAsString, out paymentID))
            {
                return paymentID;
            }
            return null;
        }
        public virtual List<Payment> GetReceivedPayments()
        {
            string resourceAsString = GetResource("/Payment/GetReceivedPayments/");
            return DeserializeFromJson<List<Payment>>(resourceAsString);
        }
        public virtual List<Payment> GetReceivedPayments(int pageNumber, int pageSize, DateTime? startDate, DateTime? endDate,
            string customerName, Data.PaymentStatus? status = null, DateFilteringMode? dateFilteringMode = null)
        {
            string resourceAsString = GetResource("/Payment/GetReceivedPayments/?pageNumber=" + pageNumber.ToString() + "&pageSize=" + pageSize.ToString()
                + "&startDate=" + MakeDateTimeString(startDate) + "&endDate=" + MakeDateTimeString(endDate) + "&customerName=" + customerName
                + "&status=" + status.ToString() + "&dateFilteringMode=" + dateFilteringMode.ToString());
            return DeserializeFromJson<List<Payment>>(resourceAsString);
        }
        public virtual PaymentsWithTotals GetReceivedPaymentsWithTotals(int pageNumber, int pageSize, DateTime? startDate, DateTime? endDate,
            string customerName, Data.PaymentStatus? status = null, DateFilteringMode? dateFilteringMode = null)
        {
            string resourceAsString = GetResource("/Payment/GetReceivedPaymentsWithTotals/?pageNumber=" + pageNumber.ToString() + "&pageSize=" + pageSize.ToString()
                + "&startDate=" + MakeDateTimeString(startDate) + "&endDate=" + MakeDateTimeString(endDate) + "&customerName=" + customerName
                + "&status=" + status.ToString() + "&dateFilteringMode=" + dateFilteringMode.ToString());
            return DeserializeFromJson<PaymentsWithTotals>(resourceAsString);
        }

        public virtual List<Payment> GetSentPayments(int pageNumber, int pageSize, DateTime? startDate, DateTime? endDate,
                    string vendorName, Data.PaymentStatus? status = null, DateFilteringMode? dateFilteringMode = null)
        {
            string resourceAsString = GetResource("/Payment/GetSentPayments/?pageNumber=" + pageNumber.ToString() + "&pageSize=" + pageSize.ToString()
                + "&startDate=" + MakeDateTimeString(startDate) + "&endDate=" + MakeDateTimeString(endDate) + "&vendorName=" + vendorName
                + "&status=" + status.ToString() + "&dateFilteringMode=" + dateFilteringMode.ToString());
            return DeserializeFromJson<List<Payment>>(resourceAsString);
        }
        public virtual PaymentsWithTotals GetSentPaymentsWithTotals(int pageNumber, int pageSize, DateTime? startDate, DateTime? endDate,
                    string vendorName, Data.PaymentStatus? status = null, DateFilteringMode? dateFilteringMode = null)
        {
            string resourceAsString = GetResource("/Payment/GetSentPaymentsWithTotals/?pageNumber=" + pageNumber.ToString() + "&pageSize=" + pageSize.ToString()
                + "&startDate=" + MakeDateTimeString(startDate) + "&endDate=" + MakeDateTimeString(endDate) + "&vendorName=" + vendorName
                + "&status=" + status.ToString() + "&dateFilteringMode=" + dateFilteringMode.ToString());
            return DeserializeFromJson<PaymentsWithTotals>(resourceAsString);
        }

        public virtual Payment GetPayment(int paymentID)
        {
            string resourceAsString = GetResource("/Payment/GetPayment/?id=" + paymentID.ToString());
            return DeserializeFromJson<Payment>(resourceAsString);
        }

        public virtual bool CancelPayment(int paymentID, string cancelReason)
        {
            string resourceAsString = GetResource("/Payment/CancelPayment/?paymentID=" + paymentID.ToString() + "&cancelReason=" + cancelReason);

            if (Boolean.TryParse(resourceAsString, out bool success))
            {
                return success;
            }

            return false;
        }
        public void UpdatePaymentStatus(int paymentID, string merchantReferenceNumber, bool success)
        {
            UpdatePaymentStatus(paymentID, merchantReferenceNumber, success, "", "");
        }

        public void UpdatePaymentStatus(int paymentID, string merchantReferenceNumber, bool success,
            string rejectionDescription = "", string rejectionCode = "")
        {
            GetResource("/Payment/UpdatePaymentStatus/?paymentID=" + paymentID.ToString() + "&merchantReferenceNumber=" + merchantReferenceNumber
                + "&success=" + success.ToString() + "&rejectionDescription=" + rejectionDescription + "&rejectionCode=" + rejectionCode);
        }
        
        /// <summary>
        /// Process online payment for an unregistered Fidesic user
        /// </summary>
        /// <param name="customer"></param>
        /// <param name="account"></param>
        /// <param name="payment"></param>
        /// <returns></returns>
        public int ProcessNonMemberPayment(Customer customer, PaymentAccount account, Payment payment)
        {
            if (payment == null) { throw new ArgumentNullException("Missing payment information."); }
            if (account == null) { throw new ArgumentNullException("Missing account information."); }
            if (customer == null) { throw new ArgumentNullException("Missing customer information."); }
            if (string.IsNullOrWhiteSpace(customer.Email))
            {
                throw new Exception("Customer email address is required.");
            }
            if (account != null && String.IsNullOrWhiteSpace(account.AccountName))
            {
                throw new Exception("A non-empty name for the Credit Card account is required.");
            }
            payment.Normalize();
            account.Normalize();
            customer.Normalize();
            string customerString = customer.SerializeToJson();
            string accountString = account.SerializeToJson();
            string paymentString = payment.SerializeToJson();
            string resourceAsString = "{\"customer\": " + customerString + ", \"account\": " + accountString + ", \"payment\": " + paymentString + "}";
            resourceAsString = PostResource("/Payment/ProcessNonMemberPayment/", resourceAsString);

            int paymentID;

            if (Int32.TryParse(resourceAsString, out paymentID))
            {
                return paymentID;
            }
            return 0;
        }

        /// <summary>
        /// Process online credit card payment without storing the card information.
        /// </summary>
        /// <param name="customer"></param>
        /// <param name="account"></param>
        /// <param name="payment"></param>
        /// <returns></returns>
        public int ProcessPaymentWithUnsavedCreditCard(CreditCardAccount creditCard, Payment payment)
        {
            if (payment == null) { throw new ArgumentNullException("Missing payment information."); }
            if (creditCard == null) { throw new ArgumentNullException("Missing account information."); }
           
            payment.Normalize();
            creditCard.Normalize();
            if (payment.Remittance == null || payment.Remittance.Count == 0)
                throw new Exception("Payment must be against an invoice");
            string creditCardString = creditCard.SerializeToJson();
            string paymentString = payment.SerializeToJson();
            string resourceAsString = "{\"creditCard\": " + creditCardString + ", \"payment\": " + paymentString + "}";
            resourceAsString = PostResource("/Payment/ProcessCreditCardPayment/", resourceAsString);

            int paymentID;

            if (Int32.TryParse(resourceAsString, out paymentID))
            {
                return paymentID;
            }
            return 0;
        }

        /// <summary>
        /// Process online payment for a registered Fidesic user.
        /// </summary>
        /// <param name="payment"></param>
        /// <returns></returns>
        public int ProcessPayment(Payment payment)
        {
            if (payment == null) { throw new ArgumentNullException("Missing payment information."); }
            payment.Normalize();
            string paymentString = payment.SerializeToJson();
            string resourceAsString = "{\"payment\": " + paymentString + "}";
            resourceAsString = PostResource("/Payment/ProcessPayment/", resourceAsString);

            int paymentID;

            if (Int32.TryParse(resourceAsString, out paymentID))
            {
                return paymentID;
            }
            return 0;
        }
        #endregion
        #endregion

        #region Helper Methods
        #region GetPostResources
        protected virtual string GetResource(string resourceUrl, bool isProtectedResource = true)
        {
            try
            {
                ProvisionSessionForRequest(isProtectedResource);

                return WebClient.GetResource(Session, resourceUrl, isProtectedResource);
            }
            catch (System.Exception exception)
            {
                System.Exception thrownException = HandleException(exception);

                if (thrownException != null)
                {
                    throw thrownException;
                }

                return String.Empty;
            }
        }
        /// <summary>
        /// Make sure you SerializeToJson(string) whatever object is passed in here.
        /// </summary>
        /// <param name="resourceUrl"></param>
        /// <param name="postData"></param>
        /// <param name="isProtectedResource"></param>
        /// <returns></returns>
        protected virtual string PostResource(string resourceUrl, string postData, bool isProtectedResource = true)
        {
            try
            {
                ProvisionSessionForRequest(isProtectedResource);

                return WebClient.PostResource(Session, resourceUrl, postData, isProtectedResource);
            }
            catch (System.Exception exception)
            {
                System.Exception thrownException = HandleException(exception);

                if (thrownException != null)
                {
                    throw thrownException;
                }

                return String.Empty;
            }
        }

        protected virtual string DeleteResource(string resourceUrl, bool isProtectedResource = true)
        {
            try
            {
                ProvisionSessionForRequest(isProtectedResource);

                return WebClient.DeleteResource(Session, resourceUrl, isProtectedResource);
            }
            catch (System.Exception exception)
            {
                System.Exception thrownException = HandleException(exception);

                if (thrownException != null)
                {
                    throw thrownException;
                }

                return String.Empty;
            }
        }

        public virtual void ConnectToFidesic(string emailAddress, string companyName)
        {
            try
            {
                ProvisionSession(emailAddress, companyName);
            }
            catch (System.Exception exception)
            {
                System.Exception thrownException = HandleException(exception);

                if (thrownException != null)
                {
                    throw thrownException;
                }
            }
        }


        public virtual void ConnectToFidesic(string emailAddress, string companyName, string password, bool allowSwitch = false)
        {
            try
            {
                ProvisionSession(emailAddress, companyName, password, allowSwitch);
            }
            catch (System.Exception exception)
            {
                System.Exception thrownException = HandleException(exception);

                if (thrownException != null)
                {
                    throw thrownException;
                }
            }
        }

        public virtual void DisconnectFromFidesic()
        {
            WebClient = null;
            Session = null;

            ClearSessionFromEnvironment();
        }

        #endregion

        #region "Infrastructure Methods"
        protected virtual bool RestoreSessionFromEnvironment()
        {
            string email = DetermineEmailFromEnvironment();
            string companyName = DetermineCompanyNameFromEnvironment();
            string accessToken = DetermineAccessTokenFromEnvironment();

            if (!String.IsNullOrWhiteSpace(email) && !String.IsNullOrWhiteSpace(accessToken)) // CompanyName is allowed to be null...for now...
            {
                WebClient = new FidesicWebClient(ApiSettings);
                Session = WebClient.RestoreSession(email, companyName, accessToken);

                return true;
            }
            else
            {
                return false;
            }
        }

        protected abstract void PersistSessionWithinEnvironment();

        protected virtual void ClearSessionFromEnvironment()
        {
            //throw new NotImplementedException();
        }

        // For the base FidesicController class (which is meant to be applicable within any type of application (mobile, desktop, web, etc.),
        // reading and writing the 
        protected abstract string DetermineEmailFromEnvironment();
        protected abstract string DetermineCompanyNameFromEnvironment();
        protected abstract string DetermineAccessTokenFromEnvironment();

        /// <summary>Provisions the FidesicSession by attempting to restore an existing session from environment variables. If an existing session 
        /// does not yet exist, a new one will NOT be created and an exception will be thrown.</summary>
        /// <remarks>This overload of the ProvisionSession method does not accept email address or company name as parameters so a new FidesicSession 
        /// cannot be established but calling it. If an existing session cannot be restored from environment variables, an exception will be thrown 
        /// informing that the ConnectToFidesic method will need to be called before any request for a resource can be performed.</remarks>
        protected virtual void ProvisionSessionForRequest(bool isProtectedResource = true)
        {
            if (Session == null)
            {
                if (WebClient == null)
                {
                    WebClient = new FidesicWebClient(ApiSettings);
                }

                // First, attempt to restore the session from environment variables.
                RestoreSessionFromEnvironment();
                if (!isProtectedResource)
                {
                    Session = WebClient.CreateSession(new Guid().ToString(), new Guid().ToString());
                }
                // If Session is still null, throw an exception since 
                if (Session == null)
                {
                    throw new NotConnectedToFidesicException("A valid session has not been established with Fidesic. Call the \"ConnectToFidesic\" method to connect to Fidesic and establish a session.");
                }
            }
        }

        protected virtual void ProvisionSession(string emailAddress, string companyName)
        {
            if (Session == null)
            {
                if (WebClient == null)
                {
                    WebClient = new FidesicWebClient(ApiSettings);
                }

                // First, attempt to restore the session from environment variables.
                RestoreSessionFromEnvironment();

                // If Session is still null, create a new one (which will cause a re-authentication against the OAuth services).
                if (Session == null)
                {
                    if (ApiSettings.ConsumerIsTrusted)
                    {
                        throw new FidesicException("The Fidesic API is configured to allow the consumer application to retain the user's password and broker the entire authentication flow with the Fidesic OAuth services on behalf of the user. No user password has been provided. If a user's password cannot be provided, consider setting the ConsumerIsTrusted property of the FidesicApiSettings to false.");
                    }

                    Session = WebClient.CreateSession(emailAddress, companyName);

                    PersistSessionWithinEnvironment();
                }
            }
        }
        protected virtual void ProvisionSession(string emailAddress, string companyName, string password, bool allowSwitch = false)
        {
            if (Session == null || allowSwitch)
            {
                if (WebClient == null)
                {
                    WebClient = new FidesicWebClient(ApiSettings);
                }

                // First, attempt to restore the session from environment variables.
                RestoreSessionFromEnvironment();

                // If Session is still null, create a new one (which will cause a re-authentication against the OAuth services).
                if (Session == null || allowSwitch)
                {
                    if (!ApiSettings.ConsumerIsTrusted)
                    {
                        Session = WebClient.CreateSession(emailAddress, companyName);
                    }
                    else
                    {
                        Session = WebClient.CreateSession(emailAddress, companyName, password);
                    }


                    PersistSessionWithinEnvironment();
                }
            }
        }

        protected virtual System.Exception HandleException(System.Exception exception)
        {
            if (exception is FidesicSessionExpiredException)
            {
                DisconnectFromFidesic();
            }

            return exception;
        }

        private decimal MakeDec(decimal? input, int precision)
        {

            try
            {
                decimal retval = Convert.ToDecimal(input);
                string sdec = Convert.ToString(retval);
                int IOf = sdec.IndexOf(".");
                if (IOf > 0)
                {
                    sdec += "0000";
                    sdec = sdec.Substring(0, IOf + precision + 2);
                    retval = Convert.ToDecimal(sdec);
                    return Decimal.Round(retval, precision);
                    //return retval;
                }
                else
                {
                    sdec += ".0000";
                    sdec = sdec.Substring(0, sdec.IndexOf(".") + precision + 1);
                    return Convert.ToDecimal(sdec);
                }
            }
            catch
            {
                return Convert.ToDecimal(0.00);
            }

        }
        private string MakeDateTimeString(DateTime? input)
        {
            if (input.HasValue)
            {
                return ((DateTime)input).ToString("s", System.Globalization.CultureInfo.InvariantCulture);
            }
            else
            {
                return "";
            }  
        }
        private string SerializeToJson(object var)
        {
            var settings = new JsonSerializerSettings
            {
                DateFormatHandling = DateFormatHandling.MicrosoftDateFormat
            };
            return JsonConvert.SerializeObject(var, settings);
        }

        private IDataObject DeserializeFromJson(string rawData)
        {

            return (IDataObject)Newtonsoft.Json.JsonConvert.DeserializeObject(rawData);
        }


        private T DeserializeFromJson<T>(string rawData)
        {
            return Newtonsoft.Json.JsonConvert.DeserializeObject<T>(rawData);
        }
        #endregion

        #endregion
        //------\\ Methods //-----------------------------------------------//
    }
}