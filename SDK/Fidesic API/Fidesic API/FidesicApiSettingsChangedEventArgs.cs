﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Fidesic
{
    public class FidesicApiSettingsChangedEventArgs : EventArgs
    {
        //------// Properties \\--------------------------------------------\\
        private string _settingName = null;
        public virtual string SettingName
        {
            get { return _settingName; }
            set { _settingName = value; }
        }

        
        private object _settingValue = null;
        public virtual object SettingValue
        {
            get { return _settingValue; }
            set { _settingValue = value; }
        }


        private FidesicApiSettings _settings = null;
        public FidesicApiSettings Settings
        {
            get { return _settings; }
            set { _settings = value; }
        }        


        public static FidesicApiSettingsChangedEventArgs Empty
        {
            get {  return new FidesicApiSettingsChangedEventArgs(); }
        }
        //------\\ Properties //--------------------------------------------//



        //------// Constructors \\------------------------------------------\\
        public FidesicApiSettingsChangedEventArgs()
            : this(null, null, null)
        { }


        public FidesicApiSettingsChangedEventArgs(string settingName, object settingValue)
            : this(settingName, settingValue, null)
        { }


        public FidesicApiSettingsChangedEventArgs(string settingName, object settingValue, FidesicApiSettings settings)
        {
            SettingName = settingName;
            SettingValue = settingValue;
            Settings = settings;
        }
        //------\\ Constructors //------------------------------------------//



        //------// Methods \\-----------------------------------------------\\
        
        //------\\ Methods //-----------------------------------------------//
    }
}