﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using DevDefined.OAuth;
using DevDefined.OAuth.Consumer;
using DevDefined.OAuth.Framework;

namespace Fidesic.Web
{
    public abstract class FidesicWebController : FidesicController
    {
        //------// Properties \\--------------------------------------------\\
        public override bool ConnectedToFidesic
        {
            get
            {
                if (!FidesicAuthCookieExists())
                {
                    LogoutCurrentUser();
                    return false;
                }

                return true;
            }
        }


        public virtual bool LoggedInToEnvironment
        {
            get { return (HttpContext.Current != null && HttpContext.Current.Request != null && HttpContext.Current.Request.IsAuthenticated); }
        }


        protected virtual string FidesicSessionKey
        {
            get { return "FidesicSession.CurrentUser"; }
        }
        //------\\ Properties //--------------------------------------------//



        //------// Constructors \\------------------------------------------\\
        public FidesicWebController(FidesicApiSettings apiSettings)
            : base(apiSettings)
        { }


        public FidesicWebController(FidesicApiSettings apiSettings, bool automaticallyReconnectOnChange)
            : base(apiSettings, automaticallyReconnectOnChange)
        { }
        //------\\ Constructors //------------------------------------------//



        //------// Methods \\-----------------------------------------------\\
        protected virtual void PersistFidesicAuthCookie(IToken accessToken, long expires, string username)
        {
            HttpCookie authCookie = HttpContext.Current.Request.Cookies[System.Web.Security.FormsAuthentication.FormsCookieName];

            if (authCookie != null)
            {
                HttpContext.Current.Response.Cookies.Remove(System.Web.Security.FormsAuthentication.FormsCookieName);

                System.Web.Security.FormsAuthenticationTicket formsTicket = new System.Web.Security.FormsAuthenticationTicket(0, username, DateTime.Now, DateTime.Now.AddMinutes(expires), true, "fidesic_access=" + accessToken.ToString(), System.Web.Security.FormsAuthentication.FormsCookiePath);

                string encryptedTicket = System.Web.Security.FormsAuthentication.Encrypt(formsTicket);

                HttpContext.Current.Response.Cookies.Add(new HttpCookie(System.Web.Security.FormsAuthentication.FormsCookieName, encryptedTicket));
                AddFidesicConnectCookie(expires);
            }
        }


        protected virtual bool FidesicAuthCookieExists()
        {
            HttpCookie authCookie = HttpContext.Current.Request.Cookies[System.Web.Security.FormsAuthentication.FormsCookieName];

            if (authCookie != null && !String.IsNullOrWhiteSpace(authCookie.Value) /*&& authCookie.Expires > DateTime.Now*/)
            {
                System.Web.Security.FormsAuthenticationTicket decryptedTicket = System.Web.Security.FormsAuthentication.Decrypt(authCookie.Value);

                if (decryptedTicket.UserData != null && decryptedTicket.UserData.StartsWith("fidesic_access="))
                {
                    return true;
                }
            }

            return false;
        }


        protected virtual bool FidesicAuthCookieExists(out string accessToken)
        {
            try
            {
                HttpCookie authCookie = HttpContext.Current.Request.Cookies[System.Web.Security.FormsAuthentication.FormsCookieName];

                if (authCookie != null && !String.IsNullOrWhiteSpace(authCookie.Value))
                {
                    System.Web.Security.FormsAuthenticationTicket decryptedTicket = System.Web.Security.FormsAuthentication.Decrypt(authCookie.Value);

                    if (decryptedTicket.UserData != null && decryptedTicket.UserData.StartsWith("fidesic_access="))
                    {
                        accessToken = decryptedTicket.UserData.Replace("fidesic_access=", String.Empty);
                        return true;
                    }
                }
                accessToken = null;
                return false;
            }
            catch
            {
                accessToken = null;
                return false;
            }
        }


        protected virtual void DeleteFidesicAuthCookie()
        {
            if (HttpContext.Current.Request.Cookies.AllKeys.Contains(System.Web.Security.FormsAuthentication.FormsCookieName))
            {
                HttpContext.Current.Request.Cookies.Remove(System.Web.Security.FormsAuthentication.FormsCookieName);
                HttpContext.Current.Response.Cookies.Remove(System.Web.Security.FormsAuthentication.FormsCookieName);
                string currentUserEmail = GetCurrentUsername();

                if (!String.IsNullOrWhiteSpace(currentUserEmail) && !ConnectedToFidesic)
                {
                    // This is the case for when a user disconnects from Fidesic. The fidesic_connect cookie will not be there to instruct the user to re-login with Fidesic.

                    System.Web.Security.FormsAuthenticationTicket formsTicket = new System.Web.Security.FormsAuthenticationTicket(0, currentUserEmail, DateTime.Now, DateTime.Now.Add(System.Web.Security.FormsAuthentication.Timeout), true, String.Empty, System.Web.Security.FormsAuthentication.FormsCookiePath);

                    string encryptedTicket = System.Web.Security.FormsAuthentication.Encrypt(formsTicket);

                    HttpContext.Current.Response.Cookies.Add(new HttpCookie(System.Web.Security.FormsAuthentication.FormsCookieName, encryptedTicket));
                }
                else
                {
                    LogoutCurrentUser();
                }
            }
        }


        public virtual void DisconnectFromFidesic(bool logoutUser)
        {
            DisconnectFromFidesic();

            if (logoutUser)
            {
                LogoutCurrentUser();
            }
        }


        public override void DisconnectFromFidesic()
        {
            WebClient = null;
            Session = null;

            ClearSessionFromEnvironment();

            if (FidesicConnectCookieExists())
            {
                DeleteFidesicConnectCookie();
            }

            if (FidesicAuthCookieExists())
            {
                DeleteFidesicAuthCookie();
            }

            ClearFidesicSessionStorage();
        }


        public virtual bool FidesicConnectCookieExists()
        {
            return HttpContext.Current.Request.Cookies.AllKeys.Contains("fidesic_connect");
        }


        protected virtual void AddFidesicConnectCookie(long expires)
        {
            HttpContext.Current.Response.Cookies.Add(new HttpCookie("fidesic_connect", DateTime.Now.ToString()) { Expires = DateTime.Now.AddMinutes(expires) });
        }


        public virtual void DeleteFidesicConnectCookie()
        {
            HttpContext.Current.Request.Cookies.Remove("fidesic_connect");
            HttpContext.Current.Response.Cookies.Add(new HttpCookie("fidesic_connect", DateTime.Now.ToString()) { Expires = DateTime.Now.AddDays(-1d) });
        }


        protected virtual void LoginUser(string currentUsername)
        {
            System.Web.Security.FormsAuthentication.SetAuthCookie(currentUsername, false);
        }


        protected virtual void LogoutCurrentUser()
        {
            System.Web.Security.FormsAuthentication.SignOut();
        }


        protected virtual string GetCurrentUsername()
        {
            if (HttpContext.Current.User != null && HttpContext.Current.User.Identity != null)
            {
                return HttpContext.Current.User.Identity.Name;
            }

            return null;
        }


        protected override void ProvisionSessionForRequest(bool isProtectedResource = true)
        {
            try
            {
                base.ProvisionSessionForRequest(isProtectedResource);

                if (Session == null && LoggedInToEnvironment)
                {
                    DisconnectFromFidesic(false); // Disconnect but don't logout.
                }
            }
            catch (System.Exception exception)
            {
                DisconnectFromFidesic(true);
                throw exception;
            }
        }


        protected override bool RestoreSessionFromEnvironment()
        {
            FidesicWebClient.FidesicSession session = null;

            // Check the ASP.NET Session for an Fidesic Session object.
            if (HttpContext.Current != null)
            {
                session = (FidesicWebClient.FidesicSession)HttpContext.Current.Session[FidesicSessionKey];

                if (session != null && !FidesicAuthCookieExists() && session.AccessToken != null && !String.IsNullOrWhiteSpace(session.AccessToken.Token))
                {
                    // TODO: Pull expiration of cookie from settings.
                    PersistFidesicAuthCookie(session.AccessToken, 30, session.Email);
                }
            }

            if (session != null)
            {
                Session = session;
                return true;
            }
            else
            {
                return base.RestoreSessionFromEnvironment();
            }
        }


        protected override void PersistSessionWithinEnvironment()
        {
            if (base.Session != null && HttpContext.Current != null && base.Session.AccessToken != null && !String.IsNullOrWhiteSpace(base.Session.AccessToken.Token))
            {
                HttpContext.Current.Session[FidesicSessionKey] = base.Session;
                // TODO: Pull expiration of cookie from settings.
                PersistFidesicAuthCookie(base.Session.AccessToken, 30, base.Session.Email);
            }
        }


        protected virtual void ClearFidesicSessionStorage()
        {
            HttpContext.Current.Session[FidesicSessionKey] = null;
        }


        protected override string DetermineAccessTokenFromEnvironment()
        {
            string accessToken = null;

            if (!FidesicAuthCookieExists(out accessToken))
            {
                return null;
            }

            return accessToken;
        }


        protected override System.Exception HandleException(System.Exception exception)
        {
            if (exception is OAuthException)
            {
                HttpContext.Current.Session.Remove(FidesicSession_CacheKey);
            }


            return base.HandleException(exception);
        }
        //------\\ Methods //-----------------------------------------------//
    }
}