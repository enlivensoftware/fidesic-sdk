﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Fidesic
{
    public class PasswordRequiredException : FidesicException
    {
        //------// Properties \\--------------------------------------------\\

        //------\\ Properties //--------------------------------------------//



        //------// Constructors \\------------------------------------------\\
        public PasswordRequiredException()
        { }


        public PasswordRequiredException(string message)
            : base(message)
        { }


        public PasswordRequiredException(string message, Exception inner)
            : base(message, inner)
        { }


        protected PasswordRequiredException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        { }
        //------\\ Constructors //------------------------------------------//



        //------// Methods \\-----------------------------------------------\\

        //------\\ Methods //-----------------------------------------------//
    }
}
