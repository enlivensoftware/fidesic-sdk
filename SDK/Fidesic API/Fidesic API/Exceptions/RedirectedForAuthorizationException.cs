﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Fidesic
{
    public class RedirectedForAuthorizationException : FidesicException
    {
        //------// Properties \\--------------------------------------------\\

        //------\\ Properties //--------------------------------------------//



        //------// Constructors \\------------------------------------------\\
        public RedirectedForAuthorizationException()
        { }


        public RedirectedForAuthorizationException(string message)
            : base(message)
        { }


        public RedirectedForAuthorizationException(string message, Exception inner)
            : base(message, inner)
        { }


        protected RedirectedForAuthorizationException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        { }
        //------\\ Constructors //------------------------------------------//



        //------// Methods \\-----------------------------------------------\\

        //------\\ Methods //-----------------------------------------------//
    }
}
