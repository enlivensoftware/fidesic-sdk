﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Fidesic
{
    public class FidesicLoginException : FidesicException
    {
        //------// Properties \\--------------------------------------------\\

        //------\\ Properties //--------------------------------------------//



        //------// Constructors \\------------------------------------------\\
        public FidesicLoginException()
        { }


        public FidesicLoginException(string message)
            : base(message)
        { }


        public FidesicLoginException(string message, Exception inner)
            : base(message, inner)
        { }


        protected FidesicLoginException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        { }
        //------\\ Constructors //------------------------------------------//



        //------// Methods \\-----------------------------------------------\\

        //------\\ Methods //-----------------------------------------------//
    }
}
