﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Fidesic
{
    public class FidesicException : System.Exception, ISerializable
    {
        //------// Properties \\--------------------------------------------\\

        //------\\ Properties //--------------------------------------------//



        //------// Constructors \\------------------------------------------\\
        public FidesicException()
        { }


        public FidesicException(string message)
            : base(message)
        { }


        public FidesicException(string message, Exception inner)
            : base(message, inner)
        { }


        protected FidesicException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        { }
        //------\\ Constructors //------------------------------------------//



        //------// Methods \\-----------------------------------------------\\

        //------\\ Methods //-----------------------------------------------//
    }
}
