﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Fidesic
{
    public class FidesicSessionExpiredException : FidesicException
    {
        //------// Properties \\--------------------------------------------\\

        //------\\ Properties //--------------------------------------------//



        //------// Constructors \\------------------------------------------\\
        public FidesicSessionExpiredException()
        { }


        public FidesicSessionExpiredException(string message)
            : base(message)
        { }


        public FidesicSessionExpiredException(string message, Exception inner)
            : base(message, inner)
        { }


        protected FidesicSessionExpiredException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        { }
        //------\\ Constructors //------------------------------------------//



        //------// Methods \\-----------------------------------------------\\

        //------\\ Methods //-----------------------------------------------//
    }
}
