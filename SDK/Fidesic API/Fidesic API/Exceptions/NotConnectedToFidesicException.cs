﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Fidesic
{
    public class NotConnectedToFidesicException : FidesicException
    {
        //------// Properties \\--------------------------------------------\\

        //------\\ Properties //--------------------------------------------//



        //------// Constructors \\------------------------------------------\\
        public NotConnectedToFidesicException()
        { }


        public NotConnectedToFidesicException(string message)
            : base(message)
        { }


        public NotConnectedToFidesicException(string message, Exception inner)
            : base(message, inner)
        { }


        protected NotConnectedToFidesicException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        { }
        //------\\ Constructors //------------------------------------------//



        //------// Methods \\-----------------------------------------------\\

        //------\\ Methods //-----------------------------------------------//
    }
}
