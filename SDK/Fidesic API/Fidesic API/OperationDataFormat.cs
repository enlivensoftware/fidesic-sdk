﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Fidesic
{
    public enum OperationDataFormat : int
    {
        Json = 1,
        Xml = 2
    }
}