﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Fidesic.Data
{
    [Serializable]
    public class CreateUserData : DataObject
    {
        //------// Properties \\--------------------------------------------\\
        public virtual FidesicUser UserInfo { get; set; }
        public virtual string Password { get; set; }
        public virtual string SecretQuestion { get; set; }
        public virtual string SecretAnswer { get; set; }
        public virtual string TaxID { get; set; }
        //------\\ Properties //--------------------------------------------//



        //------// Constructors \\------------------------------------------\\
        public CreateUserData()
        { }
        //------\\ Constructors //------------------------------------------//



        //------// Methods \\-----------------------------------------------\\
        public override void Normalize()
        {
            if (UserInfo == null) { UserInfo = FidesicUser.Empty; }
            if (TaxID == null) { TaxID = String.Empty; }
        }
        //------\\ Methods //-----------------------------------------------//
    }
}