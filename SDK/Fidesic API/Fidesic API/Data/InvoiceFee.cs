using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Xml.Serialization;

namespace Fidesic.Data
{
    [Serializable]
    public class InvoiceFee
    {
        //------// Properties \\--------------------------------------------\\
        public virtual int? FeeID { get; set; }
        public virtual int ClientUserID { get; set; }
        public virtual string PartnerName { get; set; }
        public virtual string PartnerCode { get; set; }
        public virtual string AccountName { get; set; }
        public virtual string AccountNumber { get; set; }
        public virtual string AccountType { get; set; }
        public virtual string BankCode { get; set; }
        public virtual string BranchSortCode { get; set; }
        public virtual decimal InvoiceFeeAmount { get; set; }
        public virtual bool isPrinciple { get; set; }
        /// <summary>
        /// DEPRACATED. Field will be removed in future versions.
        /// </summary>
        public virtual bool customerBearsFee { get; set; }
        //------\\ Properties //--------------------------------------------//
        


        //------// Constructors \\------------------------------------------\\
        public InvoiceFee()
        { }
        //------\\ Constructors //------------------------------------------//



        //------// Methods \\-----------------------------------------------\\
        
        //------\\ Methods //-----------------------------------------------//
    }
}