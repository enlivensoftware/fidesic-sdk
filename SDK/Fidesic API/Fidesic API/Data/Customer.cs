using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Fidesic.Data
{
    [Serializable]
    public class Customer : Contact
    {
        //------// Properties \\--------------------------------------------\\
        public virtual string CustomerNumber { get; set; }

        //------\\ Properties //--------------------------------------------//



        //------// Constructors \\------------------------------------------\\
        public Customer()
        { }
        //------\\ Constructors //------------------------------------------//



        //------// Methods \\-----------------------------------------------\\
        
        //------\\ Methods //-----------------------------------------------//
    }
}