using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Fidesic.Data
{
    [Serializable]
    public class VendorGLCode : GLCode
    {
        //------// Properties \\--------------------------------------------\\
        public GLType GLTypeID { get; set; }


        //------\\ Properties //--------------------------------------------//

        //------// Constructors \\------------------------------------------\\
        public VendorGLCode()
        { }
        //------\\ Constructors //------------------------------------------//

        //------\\ Methods //-----------------------------------------------//
    }
}