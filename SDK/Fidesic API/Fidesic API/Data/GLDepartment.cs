using System;

namespace Fidesic.Data
{
    [Serializable]
    public class GLDepartment : DataObject
    {
        public int ID { get; set; }
        /// <summary>
        /// GL Department description field. 
        /// Only item that gets set for Quickbooks.
        /// </summary>
        public string FullGLDepartment { get; set; }
        /// <summary>
        /// GL Department value field. 
        /// Not used with Quickbooks.
        /// </summary>
        public string Value { get; set; }

        public GLDepartment()
        { }
    }
}