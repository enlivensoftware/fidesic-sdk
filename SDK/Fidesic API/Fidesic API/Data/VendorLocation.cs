using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Fidesic.Data
{
    [Serializable]
    public class VendorLocation : DataObject
    {
        //------// Properties \\--------------------------------------------\\
        public int LocationID { get; set; }
        public string LocationName { get; set; }
        public string LocationCode { get; set; }
        public DateTime LastModified { get; set; }


        //------\\ Properties //--------------------------------------------//

        //------// Constructors \\------------------------------------------\\
        public VendorLocation()
        { }
        //------\\ Constructors //------------------------------------------//

        //------\\ Methods //-----------------------------------------------//
    }
}