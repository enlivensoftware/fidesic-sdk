using System;

namespace Fidesic.Data
{
    [Serializable]
    public class GLCode : DataObject
    {
        public int? GLCodeID { get; set; }
        public string FullGLCode { get; set; }
        public string FriendlyName { get; set; }
        public string ExternalID { get; set; }
        public bool Active { get; set; } = true;
        public GLType Type { get; set; } = GLType.Purchasing;

        public GLCode()
        { }
    }
}