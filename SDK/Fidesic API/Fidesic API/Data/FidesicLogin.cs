﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Fidesic.Data
{
    [Serializable]
    public class FidesicLogin : DataObject<int>
    {
        public virtual int UserID
        {
            get { return base.Key; }
            set { base.Key = value; }
        }
        public virtual string LoginName { get; set; }
        public virtual string CompanyName { get; set; }
        public virtual ContactType ContactType { get; set; }
        public virtual int PendingInvoiceApprovals { get; set; }
        public virtual int PendingPaymentApprovals { get; set; }
    }
}