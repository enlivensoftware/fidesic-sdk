using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Xml.Serialization;

namespace Fidesic.Data
{
    [Serializable]
    public class InvoiceItem : DataObject<int>
    {
        //------// Properties \\--------------------------------------------\\
        public virtual int InvoiceItemID
        {
            get { return base.Key; }
            set { base.Key = value; }
        }


        public virtual int LineNumber { get; set; }
        public virtual int InvoiceID { get; set; }
        public virtual string GLCode { get; set; }
        public virtual string ItemCode { get; set; }
        public virtual string Name { get; set; }
        public virtual string Description { get; set; }
        public virtual decimal? UnitPrice { get; set; }
        public virtual decimal? ExtendedPrice { get; set; }
        public virtual string RevenueCode { get; set; }
        public virtual string RevenueName { get; set; }
        public virtual List<InvoiceFee> fees { get; set; }

        private decimal? _quantity = 1;
        public virtual decimal? Quantity
        {
            get
            {
                if (!_quantity.HasValue || _quantity.Value <= 1)
                {
                    return 1;
                }

                return _quantity;
            }
            set { _quantity = value; }
        }


        private bool _saveAsTemplate = false;
        [XmlIgnore] [ScriptIgnore] public virtual bool SaveAsTemplate
        {
            get { return _saveAsTemplate; }
            set { _saveAsTemplate = value; }
        }
        //------\\ Properties //--------------------------------------------//

        //------// Constructors \\------------------------------------------\\
        public InvoiceItem()
        { }
        //------\\ Constructors //------------------------------------------//

        

        //------// Methods \\-----------------------------------------------\\
        public override void Normalize()
        {
            if (GLCode == null) { GLCode = String.Empty; }
            if (ItemCode == null) { ItemCode = String.Empty; }
            if (Name == null) { Name = String.Empty; }
            if (Description == null) { Description = String.Empty; }
            if (!Quantity.HasValue) { Quantity = 0; }
            if (!UnitPrice.HasValue) { UnitPrice = 0; }
            if (!ExtendedPrice.HasValue) { ExtendedPrice = 0; }
        }
        //------\\ Methods //-----------------------------------------------//
    }
}