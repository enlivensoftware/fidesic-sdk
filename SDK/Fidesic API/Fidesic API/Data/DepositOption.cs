﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Fidesic.Data
{
    public enum DepositOption : int
    {
        /// <summary>
        /// Payments that process thru Fidesic
        /// </summary>
        Invalid = -1,
        Fidesic = 0,
        CashReceipt = 3,
        PointOfSale = 5,
        Teller = 6,
        Kiosk = 7

    }
}