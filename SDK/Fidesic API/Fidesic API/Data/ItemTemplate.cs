using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Fidesic.Data
{
    [Serializable]
    public class ItemTemplate : DataObject<int?>
    {
        //------// Properties \\--------------------------------------------\\
        public virtual int? ItemTemplateID
        {
            get { return base.Key; }
            set { base.Key = value; }
        }


        public virtual int UserID { get; set; }
        public virtual string GLCode { get; set; }
        public virtual string ItemCode { get; set; }
        public virtual string Name { get; set; }
        public virtual string Description { get; set; }
        public virtual decimal? UnitPrice { get; set; }


        public static ItemTemplate Empty
        {
            get
            {
                ItemTemplate empty = new ItemTemplate();
                empty.Normalize();

                return empty;
            }
        }
        //------\\ Properties //--------------------------------------------//



        //------// Constructors \\------------------------------------------\\
        public ItemTemplate()
        { }
        //------\\ Constructors //------------------------------------------//



        //------// Methods \\-----------------------------------------------\\
        public override void Normalize()
        {
            if (!ItemTemplateID.HasValue) { ItemTemplateID = 0; }
            if (GLCode == null) { GLCode = String.Empty; }
            if (ItemCode == null) { ItemCode = String.Empty; }
            if (Name == null) { Name = String.Empty; }
            if (Description == null) { Description = String.Empty; }
            if (!UnitPrice.HasValue) { UnitPrice = 0; }
        }
        //------\\ Methods //-----------------------------------------------//
    }
}