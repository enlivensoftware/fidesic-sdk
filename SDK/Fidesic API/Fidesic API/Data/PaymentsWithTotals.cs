﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Fidesic.Data
{
    [Serializable]
    public class PaymentsWithTotals
    {
        /// <summary>
        /// Paginated result of payments
        /// </summary>
        public virtual List<Payment> Payments { get; set; }
        /// <summary>
        /// Number of pages of payments available with current method and filters 
        /// </summary>
        public virtual int PageCount { get; set; }
        /// <summary>
        /// Total number of payments available with current method and filters 
        /// </summary>
        public virtual int TotalCount { get; set; }
        /// <summary>
        /// Total amount of payments available with current method and filters
        /// </summary>
        public virtual decimal TotalAmount { get; set; }
    }
}