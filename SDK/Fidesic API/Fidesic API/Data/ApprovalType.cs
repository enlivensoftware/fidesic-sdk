﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Fidesic.Data
{
    public enum ApprovalType : int
    {
        Invoice = 1,
        Payment = 2
    }
}