﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Fidesic.Data
{
    public enum ApprovalStatus : int
    {
        Approve = 1,
        Disapprove = 2
    }
}