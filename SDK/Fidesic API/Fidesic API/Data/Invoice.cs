using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Xml.Serialization;



namespace Fidesic.Data
{
    [Serializable]
    public class Invoice : DataObject<int?>
    {
        //------// Properties \\--------------------------------------------\\
        public virtual int? InvoiceID
        {
            get { return base.Key; }
            set { base.Key = value; }
        }

        public virtual int CustomerID { get; set; }
        public virtual int VendorID { get; set; }

        public virtual string InvoiceNumber { get; set; }
        public virtual DateTime InvoiceDate { get; set; }
        public virtual string Term { get; set; }
        public virtual DateTime DueDate { get; set; }
        public virtual DateTime CreatedDate { get; set; }
        //[XmlIgnore]
        //[ScriptIgnore]
        public virtual Customer Customer { get; set; }
        public virtual Vendor Vendor { get; set; }
        public virtual string Description { get; set; }
        public virtual string Memo { get; set; }
        public virtual InvoiceStatus Status { get; set; }
        public virtual decimal Amount { get; set; }
        public virtual decimal UnpaidAmount { get; set; }
        /// <summary>
        /// DEPRACATED. USE VendorLocation object instead!
        /// </summary>
        public virtual string VendorLocationName { get; set; }
        /// <summary>
        /// DEPRACATED. USE VendorLocation object instead!
        /// </summary>
        public virtual int? VendorLocationID { get; set; }
        public virtual VendorLocation VendorLocation { get; set; }
        public virtual InvoiceType InvoiceType { get; set; }
        public virtual List<InvoiceItem> LineItems { get; set; }
        public virtual List<InvoiceFee> Fees { get; set; }
        public virtual List<InvoiceGLCode> GLCodes { get; set; }
        public virtual List<Remittance> Remittance { get; set; }
        public virtual string CurrencyCode { get; set; }
        public virtual Data.InvoiceCategory Category { get; set; }
        public virtual List<Attachment> Attachments { get; set; }
        /// <summary>
        /// will be null if method does not check value.
        /// </summary>
        public virtual bool? canApprove { get; set; }
        public virtual string PONumber { get; set; }
        /// <summary>
        /// if unapproved, the next approver in workflow
        /// </summary>
        public virtual string NextApproverName { get; set; }
        //------\\ Properties //--------------------------------------------//



        //------// Constructors \\------------------------------------------\\
        public Invoice()
        { }
        //------\\ Constructors //------------------------------------------//



        //------// Methods \\-----------------------------------------------\\
        
        //------\\ Methods //-----------------------------------------------//
    }
}