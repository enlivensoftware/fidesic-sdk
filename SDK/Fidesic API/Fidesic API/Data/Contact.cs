using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Fidesic.Data
{
    [Serializable]
    public class Contact : FidesicUser
    {
        //------// Properties \\--------------------------------------------\\
        public virtual ContactType ContactType { get; set; }
        public virtual string ContactClass { get; set; }
        public virtual string AddressCode { get; set; }
        //------\\ Properties //--------------------------------------------//



        //------// Constructors \\------------------------------------------\\
        public Contact()
        { }
        //------\\ Constructors //------------------------------------------//



        //------// Methods \\-----------------------------------------------\\
        
        //------\\ Methods //-----------------------------------------------//
    }
}