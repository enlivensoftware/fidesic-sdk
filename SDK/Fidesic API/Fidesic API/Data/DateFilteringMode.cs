﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Fidesic.Data
{
    public enum DateFilteringMode : int
    {
        /// <summary>
        /// Invoice or Payment
        /// </summary>
        DueDate,
        /// <summary>
        /// Invoice Only
        /// </summary>
        InvoiceDate,
        /// <summary>
        /// Invoice or Payment
        /// </summary>
        CreatedDate,
        /// <summary>
        /// Payment Only
        /// </summary>
        ProcessingDate
    }
}