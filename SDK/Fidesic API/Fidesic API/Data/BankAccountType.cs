using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Fidesic.Data
{
    public enum BankAccountType : int
    {
        Checking = 0,
        Savings = 1,
		Unknown = 2
    }
}