﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace Fidesic.Data
{
    public interface IDataObject
    {
        //------// Properties \\--------------------------------------------\\
        object Key { get; set; }
        //------\\ Properties //--------------------------------------------//



        //------// Methods \\-----------------------------------------------\\
        void Normalize();
        string SerializeToXml(bool indentOutput);
        IDataObject DeserializeFromXml(string rawData);
        string SerializeToJson();
        IDataObject DeserializeFromJson(string rawData);
        //------\\ Methods //-----------------------------------------------//
    }
}
