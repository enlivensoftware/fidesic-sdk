﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Fidesic.Data
{
    [Serializable]
    public class Permissions
    {
        #region RolePermissions

        #region Banking Module
        /// <summary>
        /// User has any banking access
        /// </summary>
        public bool BankingAccess { get; set; }
        /// <summary>
        /// User can edit bank/credit card accounts
        /// </summary>
        public bool BankingManageAccounts { get; set; }
        /// <summary>
        /// User can view bank/credit card accounts
        /// </summary>
        public bool BankingViewAll { get; set; }
        #endregion
        #region Receivables Module
        /// <summary>
        /// User has any receivables access
        /// </summary>
        public bool ReceivablesAccess { get; set; }
        public bool ReceivablesViewCustomers { get; set; }
        //public bool ReceivablesDiscuss { get; set; }
        /// <summary>
        /// Receivables Invoice - Process Sent Invoices, Create invoices, 
        /// Add/Edit/Delete Receivables, Process received Purchase Orders
        /// </summary>
        public bool ReceivablesSendInvoice { get; set; }
        /// <summary>
        /// Only available for ContactType of Vendor if enabled.
        /// Send an expense report to MasterAccount
        /// </summary>
        public bool ReceivablesSubmitExpense { get; set; }
        public bool ReceivablesViewInvoices { get; set; }
        public bool ReceivablesViewPayments { get; set; }
        #endregion
        #region Payables Module
        /// <summary>
        /// User has any payables access
        /// </summary>
        public bool PayablesAccess { get; set; }
        //public bool PayablesTasks { get; set; }
        /// <summary>
        /// Can Pay invoices
        /// </summary>
        public bool PayablesPay { get; set; }
        /// <summary>
        /// Can Approve payables payments
        /// </summary>
        public bool PayablesApprovePayments { get; set; }
        /// <summary>
        /// Can Approve payables invoices
        /// </summary>
        public bool PayablesApproveInvoices { get; set; }
        public bool PayablesVoidInvoices { get; set; }
        public bool PayablesAdjustApprovedInvoices { get; set; }
        //public bool PayablesDiscuss { get; set; }
        //public bool PayablesSendPO { get; set; }
        public bool PayablesSubmitInvoice { get; set; }
        public bool PayablesViewInvoices { get; set; }
        public bool PayablesViewPayments { get; set; }
        //public bool PayablesAddInvoiceApprovers { get; set; }
        //public bool PayablesDeleteInvoiceApprovers { get; set; }
        //public bool PayablesAddPaymentApprovers { get; set; }
        //public bool PayablesDeletePaymentApprovers { get; set; }
        /// <summary>
        /// user can edit vendor details
        /// </summary>
        public bool PayablesEditVendors { get; set; }
        /// <summary>
        /// user can view vendor details
        /// </summary>
        public bool PayablesViewVendors { get; set; }
        #endregion
        #endregion

    }
}