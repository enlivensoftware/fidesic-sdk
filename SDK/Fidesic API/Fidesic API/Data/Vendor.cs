using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Fidesic.Data
{
    [Serializable]
    public class Vendor : Contact
    {
        //------// Properties \\--------------------------------------------\\
        public virtual string VendorNumber { get; set; }
        public virtual string PrintNameOnCheck { get; set; }
        public virtual string DefaultTerms { get; set; }

        public virtual List<string> AccountNumbers { get; set; }
        public virtual List<VendorGLCode> DefaultGLs { get; set; }


        //------\\ Properties //--------------------------------------------//

        //------// Constructors \\------------------------------------------\\
        public Vendor()
        { }
        //------\\ Constructors //------------------------------------------//

        //------\\ Methods //-----------------------------------------------//
    }
}