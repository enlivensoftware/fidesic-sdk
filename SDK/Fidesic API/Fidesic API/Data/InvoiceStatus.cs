using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Fidesic.Data
{
    public enum InvoiceStatus : int
    {
        Unpaid = 1,
        Sent = 2,
        Approved = 3,
        Voided = 4,
        Cancelled = 5,
        Partially_Paid = 6,
        Paid = 7,
        Unapproved = 8,
        Exported = 9,
    }
}