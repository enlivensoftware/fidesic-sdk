using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Fidesic.Data
{
    public enum ContactType : int
    {
        Vendor = 0,
        Customer = 1,
        Agent = 2,
        Master = 3
    }
}