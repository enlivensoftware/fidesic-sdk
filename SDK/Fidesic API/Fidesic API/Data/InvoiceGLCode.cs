using System;

namespace Fidesic.Data
{
    [Serializable]
    public class InvoiceGLCode
    {
        //------// Properties \\--------------------------------------------\\
         #region Properties
        public int? GLCodeID { get; set; }
        public string FullGLCode { get; set; }
        public string FriendlyName { get; set; }
        public decimal GLAmount { get; set; }
        public string Class { get; set; }
        public string Department { get; set; }
        //public GLType GLTypeID { get; set; }
        //public int InvoiceGLID { get; set; }
        #endregion
    }
}