﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Fidesic.Data
{
    [Serializable]
    public class Account : DataObject<int>
    {
        //------// Properties \\--------------------------------------------\\
        public virtual int AccountID
        {
            get { return base.Key; }
            set { base.Key = value; }
        }


        public virtual int ParentID { get; set; }
        public virtual string Name { get; set; }
        public virtual string Description { get; set; }
        public virtual string GLCode { get; set; }
        public virtual int CategoryID { get; set; }
        public virtual bool Enabled { get; set; }


        private DateTime _deletedDate = DateTime.MinValue;
        public virtual DateTime DeletedDate
        {
            get { return _deletedDate; }
            set { _deletedDate = value; }
        }


        public virtual bool Deleted
        {
            get { return (DeletedDate != DateTime.MinValue); }
            set
            {
                if (value && DeletedDate == DateTime.MinValue)
                {
                    DeletedDate = DateTime.Now;
                }
                else if (!value)
                {
                    DeletedDate = DateTime.MinValue;
                }
            }
        }


        public static Account Empty
        {
            get
            {
                Account empty = new Account();
                empty.Normalize();

                return empty;
            }
        }
        //------\\ Properties //--------------------------------------------//



        //------// Constructors \\------------------------------------------\\
        public Account()
        { }
        //------\\ Constructors //------------------------------------------//



        //------// Methods \\-----------------------------------------------\\
        public override void Normalize()
        {
            if (AccountID == 0) { AccountID = -1; }
            if (ParentID == 0) { ParentID = -1; }
            if (Name == null) { Name = String.Empty; }
            if (Description == null) { Description = String.Empty; }
            if (GLCode == null) { GLCode = String.Empty; }
            if (CategoryID == 0) { CategoryID = -1; }
        }
        //------\\ Methods //-----------------------------------------------//
    }
}