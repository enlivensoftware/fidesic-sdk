using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Fidesic.Data
{
    [Serializable]
    public class BankAccount : PaymentAccount
    {
        //------// Properties \\--------------------------------------------\\
        public virtual string BankName { get; set; }
        public virtual string RoutingNumber { get; set; }
        public virtual BankAccountType Type { get; set; }


        //------\\ Properties //--------------------------------------------//
        //------// Constructors \\------------------------------------------\\
        public BankAccount()
            : base()
        { }
        //------\\ Constructors //------------------------------------------//

        //------// Methods \\-----------------------------------------------\\
        
        //------\\ Methods //-----------------------------------------------//
    }
}