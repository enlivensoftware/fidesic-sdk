﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Fidesic.Data
{
    public enum InvoiceType : int
    {
        Invalid = 0,
        Invoice = 1,
        CreditMemo = 2,
        DebitMemo = 3,
        FinanceCharge = 4,
        Return = 5
    }
}