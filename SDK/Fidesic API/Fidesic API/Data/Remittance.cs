using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Fidesic.Data
{
    [Serializable]
    public class Remittance : DataObject
    {
        //------// Properties \\--------------------------------------------\\
        public virtual int? PaymentID { get; set; }
        public virtual int? InvoiceID { get; set; }
        public virtual string InvoiceNumber { get; set; }
        public virtual decimal Amount { get; set; }
        public virtual int? DocumentType { get; set; }
        /// <summary>
        /// Payment Date on invoice calls. Invoice date on payment calls
        /// </summary>
        public virtual DateTime ReferenceDate { get; set; }
        /// <summary>
        /// current payment status
        /// </summary>
        public virtual string PaymentStatus { get; set; }
        //------\\ Properties //--------------------------------------------//

        //------// Constructors \\------------------------------------------\\
        public Remittance()
        { }
        //------\\ Constructors //------------------------------------------//



        //------// Methods \\-----------------------------------------------\\
        public override void Normalize()
        {
            if (!PaymentID.HasValue) { PaymentID = 0; }
            if (!DocumentType.HasValue) { DocumentType = 1; }
        }
        //------\\ Methods //-----------------------------------------------//
    }
}