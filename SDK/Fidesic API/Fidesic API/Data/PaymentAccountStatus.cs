﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Fidesic.Data
{
    public enum PaymentAccountStatus : int
    {
        Pending = 0,
        Active = 5,
        Inactive = 10,
        Rejected = 15,
        RejectedInactive = 20,
        Frozen = 30
    }
}