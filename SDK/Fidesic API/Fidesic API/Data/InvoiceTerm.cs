using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Xml.Serialization;

namespace Fidesic.Data
{
    [Serializable]
    public class InvoiceTerm : DataObject
    {
        public int TermOptionID { get; set; }
        public int? MasterUserID { get; set; }
        public string Description { get; set; }
        public byte? DiscountTypeID { get; set; }
        public decimal? DiscountPercentage { get; set; }
        public decimal? DiscountAmount { get; set; }
        public int? DiscountDateDays { get; set; }
        public byte DueTypeID { get; set; }
        public int? DueDateDays { get; set; }

        public InvoiceTerm()
        { }
    }
}