﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using System.Web.Script.Serialization;
using Newtonsoft.Json;

namespace Fidesic.Data
{
    [Serializable]
    public abstract class DataObject : IDataObject
    {
        //------// Properties \\--------------------------------------------\\
        object IDataObject.Key { get; set; }
        //------\\ Properties //--------------------------------------------//



        //------// Constructors \\------------------------------------------\\
        protected DataObject()
            : base()
        { }
        //------\\ Constructors //------------------------------------------//



        //------// Methods \\-----------------------------------------------\\
        public virtual void Normalize() { }


        public virtual string SerializeToXml(bool indentOutput)
        {
            XmlWriterSettings xmlSettings = new XmlWriterSettings();
            xmlSettings.OmitXmlDeclaration = true;
            xmlSettings.Encoding = new UTF8Encoding();

            if (indentOutput) { xmlSettings.Indent = true; }
            else { xmlSettings.Indent = false; }

            StringBuilder output = new StringBuilder();

            using (XmlWriter xmlWriter = XmlWriter.Create(output, xmlSettings))
            {
                XmlTypeMapping typeMapping = new XmlReflectionImporter().ImportTypeMapping(GetType());
                XmlSerializer serializer = new XmlSerializer(typeMapping);
                serializer.Serialize(xmlWriter, this);
            }

            return output.ToString();
        }


        public virtual IDataObject DeserializeFromXml(string rawData)
        {
            XmlTypeMapping typeMapping = new XmlReflectionImporter().ImportTypeMapping(GetType());
            return (IDataObject)new XmlSerializer(typeMapping).Deserialize(new StringReader(rawData));
        }


        public static T DeserializeFromXml<T>(string rawData) where T : DataObject
        {
            XmlTypeMapping typeMapping = new XmlReflectionImporter().ImportTypeMapping(typeof(T));
            return (T)new XmlSerializer(typeMapping).Deserialize(new StringReader(rawData));
        }


        public virtual string SerializeToJson()
        {
            var settings = new JsonSerializerSettings
            {
                DateFormatHandling = DateFormatHandling.MicrosoftDateFormat
            };
            return Newtonsoft.Json.JsonConvert.SerializeObject(this, settings);
        }


        public virtual IDataObject DeserializeFromJson(string rawData)
        {

            return (IDataObject)Newtonsoft.Json.JsonConvert.DeserializeObject(rawData); //new JavaScriptSerializer().DeserializeObject(rawData);
        }


        public static T DeserializeFromJson<T>(string rawData) where T : DataObject
        {
            return Newtonsoft.Json.JsonConvert.DeserializeObject<T>(rawData);
        }
        //------\\ Methods //-----------------------------------------------//
    }
}
