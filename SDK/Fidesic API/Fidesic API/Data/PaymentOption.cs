﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Fidesic.Data
{
    public enum PaymentOption : int
    {
        ACH = 0,
        PaperCheck = 1,
        Credit = 2,
        Wire = 3
    }
}