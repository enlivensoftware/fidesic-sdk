using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Fidesic.Data
{
    [Serializable]
    public class CreditCardAccount : PaymentAccount
    {
        //------// Properties \\--------------------------------------------\\
        public virtual string AccountHolderName { get; set; }
        public virtual string CardNumber { get; set; }
        public virtual string CVV2 { get; set; }
        public virtual DateTime ExpirationDate { get; set; }
        public virtual Address Address { get; set; }

        //------\\ Properties //--------------------------------------------//



        //------// Constructors \\------------------------------------------\\
        public CreditCardAccount()
            : base()
        { }
        //------\\ Constructors //------------------------------------------//



        //------// Methods \\-----------------------------------------------\\

        //------\\ Methods //-----------------------------------------------//
    }
}