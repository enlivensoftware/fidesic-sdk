﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fidesic.Data
{
    public interface IDataObject<TKey> : IDataObject
    {
        //------// Properties \\--------------------------------------------\\
        new TKey Key { get; set; }
        //------\\ Properties //--------------------------------------------//
    }
}
