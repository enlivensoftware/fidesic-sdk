using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


namespace Fidesic.Data
{
    [Serializable]
    public class Payment : DataObject<int?>
    {
        //------// Properties \\--------------------------------------------\\
        public virtual int? PaymentID
        {
            get { return base.Key; }
            set { base.Key = value; }
        }

        public virtual int? PaymentAccountID { get; set; }
        public virtual int PayeeUserID { get; set; }
        public virtual int PayerUserID { get; set; }
        public virtual string PayerName { get; set; }
        public virtual string PayeeName { get; set; }
        public virtual DateTime? ProcessingDate { get; set; }
        public virtual DateTime? CreatedDate { get; set; }
        public virtual string PaymentNumber { get; set; }
        public virtual string CheckNumber { get; set; }
        public virtual int? ReferenceNumber { get; set; }
        public virtual string MerchantReferenceNumber { get; set; }
        public virtual decimal Amount { get; set; }
        public virtual PaymentOption PaymentOption { get; set; }
        public virtual DepositOption DepositOption { get; set; }
        public virtual string CancelReason { get; set; }
        public virtual string Memo { get; set; }
        public virtual List<Remittance> Remittance { get; set; }
        public virtual List<Attachment> Attachments { get; set; }
        public virtual PaymentStatus Status { get; set; }
		/// <summary>
        /// Expected Settlement date. 
        /// </summary>        
        public virtual DateTime? DueDate { get; set; }
        public virtual string CurrencyCode { get; set; }
        /// <summary>
        /// will be null if method does not check value.
        /// </summary>
        public virtual bool? canApprove { get; set; }
        /// <summary>
        /// if unapproved, the next approver in workflow
        /// </summary>
        public virtual string NextApproverName { get; set; }
        //------\\ Properties //--------------------------------------------//


        //------// Constructors \\------------------------------------------\\
        public Payment()
        { }
        //------\\ Constructors //------------------------------------------//



        //------// Methods \\-----------------------------------------------\\
        
        //------\\ Methods //-----------------------------------------------//
    }
}