﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Fidesic.Data
{
    public enum PaymentStatus : int
    {
        Edit = 0,
        InProcess = 1,
        Scheduled = 2,
        Unapproved = 3,
        Approved = 4,
        Mailed = 5,
        Paid = 6,
        Voided = 7,
        Failed = 8
    }
}