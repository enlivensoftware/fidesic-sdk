using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace Fidesic.Data
{
    [Serializable]
    public class Attachment
    {
        //------// Properties \\--------------------------------------------\\
        public virtual string FileName { get; set; }
        public virtual string ContentType { get; set; }
        public virtual string URL { get; set; }
        /// <summary>
        /// base64 content of file. This is not currently used anywhere.
        /// </summary>
        public virtual string Content { get; set; }

        //------\\ Properties //--------------------------------------------//



        //------// Constructors \\------------------------------------------\\
        public Attachment()
        { }
        //------\\ Constructors //------------------------------------------//

        //------// Methods \\-----------------------------------------------\\
        public void makeByteString(byte[] shouldBeString)
        {
            Content = shouldBeString == null ? null : Convert.ToBase64String(shouldBeString);
        }
        //------\\ Methods //-----------------------------------------------//
    }
}