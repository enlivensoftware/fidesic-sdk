using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Fidesic.Data
{
    public enum GLType
    {
        Purchasing = 1,
        AP = 2,
        Freight = 3,
        Tax = 4,
        TradeDiscount = 5,
        Misc = 6,
        Unit = 7
    }
}