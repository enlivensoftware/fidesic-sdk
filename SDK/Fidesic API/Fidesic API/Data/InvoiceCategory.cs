using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Xml.Serialization;

namespace Fidesic.Data
{
    /// <summary>
    /// Invoice Category or Revenue Line
    /// </summary>
    [Serializable]
    public class InvoiceCategory : DataObject
    {
        #region Properties
        public virtual int? CategoryID { get; set; }
        public virtual string CategoryName { get; set; }
        public virtual string InvoiceCode { get; set; }
        #endregion

        public InvoiceCategory()
        { }
    }
}