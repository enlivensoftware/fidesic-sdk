﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Fidesic.Data
{
    [Serializable]
    public class Message : DataObject<int>
    {
        //------// Properties \\--------------------------------------------\\
        public virtual int MessageID
        {
            get { return base.Key; }
            set { base.Key = value; }
        }


        public virtual int ParentID { get; set; }
        public virtual int PostedBy { get; set; }
        public virtual DateTime PostedDate { get; set; }
        public virtual string Content { get; set; }
        public virtual string Subject { get; set; }


        public static Message Empty
        {
            get
            {
                Message empty = new Message();
                empty.Normalize();

                return empty;
            }
        }
        //------\\ Properties //--------------------------------------------//



        //------// Constructors \\------------------------------------------\\
        public Message()
        { }
        //------\\ Constructors //------------------------------------------//



        //------// Methods \\-----------------------------------------------\\
        public override void Normalize()
        {
            if (Content == null) { Content = String.Empty; }
            if (Subject == null) { Subject = String.Empty; }
        }
        //------\\ Methods //-----------------------------------------------//
    }
}