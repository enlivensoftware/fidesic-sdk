using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Globalization;
using System.Text;

namespace Fidesic.Data
{
    [Serializable]
    public class FidesicUser : DataObject<int>
    {
        //------// Properties \\--------------------------------------------\\
        public virtual int UserID // RegisteredUserID
        {
            get { return base.Key; }
            set { base.Key = value; }
        }


        public virtual int? CompanyID { get; set; } // Equivalent to ContactCSUserID in the Contacts table.
        public virtual string FirstName { get; set; }
        public virtual string MiddleName { get; set; }
        public virtual string LastName { get; set; }
        public virtual string Email { get; set; } // EmailAddress
        public virtual int? EmailID { get; set; }
        public virtual Address AddressInfo { get; set; }
        public virtual int? DefaultDepositAccountID { get; set; }
        public virtual int? DefaultPaymentAccountID { get; set; }
        public virtual string CompanyName { get; set; } // DisplayName
        public virtual string FaxNumber { get; set; }
		public virtual string PhoneNumber { get; set; }
        public virtual bool IsCompany { get; set; } // PartnerFlag
        public virtual bool Enabled { get; set; }
        /// <summary>
        /// if null, this field was not returned
        /// </summary>
        public virtual bool? Registered { get; set; }
        public virtual List<Address> AdditionalAddresses { get; set; }


        public static FidesicUser Empty
        {
            get
            {
                FidesicUser empty = new FidesicUser();
                empty.Normalize();

                return empty;
            }
        }
        //------\\ Properties //--------------------------------------------//



        //------// Constructors \\------------------------------------------\\
        public FidesicUser()
            : base()
        { }


        public FidesicUser(int userID)
            : base(userID)
        { }
        //------\\ Constructors //------------------------------------------//



        //------// Methods \\-----------------------------------------------\\
        public override void Normalize()
        {
            if (AddressInfo == null) { AddressInfo = new Address(); }
        }
        //------\\ Methods //-----------------------------------------------//
    }
}