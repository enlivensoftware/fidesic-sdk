﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Fidesic.Data
{
    public enum AddressType : int
    {
        Home = 1,
        CreditCard = 2,
        CreditReport = 3,
        Lockbox = 5,
        Shipping = 6
    }
}