using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Fidesic.Data
{
    [Serializable]
    public class Address : DataObject<int>
    {
        //------// Properties \\--------------------------------------------\\
        public virtual int AddressID
        {
            get { return base.Key; }
            set { base.Key = value; }
        }
        /// <summary>
        /// Name descriptor to differentiate multiple addresses of the same type
        /// </summary>
        public virtual string AddressCode { get; set; }

        public virtual string Address1 { get; set; }
        public virtual string Address2 { get; set; }
        public virtual string Address3 { get; set; }
        public virtual string City { get; set; }
        public virtual string State { get; set; }
        public virtual string Zip { get; set; }
        public virtual string Country { get; set; }
        public virtual AddressType Type { get; set; }

        //------\\ Properties //--------------------------------------------//



        //------// Constructors \\------------------------------------------\\
        public Address()
        { }
        //------\\ Constructors //------------------------------------------//

        //------// Methods \\-----------------------------------------------\\
        
        //------\\ Methods //-----------------------------------------------//
    }
}