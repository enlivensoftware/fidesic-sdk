using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Fidesic.Data
{
    [Serializable]
    public abstract class PaymentAccount : DataObject<int?>
    {
        //------// Properties \\--------------------------------------------\\
        public virtual int? PaymentAccountID
        {
            get { return base.Key; }
            set { base.Key = value; }
        }


		public virtual string AccountName { get; set; }
        public virtual string AccountNumber { get; set; }
        public virtual PaymentAccountStatus Status { get; set; }
        //------\\ Properties //--------------------------------------------//

        //------// Constructors \\------------------------------------------\\
        protected PaymentAccount()
        { }
        //------\\ Constructors //------------------------------------------//

        //------// Methods \\-----------------------------------------------\\
        //------\\ Methods //-----------------------------------------------//
    }
}