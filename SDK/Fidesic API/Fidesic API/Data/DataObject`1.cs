﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using System.Web.Script.Serialization;

namespace Fidesic.Data
{
    [Serializable]
    public abstract class DataObject<TKey> : DataObject, IDataObject<TKey>
    {
        //------// Properties \\--------------------------------------------\\
        TKey IDataObject<TKey>.Key
        {
            get { return Key; } // Implemented the getter and setter so a private field wouldn't be created.
            set { Key = value; }
        }


        public virtual TKey Key { get; set; }
        //------\\ Properties //--------------------------------------------//



        //------// Constructors \\------------------------------------------\\
        protected DataObject()
            : base()
        { }


        protected DataObject(TKey key)
            : base()
        {
            Key = key;
        }
        //------\\ Constructors //------------------------------------------//



        //------// Methods \\-----------------------------------------------\\

        //------\\ Methods //-----------------------------------------------//
    }
}
