using System;

namespace Fidesic.Data
{
    [Serializable]
    public class GLClass : DataObject
    {
        public int ID { get; set; }
        /// <summary>
        /// GL Class description field. 
        /// Only item that gets set for Quickbooks.
        /// </summary>
        public string FullGLClass { get; set; }
        /// <summary>
        /// GL Class value field. 
        /// Not used with Quickbooks.
        /// </summary>
        public string Value { get; set; }

        public GLClass()
        { }
    }
}