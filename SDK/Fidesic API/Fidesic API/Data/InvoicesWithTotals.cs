﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Fidesic.Data
{
    [Serializable]
    public class InvoicesWithTotals
    {
        /// <summary>
        /// Paginated result of invoices
        /// </summary>
        public virtual List<Invoice> Invoices { get; set; }
        /// <summary>
        /// Number of pages of invoices available with current method and filters 
        /// </summary>
        public virtual int PageCount { get; set; }
        /// <summary>
        /// Total number of invoices available with current method and filters 
        /// </summary>
        public virtual int TotalCount { get; set; }
        /// <summary>
        /// Total paid amount of invoices available with current method and filters
        /// </summary>
        public virtual decimal TotalPaid { get; set; }
        /// <summary>
        /// Total unpaid amount of invoices available with current method and filters
        /// </summary>
        public virtual decimal TotalUnpaid { get; set; }
    }
}