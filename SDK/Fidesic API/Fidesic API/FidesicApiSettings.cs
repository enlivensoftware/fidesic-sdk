﻿using System;
using System.Configuration;
using System.Collections.Generic;
using System.Text;
using System.Web;

namespace Fidesic
{
    public class FidesicApiSettings : IEquatable<FidesicApiSettings>
    {
        //------// Properties \\--------------------------------------------\\
        private bool _isDirty = false;
        public virtual bool IsDirty
        {
            get { return _isDirty; }
        }


        private OperationDataFormat _dataFormat = OperationDataFormat.Json;
        public virtual OperationDataFormat DataFormat
        {
            get { return _dataFormat; }
            set { _dataFormat = value; }
        }

        private FidesicEnvironment _environment = FidesicEnvironment.Staging;
        /// <summary>
        /// Determines end points for Fidesic integration.
        /// </summary>
        public FidesicEnvironment Environment
        {
            get
            {
                return _environment;
            }
            set
            {
                _environment = value;
            }
        }

        private string _apiKey = null;
        public virtual string ApiKey
        {
            get { return _apiKey; }
            set
            {
                if (!String.Equals(_apiKey, value))
                {
                    _apiKey = value;
                    _isDirty = true;

                    FireChangedEvent("ApiKey", value);
                }
            }
        }


        private string _secretKey = null;
        public virtual string SecretKey
        {
            get { return _secretKey; }
            set
            {
                if (!String.Equals(_secretKey, value))
                {
                    _secretKey = value;
                    _isDirty = true;

                    FireChangedEvent("SecretKey", value);
                }
            }
        }


        private bool _consumerIsTrusted = false;
        public virtual bool ConsumerIsTrusted
        {
            get { return _consumerIsTrusted; }
            set
            {
                if (_consumerIsTrusted != value)
                {
                    _consumerIsTrusted = value;
                    _isDirty = true;

                    FireChangedEvent("ConsumerIsTrusted", value);
                }
            }
        }


        private string _authorizationCallbackUrl = null;
        public virtual string AuthorizationCallbackUrl
        {
            get
            {
                if (_authorizationCallbackUrl == null)
                {
                    _authorizationCallbackUrl = (ConfigurationManager.AppSettings["AuthorizationCallbackUrl"] ?? String.Empty).Trim('/', ' ').ToLower();
                }

                return _authorizationCallbackUrl;
            }
            set
            {
                if (!String.Equals(_authorizationCallbackUrl, value))
                {
                    _authorizationCallbackUrl = value;
                    _isDirty = true;

                    FireChangedEvent("AuthorizationCallbackUrl", value);
                }
            }
        }


        private string _baseApiServiceUrl = null;
        /// <summary>
        /// Only set manually if Environment is set to custom.
        /// </summary>
        public virtual string BaseApiServiceUrl
        {
            get
            {
                if (_baseApiServiceUrl == null)
                {
                    switch (Environment)
                    {
                        case FidesicEnvironment.Live:
                            _baseApiServiceUrl = "https://api.fidesic.com/services";
                            break;
                        case FidesicEnvironment.Development:
                            _baseApiServiceUrl = "https://dev.fidesic.com/services";
                            break;
                        case FidesicEnvironment.Custom:
                            _baseApiServiceUrl = (ConfigurationManager.AppSettings["BaseApiServiceUrl"] ?? String.Empty).Trim('/', ' ').ToLower();
                            break;
                        case FidesicEnvironment.Staging:
                        default:
                            _baseApiServiceUrl = "https://staging.fidesic.com/services";
                            break;
                    }
                    
                }

                return _baseApiServiceUrl;
            }
            set
            {
                if (Environment == FidesicEnvironment.Custom && !String.Equals(_baseApiServiceUrl, value))
                {
                    _baseApiServiceUrl = value;
                    //_isDirty = true;

                    //FireChangedEvent("BaseApiServiceUrl", value);
                }
            }
        }


        private string _baseOAuthServiceUrl = null;
        /// <summary>
        /// Only set manually if Environment is set to custom.
        /// </summary>
        public virtual string BaseOAuthServiceUrl
        {
            get
            {
                if (_baseOAuthServiceUrl == null)
                {
                    //if (Environment == FidesicEnvironment.Custom)
                    //{
                    //    try
                    //    {
                    //        _baseOAuthServiceUrl = (ConfigurationManager.AppSettings["BaseOAuthServiceUrl"] ?? String.Empty).Trim('/', ' ').ToLower();
                    //    }
                    //    catch
                    //    {
                    //        _baseOAuthServiceUrl = BaseApiServiceUrl.Trim('/', ' ') + "/oauth";
                    //    }
                    //}
                    //else
                    //{
                        _baseOAuthServiceUrl = BaseApiServiceUrl.Trim('/', ' ') + "/oauth";
                    //}
                }

                return _baseOAuthServiceUrl;
            }
            set
            {
                if (Environment == FidesicEnvironment.Custom && !String.Equals(_baseOAuthServiceUrl, value))
                {
                    _baseOAuthServiceUrl = value;
                    _isDirty = true;

                    FireChangedEvent("BaseOAuthServiceUrl", value);
                }
            }
        }


        private string _accessTokenUriResourcePath = null;
        public virtual string AccessTokenUriResourcePath
        {
            get
            {
                if (_accessTokenUriResourcePath == null)
                {
                    string resourcePath = ConfigurationManager.AppSettings["AccessTokenUriResourcePath"];

                    if (String.IsNullOrWhiteSpace(resourcePath))
                    {
                        resourcePath = Settings.Default.AccessTokenUriResourcePath;
                    }

                    _accessTokenUriResourcePath = (resourcePath ?? String.Empty).Trim('/', ' ').ToLower();
                }

                return _accessTokenUriResourcePath;
            }
            set
            {
                if (!String.Equals(_accessTokenUriResourcePath, value))
                {
                    _accessTokenUriResourcePath = (value ?? String.Empty).Trim('/', ' ').ToLower();
                    _isDirty = true;

                    FireChangedEvent("AccessTokenUriResourcePath", value);
                }
            }
        }


        private string _accessTokenUrl = null;
        public virtual string AccessTokenUrl
        {
            get
            {
                if (_accessTokenUrl == null)
                {
                    _accessTokenUrl = Utils.BuildRequestUrl(BaseOAuthServiceUrl, AccessTokenUriResourcePath, null);
                }

                return _accessTokenUrl;
            }
        }


        private string _authorizeUriResourcePath = null;
        public virtual string AuthorizeUriResourcePath
        {
            get
            {
                if (_authorizeUriResourcePath == null)
                {
                    string resourcePath = ConfigurationManager.AppSettings["AuthorizeUriResourcePath"];

                    if (String.IsNullOrWhiteSpace(resourcePath))
                    {
                        resourcePath = Settings.Default.AuthorizeUriResourcePath;
                    }

                    _authorizeUriResourcePath = (resourcePath ?? String.Empty).Trim('/', ' ').ToLower();
                }

                return _authorizeUriResourcePath;
            }
            set
            {
                if (!String.Equals(_authorizeUriResourcePath, value))
                {
                    _authorizeUriResourcePath = (value ?? String.Empty).Trim('/', ' ').ToLower();
                    _isDirty = true;

                    FireChangedEvent("AuthorizeUriResourcePath", value);
                }
            }
        }


        private string _authorizeUrl = null;
        public virtual string AuthorizeUrl
        {
            get
            {
                if (_authorizeUrl == null)
                {
                    _authorizeUrl = Utils.BuildRequestUrl(BaseOAuthServiceUrl, AuthorizeUriResourcePath, null);
                }

                return _authorizeUrl;
            }
        }


        private string _requestTokenUriResourcePath = null;
        public virtual string RequestTokenUriResourcePath
        {
            get
            {
                if (_requestTokenUriResourcePath == null)
                {
                    string resourcePath = ConfigurationManager.AppSettings["RequestTokenUriResourcePath"];

                    if (String.IsNullOrWhiteSpace(resourcePath))
                    {
                        resourcePath = Settings.Default.RequestTokenUriResourcePath;
                    }

                    _requestTokenUriResourcePath = (resourcePath ?? String.Empty).Trim('/', ' ').ToLower();
                }

                return _requestTokenUriResourcePath;
            }
            set
            {
                if (!String.Equals(_requestTokenUriResourcePath, value))
                {
                    _requestTokenUriResourcePath = (value ?? String.Empty).Trim('/', ' ').ToLower();
                    _isDirty = true;

                    FireChangedEvent("RequestTokenUriResourcePath", value);
                }
            }
        }


        private string _requestTokenUrl = null;
        public virtual string RequestTokenUrl
        {
            get
            {
                if (_requestTokenUrl == null)
                {
                    _requestTokenUrl = Utils.BuildRequestUrl(BaseOAuthServiceUrl, RequestTokenUriResourcePath, null);
                }

                return _requestTokenUrl;
            }
        }


        private string _proxyServerUrl = null;
        public virtual string ProxyServerUrl
        {
            get { return _proxyServerUrl; }
            set { _proxyServerUrl = value; }
        }


        private int _proxyServerPort = 80;
        public virtual int ProxyServerPort
        {
            get { return _proxyServerPort; }
            set { _proxyServerPort = value; }
        }


        private int _apiTimeout = 10000;
        public virtual int ApiTimeout
        {
            get { return _apiTimeout; }
            set { _apiTimeout = value; }
        }
        //------\\ Properties //--------------------------------------------//



        //------// Fields \\------------------------------------------------\\
        public delegate void FidesicApiSettingsChangedHandler(FidesicApiSettingsChangedEventArgs args);
        public event FidesicApiSettingsChangedHandler OnChanged;
        //------\\ Fields //------------------------------------------------//



        //------// Constructors \\------------------------------------------\\
        public FidesicApiSettings()
        { }


        public FidesicApiSettings(string apiKey, string secretKey)
        {
            _apiKey = apiKey;
            _secretKey = secretKey;
        }


        public FidesicApiSettings(string apiKey, string secretKey, string baseOAuthServiceUrl)
        {
            _apiKey = apiKey;
            _secretKey = secretKey;
            _baseOAuthServiceUrl = baseOAuthServiceUrl;
        }
        //------\\ Constructors //------------------------------------------//



        //------// Methods \\-----------------------------------------------\\
        public virtual bool Equals(FidesicApiSettings other)
        {
            if (other == null) { return false; }

            if (DataFormat != other.DataFormat) { return false; }
            if (!String.Equals(ApiKey, other.ApiKey)) { return false; }
            if (!String.Equals(SecretKey, other.SecretKey)) { return false; }
            if (ConsumerIsTrusted != other.ConsumerIsTrusted) { return false; }
            if (!String.Equals(AuthorizationCallbackUrl, other.AuthorizationCallbackUrl)) { return false; }
            if (!String.Equals(BaseApiServiceUrl, other.BaseApiServiceUrl)) { return false; }
            if (!String.Equals(BaseOAuthServiceUrl, other.BaseOAuthServiceUrl)) { return false; }
            if (!String.Equals(AccessTokenUriResourcePath, other.AccessTokenUriResourcePath)) { return false; }
            if (!String.Equals(AccessTokenUrl, other.AccessTokenUrl)) { return false; }
            if (!String.Equals(AuthorizeUriResourcePath, other.AuthorizeUriResourcePath)) { return false; }
            if (!String.Equals(AuthorizeUrl, other.AuthorizeUrl)) { return false; }
            if (!String.Equals(RequestTokenUriResourcePath, other.RequestTokenUriResourcePath)) { return false; }
            if (!String.Equals(RequestTokenUrl, other.RequestTokenUrl)) { return false; }
            if (!String.Equals(ProxyServerUrl, other.ProxyServerUrl)) { return false; }
            if (ProxyServerPort != other.ProxyServerPort) { return false; }
            if (ApiTimeout != other.ApiTimeout) { return false; }


            return true;
        }


        protected virtual void FireChangedEvent(string settingName, object settingValue)
        {
            if (OnChanged != null)
            {
                OnChanged(new FidesicApiSettingsChangedEventArgs(settingName, settingValue, this));
            }
        }
        //------\\ Methods //-----------------------------------------------//
    }
}