﻿using Fidesic;
using Fidesic.Web;

namespace FidesicSDK
{
    public class LoginByAPIController : FidesicWebController
    {
        //------// Properties \\--------------------------------------------\\

        //------\\ Properties //--------------------------------------------//



        //------// Constructors \\------------------------------------------\\
        public LoginByAPIController()
            : base(BuildApiSettings(), true)
        { }
        //------\\ Constructors //------------------------------------------//



        //------// Methods \\-----------------------------------------------\\
        public static FidesicApiSettings BuildApiSettings()
        {
            FidesicApiSettings apiSettings = new FidesicApiSettings
            {
                //LOG IN VIA Company API Keys
                //NOTE: please contact Fidesic support for API keys for live environment
                ApiKey = "Xbx4W81e9jsEvKwL",
                SecretKey = "7PBa9r0byrjSRlehtElX",
                DataFormat = OperationDataFormat.Json,
                ConsumerIsTrusted = true,
                //CHANGE THIS TO .Live for Production, .Staging for staging
                Environment = FidesicEnvironment.Staging
            };
            return apiSettings;
        }

        protected override void ClearFidesicSessionStorage()
        {
            //    throw new NotImplementedException();
        }

        protected override string DetermineEmailFromEnvironment()
        {
            return null;
        }


        protected override string DetermineCompanyNameFromEnvironment()
        {
            return null;
        }
        //------\\ Methods //-----------------------------------------------//
    }
}