﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ApprovalService.aspx.cs" Inherits="FidesicSDK.ApprovalService" %>
<%@ Import Namespace="Fidesic.Data" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
    <head runat="server">
        <title>Approval Service Runner</title>
    </head>
    
    <body>
        <a href="../Index.html">Back</a><br>
        <form id="form1" runat="server">
            <div class="runner login">
                <asp:PlaceHolder runat="server" ID="errorPlaceholder" Visible="false" EnableViewState="false">
                    <div class="error_container">
                        <p class="error_message">
                            <asp:Literal runat="server" ID="errorLiteral" />
                        </p>
                    </div>
                </asp:PlaceHolder>
                
                
                <fieldset>
                    <legend>Login</legend>
                     
                    <div class="field email">
                        <label for="<%= txtEmail.ClientID %>">Email:</label>
                        <input runat="server" type="text" id="txtEmail" class="text" />
                    </div>

                    <div class="field password">
                        <label for="<%= txtPassword.ClientID %>">Password:</label>
                        <input runat="server" type="password" id="txtPassword" class="text password" />
                    </div>

                    <div class="submit_field">
                        <input runat="server" type="button" id="btnLogin" value="Login" onserverclick="btnLogin_Click" />
                    </div>
                </fieldset>
            </div>

            <fieldset>
                <legend>Update Invoice Status</legend>
                InvoiceID: <input type="text" id="approvalInvoiceID" runat="server" />
                <div class="submit_field">
                    <input id="Button1" runat="server" type="button" value="Approve" title="Approve" onserverclick="btnApproveInvoice_Click"  />
                    <input id="Button2" runat="server" type="button" value="Disapprove" title="Disapprove" onserverclick="btnDisapproveInvoice_Click"  />
                </div>
            </fieldset>

            <div class="runner btnGetUnapprovedInvoices">
                <fieldset>
                    <legend>Get Unapproved Invoices</legend>

                    <div class="submit_field">
                        <input runat="server" type="button" id="btnGetAllSentInvoices" value="Get Unapproved Invoices" onserverclick="btnGetUnapprovedInvoices_Click" />
                    </div>

                    <% if (GetUnapprovedInvoices != null)
                       { %>
                        <% if (GetUnapprovedInvoices.Count == 0)
                           { %>
                            <p class="empty">You do not have any unapproved invoices.</p>
                        <% } else { %>
                            <% foreach (Invoice inv in GetUnapprovedInvoices)
                               { %>
                                <strong>Invoice ID: </strong><%= inv.InvoiceID.ToString() %><br>
                                <strong>Invoice Date: </strong><%= inv.InvoiceDate.ToString() %><br>
                                <strong>Status: </strong><%= inv.Status.ToString() %><br>
                                <strong>Memo: </strong><%= inv.Memo %><br>
                                <strong>InvoiceNumber: </strong><%= inv.InvoiceNumber %><br>
                                <strong>Due Date: </strong><%= inv.DueDate.ToString() %><br>
                                <strong>Amount: </strong><%= inv.Amount.ToString() %><br>
                                
                            <% } %>
                        <% } %>
                    <% } %>
                </fieldset>

                
                <fieldset>
                    <legend>Update Payment Status</legend>
                    PaymentID: <input type="text" id="approvalPaymentID" runat="server" />
                    <div class="submit_field">
                        <input runat="server" type="button" value="Approve" title="Approve" onserverclick="btnApprovePayment_Click"  />
                        <input runat="server" type="button" value="Disapprove" title="Disapprove" onserverclick="btnDisapprovePayment_Click"  />
                    </div>
                </fieldset>
                <fieldset>
                    <legend>Get Unapproved Payments</legend>

                    <div class="submit_field">
                        <input runat="server" type="button" value="Get Unapproved Payments" onserverclick="btnGetUnapprovedPayments_Click" />
                    </div>

                    <% if (GetUnapprovedPayments != null)
                       { %>
                        <% if (GetUnapprovedPayments.Count == 0)
                           { %>
                            <p class="empty">You do not have any unapproved Payments.</p>
                        <% } else { %>
                            <% foreach (Payment inv in GetUnapprovedPayments)
                               { %>
                                <strong>Payment ID: </strong><%= inv.PaymentID.ToString() %><br>
                                <strong>Payment Date: </strong><%= inv.ProcessingDate.ToString() %><br>
                                <strong>Status: </strong><%= inv.Status.ToString() %><br>
                                <strong>Memo: </strong><%= inv.Memo %><br>
                                <strong>PaymentNumber: </strong><%= inv.PaymentNumber %><br>
                                <strong>Due Date: </strong><%= inv.DueDate.ToString() %><br>
                                <strong>Amount: </strong><%= inv.Amount.ToString() %><br>
                                
                            <% } %>
                        <% } %>
                    <% } %>
                </fieldset>              
            </div>


        </form>
    </body>
</html>
