﻿

<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="GLCodeService.aspx.cs" Inherits="FidesicSDK.GLCodeService" %>
<%@ Import Namespace="Fidesic.Data" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
    <head runat="server">
        <title>GLCode Service Runner</title>
    </head>
    
    <body>
        <a href="../Index.html">Back</a><br>
        <form id="form1" runat="server">
            <div class="runner login">
                <asp:PlaceHolder runat="server" ID="errorPlaceholder" Visible="false" EnableViewState="false">
                    <div class="error_container">
                        <p class="error_message">
                            <asp:Literal runat="server" ID="errorLiteral" />
                        </p>
                    </div>
                </asp:PlaceHolder>
                
                
                <fieldset>
                    <legend>Login</legend>
                     
                    <div class="field email">
                        <label for="<%= txtEmail.ClientID %>">Email:</label>
                        <input runat="server" type="text" id="txtEmail" class="text" />
                    </div>

                    <div class="field password">
                        <label for="<%= txtPassword.ClientID %>">Password:</label>
                        <input runat="server" type="password" id="txtPassword" class="text password" />
                    </div>

                    <div class="submit_field">
                        <input runat="server" type="button" id="btnLogin" value="Login" onserverclick="btnLogin_Click" />
                    </div>
                </fieldset>
            </div>


            <div class="runner btnGetAllSentGLCodes">
                <fieldset>
                    <legend>Get GLCodes</legend>

                    <div class="submit_field">
                        <input runat="server" type="button" id="btnGetGLCodes" value="Get GLCodes" onserverclick="btnGetGLCodes_Click" />
                    </div>

                    <% if (GetGLCodes != null)
                       { %>
                        <% if (GetGLCodes.Count == 0)
                           { %>
                            <p class="empty">You do not have any sent GLCodes.</p>
                        <% } else { %>
                            <% foreach (GLCode g in GetGLCodes)
                               { %>
                                <strong>GLCode ID: </strong><%= g.GLCodeID.ToString() %><br>
                                <strong>GL Code: </strong><%= g.FullGLCode %><br>
                                <strong>FriendlyName: </strong><%= g.FriendlyName %><br>
                                
                            <% } %>
                        <% } %>
                    <% } %>
                </fieldset>

               
                <fieldset>
                    <legend>Add GLCode</legend>

                    <div class="submit_field">
                        <input type="text" runat="server" id="addAccountNumber" placeholder="Account" />
                        <input type="text" runat="server" id="addAccountDescription" placeholder="Description" />
                        <input type="text" runat="server" id="addExternalID" placeholder="External ID (Optional)" />
                        <input runat="server" type="button" value="Add GLCode" onserverclick="btnAddGLCode_Click" />
                    </div>

                </fieldset>
                
                <fieldset>
                    <legend>Delete GLCode</legend>
                    Full GLCode: <input type="text" id="FullGLCode" runat="server" />
                    <div class="submit_field">
                        <input runat="server" type="button" value="Delete GLCode" title="Delete" onserverclick="btnDeleteGLCode_Click"  />
                    </div>

                    <% if (GLCodeID > -1)
                       { %>
                        <span><%=GLCodeID > 0 ? "Success" : "Fail" %></span>
                    <% } %>
                </fieldset>
                <fieldset>
                    <legend>Add GL Class</legend>

                    <div class="submit_field">
                        <input type="text" runat="server" id="addClassAccount" placeholder="Account" />
                        <input type="text" runat="server" id="addClassDesc" placeholder="Description" />
                        <input runat="server" type="button" value="Add GL Class" onserverclick="btnAddGLClass_Click" />
                    </div>

                    <% if (ClassID > -1)
                       { %>
                        <span><%=ClassID > 0 ? "Success" : "Fail" %></span>
                    <% } %>
                </fieldset>

                <fieldset>
                    <legend>Add GL Department</legend>

                    <div class="submit_field">
                        <input type="text" runat="server" id="addDeptAccount" placeholder="Account" />
                        <input type="text" runat="server" id="addDeptDesc" placeholder="Description" />
                        <input runat="server" type="button" value="Add GL Department" onserverclick="btnAddGLDepartment_Click" />
                    </div>

                    <% if (DepartmentID > -1)
                       { %>
                        <span><%=DepartmentID > 0 ? "Success" : "Fail" %></span>
                    <% } %>
                </fieldset>
            </div>

        </form>
    </body>
</html>
