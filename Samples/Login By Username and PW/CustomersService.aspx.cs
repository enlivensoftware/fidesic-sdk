﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using Fidesic;
using Fidesic.Data;
using Fidesic.Web;

namespace FidesicSDK
{
    public partial class CustomersService : System.Web.UI.Page
    {
        #region Properties
        //------// Properties \\--------------------------------------------\\
        private LoginByUserNameController _fidesicController = null;
        public LoginByUserNameController FidesicController
        {
            get
            {
                if (_fidesicController == null)
                {
                    _fidesicController = new LoginByUserNameController();
                }

                return _fidesicController;
            }
        }
        //------\\ Properties //--------------------------------------------//



        //------// Fields \\------------------------------------------------\\
        public FidesicUser CurrentUser;
        public List<Customer> GetAllCustomers;
        public Customer customer;
        public int newCustomerID;
        public int mergedCustomerID;
        public string mergedMessage = "";
        //------\\ Fields //------------------------------------------------//



        //------// Methods \\-----------------------------------------------\\
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
        }

        public void EnsureConnectedToFidesic()
        {
            if (!FidesicController.ConnectedToFidesic || FidesicController.CurrentUser == null)
            {
                FidesicController.ConnectToFidesic(txtEmail.Value.Trim(), "", txtPassword.Value);
            }
        }

        public void DisplayError(System.Exception exception)
        {
            DisplayError(exception.ToString());
        }
        public void DisplayError(string errorMessage)
        {
            errorPlaceholder.Visible = true;
            errorLiteral.Text = errorMessage;
        }


        #region "Event Handlers"

        protected void btnLogin_Click(object sender, EventArgs args)
        {
            try
            {
                EnsureConnectedToFidesic();
                DisplayError("Successfully logged in: " + DateTime.Now.ToString());
                //DisplayError("Logged in as: " + CurrentUser.Email);
            }
            catch (System.Exception exception)
            {
                DisplayError(exception);
            }
        }


        protected void btnGetAllCustomers_Click(object sender, EventArgs args)
        {
            try
            {
                EnsureConnectedToFidesic();
                GetAllCustomers = FidesicController.GetAllCustomers();
                
            }
            catch (System.Exception exception)
            {
                DisplayError(exception);
            }
        }

        protected void btnAddCustomer_Click(object sender, EventArgs args)
        {
            try
            {
                int id = 0;
                int.TryParse(customerID.Value, out id);

                Customer newCustomer = new Customer()
                {
                    CompanyName = company.Value,
                    CustomerNumber = customerNumber.Value,
                    Email = emailAddress.Value,
                    AddressInfo = new Address()
                    {
                        Address1 = address1.Value,
                        Address2 = address2.Value,
                        City = City.Value,
                        State = State.Value,
                        Zip = Zip.Value
                    },
                    FirstName = firstName.Value,
                    LastName = lastName.Value,
                    PhoneNumber = phoneNumber.Value,
                    UserID=id
                };

                EnsureConnectedToFidesic();
                if(id > 0)
                    FidesicController.UpdateCustomer(newCustomer);
                else
                    newCustomerID = FidesicController.AddCustomer(newCustomer);

                if (newCustomerID != 0)
                    customerID.Value = newCustomerID.ToString();
            }
            catch (System.Exception exception)
            {
                DisplayError(exception);
            }
        }
        

        protected void btnGetCustomer_Click(object sender, EventArgs args)
        {
            try
            {
                int id = 0;
                int.TryParse(customerID.Value, out id);
                EnsureConnectedToFidesic();
                customer = FidesicController.GetCustomer(id);

                if (customer != null)
                {
                    customerID.Value = customer.UserID.ToString();
                    company.Value = customer.CompanyName;
                    customerNumber.Value = customer.CustomerNumber;
                    emailAddress.Value = customer.Email;
                    if (customer.AddressInfo != null)
                    {
                        address1.Value = customer.AddressInfo.Address1;
                        address2.Value = customer.AddressInfo.Address2;
                        City.Value = customer.AddressInfo.City;
                        State.Value = customer.AddressInfo.State;
                        Zip.Value = customer.AddressInfo.Zip;
                    }
                    firstName.Value = customer.FirstName;
                    lastName.Value = customer.LastName;
                    phoneNumber.Value = customer.PhoneNumber;
                }
            }
            catch (System.Exception exception)
            {
                DisplayError(exception);
            }
        }

        protected void btnDeleteCustomer_Click(object sender, EventArgs args)
        {
            try
            {
                EnsureConnectedToFidesic();
                FidesicController.DeleteCustomer(1);

            }
            catch (System.Exception exception)
            {
                DisplayError(exception);
            }
        }

        protected void btnMergeCustomer_Click(object sender, EventArgs args)
        {
            try
            {
                string oldCustomerNumber = oldCust.Value;
                string newCustomerNumber = newCust.Value;

                if (string.IsNullOrWhiteSpace(oldCustomerNumber) || string.IsNullOrWhiteSpace(newCustomerNumber))
                    throw new Exception("Either old or new customer number is empty.");

                EnsureConnectedToFidesic();
                try
                {
                    mergedCustomerID = FidesicController.MergeCustomers(oldCustomerNumber, newCustomerNumber);
                }
                catch (Exception ex)
                {
                    if(ex.Message.StartsWith("Could not find old customer"))
                        mergedMessage = oldCustomerNumber + " has been merged into " + newCustomerNumber + ". <br>Fidesic Contact UserID: " + mergedCustomerID.ToString();
                    else
                        throw ex;
                }
            }
            catch (System.Exception exception)
            {
                DisplayError(exception);
            }
        }
        #endregion
        //------\\ Methods //-----------------------------------------------//
    }
}