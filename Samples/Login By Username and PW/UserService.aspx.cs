﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using Fidesic;
using Fidesic.Data;
using Fidesic.Web;

namespace FidesicSDK
{
    public partial class UserService : System.Web.UI.Page
    {
        //------// Properties \\--------------------------------------------\\
        private LoginByUserNameController _fidesicController = null;
        public LoginByUserNameController FidesicController
        {
            get
            {
                if (_fidesicController == null)
                {
                    _fidesicController = new LoginByUserNameController();
                }

                return _fidesicController;
            }
        }
        //------\\ Properties //--------------------------------------------//



        //------// Fields \\------------------------------------------------\\
        public FidesicUser CurrentUser;
        public Permissions UserPermissions;
        public List<FidesicLogin> list;
        //------\\ Fields //------------------------------------------------//



        //------// Methods \\-----------------------------------------------\\
        protected void Page_Load(object sender, EventArgs e)
        {

        }


        public void EnsureConnectedToFidesic()
        {
            if (!FidesicController.ConnectedToFidesic)
            {
                FidesicController.ConnectToFidesic(txtEmail1.Value.Trim(), "LouSoft", txtPassword1.Value);
            }
        }


        public void DisplayError(System.Exception exception)
        {
            DisplayError(exception.ToString());
        }


        public void DisplayError(string errorMessage)
        {
            errorPlaceholder.Visible = true;
            errorLiteral.Text = errorMessage;
        }



        #region "Event Handlers"

        protected void btnGetProfile_Click(object sender, EventArgs args)
        {
            try
            {
                EnsureConnectedToFidesic();
                CurrentUser = FidesicController.GetCurrentUser();

                if (CurrentUser != null)
                {
                    firstName.Value = CurrentUser.FirstName;
                    lastName.Value = CurrentUser.LastName;
                    middleName.Value = CurrentUser.MiddleName;
                    phoneNumber.Value = CurrentUser.PhoneNumber;
                    company.Value = CurrentUser.CompanyName;
                    emailAddress.Value = CurrentUser.Email;
                }
            }
            catch (System.Exception exception)
            {
                DisplayError(exception);
            }
        }
        protected void btnUpdateProfile_Click(object sender, EventArgs args)
        {
            try
            {
                EnsureConnectedToFidesic();
                CurrentUser = FidesicController.GetCurrentUser();
                if (CurrentUser != null)
                {
                    CurrentUser.FirstName = firstName.Value;
                    CurrentUser.LastName = lastName.Value;
                    CurrentUser.MiddleName = middleName.Value;
                    CurrentUser.PhoneNumber = phoneNumber.Value;
                    CurrentUser.CompanyName = company.Value;
                    CurrentUser.Email = emailAddress.Value;
                }

                FidesicController.UpdateUser(CurrentUser);

                
            }
            catch (System.Exception exception)
            {
                DisplayError(exception);
            }
        }

        protected void btnCreateUser_Click(object sender, EventArgs args)
        {
            try
            {
                CreateUserData createUserData = new CreateUserData();

                FidesicUser newUser = new FidesicUser();
                newUser.FirstName = txtFirstName.Value.Trim();
                newUser.LastName = txtLastName.Value.Trim();
                newUser.Email = txtEmail2.Value.Trim();
                newUser.CompanyName = txtCompanyName.Value.Trim();
                newUser.IsCompany = false;

                createUserData.UserInfo = newUser;
                createUserData.Password = txtPassword2.Value;
                createUserData.SecretQuestion = ddlSecretQuestion.Items[ddlSecretQuestion.SelectedIndex].Text;
                createUserData.SecretAnswer = txtSecretAnswer.Value.Trim();
                createUserData.TaxID = null;


                //EnsureConnectedToFidesic();
                FidesicController.CreateUser(createUserData);
            }
            catch (System.Exception exception)
            {
                DisplayError(exception);
            }
        }
        protected void btnListAccounts_Click(object sender, EventArgs args)
        {
            try
            {
                EnsureConnectedToFidesic();
                list = FidesicController.ListAccounts();
            }
            catch (System.Exception exception)
            {
                DisplayError(exception);
            }
        }
        protected void btnSwitchAccounts_Click(object sender, EventArgs args)
        {
            try
            {
                EnsureConnectedToFidesic();
                string x = Request.Form["accountID"];
                int accountID = 0;
                if (int.TryParse(x, out accountID))
                {
                    FidesicController.SwitchAccount(accountID);
                }
                else
                {
                    DisplayError("Invalid account ID.");
                }
            }
            catch (System.Exception exception)
            {
                DisplayError(exception);
            }
        }

        protected void btnLostPassword_Click(object sender, EventArgs args)
        {
            try
            {
                if (!string.IsNullOrWhiteSpace(LostPasswordEmail.Value))
                {
                    FidesicController.LostPasswordRequest(LostPasswordEmail.Value);
                }
                else
                {
                    DisplayError("Please enter a valid email address.");
                }
            }
            catch (System.Exception exception)
            {
                DisplayError(exception);
            }
        }

        protected void btnLostPasswordReset_Click(object sender, EventArgs args)
        {
            try
            {
                if (!string.IsNullOrWhiteSpace(LostPasswordResetEmail.Value)
                    && !string.IsNullOrWhiteSpace(resetCode.Value)
                    && !string.IsNullOrWhiteSpace(newPassword.Value))
                {
                    FidesicController.LostPasswordResetWithCode(LostPasswordResetEmail.Value, resetCode.Value, newPassword.Value);
                }
                else
                {
                    DisplayError("Please enter a valid email address.");
                }
            }
            catch (System.Exception exception)
            {
                DisplayError(exception);
            }
        }

        protected void btnGetPermissions_Click(object sender, EventArgs args)
        {
            try
            {
                EnsureConnectedToFidesic();
                UserPermissions = FidesicController.GetUserPermissions();
            }
            catch (System.Exception exception)
            {
                DisplayError(exception);
            }
        }
        #endregion
        //------\\ Methods //-----------------------------------------------//
    }
}