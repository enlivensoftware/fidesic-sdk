﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using Fidesic;
using Fidesic.Data;
using Fidesic.Web;

namespace FidesicSDK
{
    public partial class VendorsService : System.Web.UI.Page
    {
        #region Properties
        //------// Properties \\--------------------------------------------\\
        private LoginByUserNameController _fidesicController = null;
        public LoginByUserNameController FidesicController
        {
            get
            {
                if (_fidesicController == null)
                {
                    _fidesicController = new LoginByUserNameController();
                }

                return _fidesicController;
            }
        }
        //------\\ Properties //--------------------------------------------//



        //------// Fields \\------------------------------------------------\\
        public FidesicUser CurrentUser;
        public List<Vendor> GetAllVendors;
        public Vendor vendor;
        public int newVendorID;
        //------\\ Fields //------------------------------------------------//



        //------// Methods \\-----------------------------------------------\\
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
        }

        public void EnsureConnectedToFidesic()
        {
            if (!FidesicController.ConnectedToFidesic || FidesicController.CurrentUser == null)
            {
                FidesicController.ConnectToFidesic(txtEmail.Value.Trim(), "", txtPassword.Value);
            }
        }

        public void DisplayError(System.Exception exception)
        {
            DisplayError(exception.ToString());
        }
        public void DisplayError(string errorMessage)
        {
            errorPlaceholder.Visible = true;
            errorLiteral.Text = errorMessage;
        }


        #region "Event Handlers"

        protected void btnLogin_Click(object sender, EventArgs args)
        {
            try
            {
                EnsureConnectedToFidesic();
                DisplayError("Successfully logged in: " + DateTime.Now.ToString());
                //DisplayError("Logged in as: " + CurrentUser.Email);
            }
            catch (System.Exception exception)
            {
                DisplayError(exception);
            }
        }


        protected void btnGetAllVendors_Click(object sender, EventArgs args)
        {
            try
            {
                EnsureConnectedToFidesic();
                GetAllVendors = FidesicController.GetAllVendors();
                
            }
            catch (System.Exception exception)
            {
                DisplayError(exception);
            }
        }

        protected void btnAddVendor_Click(object sender, EventArgs args)
        {
            try
            {
                int id = 0;
                int.TryParse(vendorID.Value, out id);

                Vendor newVendor = new Vendor()
                {
                    CompanyName = company.Value,
                    VendorNumber = vendorNumber.Value,
                    Email = emailAddress.Value,
                    AddressInfo = new Address()
                    {
                        Address1 = address1.Value,
                        Address2 = address2.Value,
                        City = City.Value,
                        State = State.Value,
                        Zip = Zip.Value
                    },
                    FirstName = firstName.Value,
                    LastName = lastName.Value,
                    PhoneNumber = phoneNumber.Value,
                    UserID=id
                };

                EnsureConnectedToFidesic();
                if(id > 0)
                    FidesicController.UpdateVendor(newVendor);
                else
                    newVendorID = FidesicController.AddVendor(newVendor);

                if (newVendorID != 0)
                    vendorID.Value = newVendorID.ToString();
            }
            catch (System.Exception exception)
            {
                DisplayError(exception);
            }
        }
        

        protected void btnGetVendor_Click(object sender, EventArgs args)
        {
            try
            {
                int id = 0;
                int.TryParse(vendorID.Value, out id);
                EnsureConnectedToFidesic();
                vendor = FidesicController.GetVendor(id);

                if (vendor != null)
                {
                    vendorID.Value = vendor.UserID.ToString();
                    company.Value = vendor.CompanyName;
                    vendorNumber.Value = vendor.VendorNumber;
                    emailAddress.Value = vendor.Email;
                    if (vendor.AddressInfo != null)
                    {
                        address1.Value = vendor.AddressInfo.Address1;
                        address2.Value = vendor.AddressInfo.Address2;
                        City.Value = vendor.AddressInfo.City;
                        State.Value = vendor.AddressInfo.State;
                        Zip.Value = vendor.AddressInfo.Zip;
                    }
                    firstName.Value = vendor.FirstName;
                    lastName.Value = vendor.LastName;
                    phoneNumber.Value = vendor.PhoneNumber;
                }
            }
            catch (System.Exception exception)
            {
                DisplayError(exception);
            }
        }
        protected void btnGetVendorNumber_Click(object sender, EventArgs args)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(VendorNumberSearch.Value)) throw new Exception("Please enter a Vendor Number to search for.");
                EnsureConnectedToFidesic();
                vendor = FidesicController.GetVendor(VendorNumberSearch.Value);

                if (vendor != null)
                {
                    vendorID.Value = vendor.UserID.ToString();
                    company.Value = vendor.CompanyName;
                    vendorNumber.Value = vendor.VendorNumber;
                    emailAddress.Value = vendor.Email;
                    if (vendor.AddressInfo != null)
                    {
                        address1.Value = vendor.AddressInfo.Address1;
                        address2.Value = vendor.AddressInfo.Address2;
                        City.Value = vendor.AddressInfo.City;
                        State.Value = vendor.AddressInfo.State;
                        Zip.Value = vendor.AddressInfo.Zip;
                    }
                    firstName.Value = vendor.FirstName;
                    lastName.Value = vendor.LastName;
                    phoneNumber.Value = vendor.PhoneNumber;
                }
            }
            catch (System.Exception exception)
            {
                DisplayError(exception);
            }
        }

        protected void btnDeleteVendor_Click(object sender, EventArgs args)
        {
            try
            {
                int id = 0;
                int.TryParse(vendorID.Value, out id);
                EnsureConnectedToFidesic();
                FidesicController.DeleteVendor(id);

            }
            catch (System.Exception exception)
            {
                DisplayError(exception);
            }
        }
        protected void btnDeleteVendorNumber_Click(object sender, EventArgs args)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(VendorNumberSearch.Value)) throw new Exception("Please enter a Vendor Number to search for.");
                EnsureConnectedToFidesic();
                FidesicController.DeleteVendor(VendorNumberSearch.Value);

            }
            catch (System.Exception exception)
            {
                DisplayError(exception);
            }
        }


        #endregion
        //------\\ Methods //-----------------------------------------------//
    }
}