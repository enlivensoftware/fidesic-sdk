﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CustomersService.aspx.cs" Inherits="FidesicSDK.CustomersService" %>
<%@ Import Namespace="Fidesic.Data" %>

<!DOCTYPE html>
<html>
    <head runat="server">
        <title>Customers Service Runner</title>
    </head>
    
    <body>
        <a href="../Index.html">Back</a><br>
        <form id="form1" runat="server">
            <div class="runner login">
                <asp:PlaceHolder runat="server" ID="errorPlaceholder" Visible="false" EnableViewState="false">
                    <div class="error_container">
                        <p class="error_message">
                            <asp:Literal runat="server" ID="errorLiteral" />
                        </p>
                    </div>
                </asp:PlaceHolder>
                
                
                <fieldset>
                    <legend>Login</legend>
                     
                    <div class="field email">
                        <label for="<%= txtEmail.ClientID %>">Email:</label>
                        <input runat="server" type="text" id="txtEmail" class="text" />
                    </div>

                    <div class="field password">
                        <label for="<%= txtPassword.ClientID %>">Password:</label>
                        <input runat="server" type="password" id="txtPassword" class="text password" />
                    </div>

                    <div class="submit_field">
                        <input runat="server" type="button" id="btnLogin" value="Login" onserverclick="btnLogin_Click" />
                    </div>
                </fieldset>
            </div>


            <div class="runner btnGetAllCustomers">
                <fieldset>
                    <legend>Get Customers</legend>

                    <div class="submit_field">
                        Get customer by ID: <input type="text" runat="server" id="customerID" />
                        <input runat="server" type="button" value="Get Customer" onserverclick="btnGetCustomer_Click" />
                        <input runat="server" type="button" value="Get All Customers" onserverclick="btnGetAllCustomers_Click" />
                    </div>

                    <div class="result_pane">
                        <strong>Customer Name:</strong> <input type="text" placeholder="Customer Name" id="company" runat="server"/><br>
                        <strong>Customer Number:</strong> <input type="text" id="customerNumber" runat="server"/><br>
                        <strong>First Name:</strong> <input type="text" id="firstName" runat="server"/><br>
                        <strong>Last Name:</strong> <input type="text" id="lastName" runat="server" /><br>
                        <strong>Email:</strong> <input type="text" id="emailAddress" runat="server"/><br>
                        <strong>Address:</strong>
                            <div>
                                <input type="text" placeholder="Address 1" id="address1" runat="server" /><br>
                                <input type="text" placeholder="Address 2" id="address2" runat="server" /><br>
                                <input type="text" placeholder="Address 3" id="address3" runat="server" /><br>
                                <input type="text" placeholder="City" id="City" runat="server" />
                                <input type="text" placeholder="State" id="State" runat="server" />
                                <input type="text" placeholder="Zip" id="Zip" runat="server" />
                            </div>
                        <strong>Phone:</strong> <input type="text" id="phoneNumber" runat="server" /><br>
                        <input runat="server" type="button" value="Add Customer" onserverclick="btnAddCustomer_Click" />
                    </div>

                    <%if(GetAllCustomers != null && GetAllCustomers.Count > 0){ %>
                    <div class="result_pane">
                        <%foreach(Customer c in GetAllCustomers){ %>
                        <div>
                            ID: <%=c.UserID %><br>
                            Customer Name: <%=c.CompanyName %><br>
                            Customer Number: <%=c.CustomerNumber %><br>
                        </div>
                        <%} %>
                    </div>
                    <%} %>
                </fieldset>
            </div>

            <div class="runner btnGetAllCustomers">
                <fieldset>
                    <legend>Merge Customers</legend>
                    <%if(mergedCustomerID > 0){ %>
                    <div><%=mergedMessage %></div>
                    <%} %>
                    <div class="result_pane">
                        <strong>Old Customer Number:</strong> <input type="text" placeholder="aviation001" id="oldCust" runat="server"/><br>
                        <strong>New Customer Number:</strong> <input type="text" placeholder="aviation002" id="newCust" runat="server"/><br>
                        <input id="Button3" runat="server" type="button" value="Merge Customer" onserverclick="btnMergeCustomer_Click" />
                    </div>

                </fieldset>
            </div>
        </form>
    </body>
</html>
