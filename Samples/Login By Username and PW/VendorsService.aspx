﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="VendorsService.aspx.cs" Inherits="FidesicSDK.VendorsService" %>
<%@ Import Namespace="Fidesic.Data" %>

<!DOCTYPE html>
<html>
    <head runat="server">
        <title>Vendors Service Runner</title>
    </head>
    
    <body>
        <a href="../Index.html">Back</a><br>
        <form id="form1" runat="server">
            <div class="runner login">
                <asp:PlaceHolder runat="server" ID="errorPlaceholder" Visible="false" EnableViewState="false">
                    <div class="error_container">
                        <p class="error_message">
                            <asp:Literal runat="server" ID="errorLiteral" />
                        </p>
                    </div>
                </asp:PlaceHolder>
                
                
                <fieldset>
                    <legend>Login</legend>
                     
                    <div class="field email">
                        <label for="<%= txtEmail.ClientID %>">Email:</label>
                        <input runat="server" type="text" id="txtEmail" class="text" />
                    </div>

                    <div class="field password">
                        <label for="<%= txtPassword.ClientID %>">Password:</label>
                        <input runat="server" type="password" id="txtPassword" class="text password" />
                    </div>

                    <div class="submit_field">
                        <input runat="server" type="button" id="btnLogin" value="Login" onserverclick="btnLogin_Click" />
                    </div>
                </fieldset>
            </div>


            <div class="runner btnGetAllVendors">
                <fieldset>
                    <legend>Get Vendors</legend>

                    <div class="submit_field">
                        Get vendor by ID: <input type="text" runat="server" id="vendorID" />
                        <input runat="server" type="button" value="Get Vendor" onserverclick="btnGetVendor_Click" />
                        <input runat="server" type="button" value="Get All Vendors" onserverclick="btnGetAllVendors_Click" />
                        <br>
                        Get vendor by VendorNumber: <input type="text" runat="server" id="VendorNumberSearch" />
                        <input runat="server" type="button" value="Get Vendor" onserverclick="btnGetVendorNumber_Click">
                    </div>

                    <div class="result_pane"><br>
                        <strong>Vendor Name:</strong> <input type="text" placeholder="Vendor Name" id="company" runat="server"/><br>
                        <strong>Vendor Number:</strong> <input type="text" id="vendorNumber" runat="server"/><input runat="server" type="button" value="Delete Vendor By Number" onserverclick="btnDeleteVendorNumber_Click"><input runat="server" type="button" value="Delete Vendor By ID" onserverclick="btnDeleteVendor_Click"><br>
                        <strong>First Name:</strong> <input type="text" id="firstName" runat="server"/><br>
                        <strong>Last Name:</strong> <input type="text" id="lastName" runat="server" /><br>
                        <strong>Email:</strong> <input type="text" id="emailAddress" runat="server"/><br>
                        <strong>Address:</strong>
                            <div>
                                <input type="text" placeholder="Address 1" id="address1" runat="server" /><br>
                                <input type="text" placeholder="Address 2" id="address2" runat="server" /><br>
                                <input type="text" placeholder="Address 3" id="address3" runat="server" /><br>
                                <input type="text" placeholder="City" id="City" runat="server" />
                                <input type="text" placeholder="State" id="State" runat="server" />
                                <input type="text" placeholder="Zip" id="Zip" runat="server" />
                            </div>
                        <strong>Phone:</strong> <input type="text" id="phoneNumber" runat="server" /><br>
                        <input runat="server" type="button" value="Add Vendor" onserverclick="btnAddVendor_Click" />
                    </div>

                    <%if(GetAllVendors != null && GetAllVendors.Count > 0){ %>
                    <div class="result_pane">
                        <%foreach(Vendor c in GetAllVendors){ %>
                        <div>
                            ID: <%=c.UserID %><br>
                            Vendor Name: <%=c.CompanyName %><br>
                            Vendor Number: <%=c.VendorNumber %><br>
                        </div>
                        <%} %>
                    </div>
                    <%} %>
                </fieldset>
            </div>
        </form>
    </body>
</html>
