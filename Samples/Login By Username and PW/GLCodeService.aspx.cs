﻿using System;
using System.Collections.Generic;
using Fidesic.Data;

namespace FidesicSDK
{
    public partial class GLCodeService : System.Web.UI.Page
    {
        #region Properties
        //------// Properties \\--------------------------------------------\\
        private LoginByUserNameController _fidesicController = null;
        public LoginByUserNameController FidesicController
        {
            get
            {
                if (_fidesicController == null)
                {
                    _fidesicController = new LoginByUserNameController();
                }

                return _fidesicController;
            }
        }
        //------\\ Properties //--------------------------------------------//



        //------// Fields \\------------------------------------------------\\
        public FidesicUser CurrentUser;
        public List<GLCode> GetGLCodes;
        public int GLCodeID = -1;
        public int ClassID = -1;
        public int DepartmentID = -1;
        //------\\ Fields //------------------------------------------------//



        //------// Methods \\-----------------------------------------------\\
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
        }

        public void EnsureConnectedToFidesic()
        {
            if (!FidesicController.ConnectedToFidesic || FidesicController.CurrentUser == null)
            {
                FidesicController.ConnectToFidesic(txtEmail.Value.Trim(), "", txtPassword.Value);
            }
        }

        public void DisplayError(System.Exception exception)
        {
            DisplayError(exception.ToString());
        }
        public void DisplayError(string errorMessage)
        {
            errorPlaceholder.Visible = true;
            errorLiteral.Text = errorMessage;
        }


        #region "Event Handlers"

        protected void btnLogin_Click(object sender, EventArgs args)
        {
            try
            {
                EnsureConnectedToFidesic();
                DisplayError("Successfully logged in: " + DateTime.Now.ToString());
                //DisplayError("Logged in as: " + CurrentUser.Email);
            }
            catch (System.Exception exception)
            {
                DisplayError(exception);
            }
        }


        protected void btnGetGLCodes_Click(object sender, EventArgs args)
        {
            try
            {
                EnsureConnectedToFidesic();
                GetGLCodes = FidesicController.GetAllGLCodes(1,100);
            }
            catch (System.Exception exception)
            {
                DisplayError(exception);
            }
        }


        protected void btnDeleteGLCode_Click(object sender, EventArgs args)
        {
            try
            {
                var tmpGLCode = new GLCode()
                {
                    FullGLCode = FullGLCode.Value //Accounting Chart of Account Code
                };
                EnsureConnectedToFidesic();
                FidesicController.DeleteGLCode(tmpGLCode);
            }
            catch (System.Exception exception)
            {
                DisplayError(exception);
            }
        }

        protected void btnAddGLCode_Click(object sender, EventArgs args)
        {
            try
            {
                GLCode newGLCode = new GLCode()
                {
                    FriendlyName = addAccountDescription.Value, //Displayed name for users
                    FullGLCode = addAccountNumber.Value,        //Accounting Chart of Account Code
                    ExternalID = addExternalID.Value ?? null      //Internal Accounting Package ID. Ignore if does not exist.
                };


                EnsureConnectedToFidesic();
                GLCodeID = FidesicController.AddGLCode(newGLCode);
            }
            catch (System.Exception exception)
            {
                DisplayError(exception);
            }
        }
        protected void btnAddGLClass_Click(object sender, EventArgs args)
        {
            try
            {
                GLClass newGLClass = new GLClass()
                {
                    Value = addClassAccount.Value,
                    FullGLClass = addClassDesc.Value
                };


                EnsureConnectedToFidesic();
                ClassID = FidesicController.AddGLCLass(newGLClass);
            }
            catch (System.Exception exception)
            {
                DisplayError(exception);
            }
        }
        protected void btnAddGLDepartment_Click(object sender, EventArgs args)
        {
            try
            {
                GLDepartment newGLDepartment = new GLDepartment()
                {
                    Value = addDeptAccount.Value,
                    FullGLDepartment = addDeptDesc.Value
                };

                EnsureConnectedToFidesic();
                DepartmentID = FidesicController.AddGLDepartment(newGLDepartment);
            }
            catch (System.Exception exception)
            {
                DisplayError(exception);
            }
        }

        #endregion
        //------\\ Methods //-----------------------------------------------//
    }
}