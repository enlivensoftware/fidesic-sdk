﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using Fidesic;
using Fidesic.Data;
using Fidesic.Web;

namespace FidesicSDK
{
    public partial class ApprovalService : System.Web.UI.Page
    {
        #region Properties
        //------// Properties \\--------------------------------------------\\
        private LoginByUserNameController _fidesicController = null;
        public LoginByUserNameController FidesicController
        {
            get
            {
                if (_fidesicController == null)
                {
                    _fidesicController = new LoginByUserNameController();
                }

                return _fidesicController;
            }
        }
        //------\\ Properties //--------------------------------------------//



        //------// Fields \\------------------------------------------------\\
        public FidesicUser CurrentUser;
        public List<Payment> GetUnapprovedPayments;
        public List<Invoice> GetUnapprovedInvoices;
        public List<ItemTemplate> GetAllInvoiceItemTemplates;
        //------\\ Fields //------------------------------------------------//



        //------// Methods \\-----------------------------------------------\\
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
        }

        public void EnsureConnectedToFidesic()
        {
            if (!FidesicController.ConnectedToFidesic || FidesicController.CurrentUser == null)
            {
                FidesicController.ConnectToFidesic(txtEmail.Value.Trim(), "", txtPassword.Value);
            }
        }

        public void DisplayError(System.Exception exception)
        {
            DisplayError(exception.ToString());
        }
        public void DisplayError(string errorMessage)
        {
            errorPlaceholder.Visible = true;
            errorLiteral.Text = errorMessage;
        }


        #region "Event Handlers"

        protected void btnLogin_Click(object sender, EventArgs args)
        {
            try
            {
                EnsureConnectedToFidesic();
                DisplayError("Successfully logged in: " + DateTime.Now.ToString());
                //DisplayError("Logged in as: " + CurrentUser.Email);
            }
            catch (System.Exception exception)
            {
                DisplayError(exception);
            }
        }


        protected void btnGetUnapprovedPayments_Click(object sender, EventArgs args)
        {
            try
            {
                EnsureConnectedToFidesic();
                GetUnapprovedPayments = FidesicController.GetUnapprovedPayments();
            }
            catch (System.Exception exception)
            {
                DisplayError(exception);
            }
        }
        protected void btnGetUnapprovedInvoices_Click(object sender, EventArgs args)
        {
            try
            {
                EnsureConnectedToFidesic();
                GetUnapprovedInvoices = FidesicController.GeUnapprovedInvoices();
            }
            catch (System.Exception exception)
            {
                DisplayError(exception);
            }
        }

        protected void btnApproveInvoice_Click(object sender, EventArgs args)
        {
            try
            {
                int invoiceID = int.Parse(approvalInvoiceID.Value);
                EnsureConnectedToFidesic();
                FidesicController.UpdateApprovalStatus(invoiceID, ApprovalType.Invoice, ApprovalStatus.Approve);
            }
            catch (System.Exception exception)
            {
                DisplayError(exception);
            }
        }
        protected void btnDisapproveInvoice_Click(object sender, EventArgs args)
        {
            try
            {
                int invoiceID = int.Parse(approvalInvoiceID.Value);
                EnsureConnectedToFidesic();
                FidesicController.UpdateApprovalStatus(invoiceID, ApprovalType.Invoice, ApprovalStatus.Disapprove);
            }
            catch (System.Exception exception)
            {
                DisplayError(exception);
            }
        }
        protected void btnApprovePayment_Click(object sender, EventArgs args)
        {
            try
            {
                int paymentID = int.Parse(approvalPaymentID.Value);
                EnsureConnectedToFidesic();
                FidesicController.UpdateApprovalStatus(paymentID, ApprovalType.Payment, ApprovalStatus.Approve);
            }
            catch (System.Exception exception)
            {
                DisplayError(exception);
            }
        }
        protected void btnDisapprovePayment_Click(object sender, EventArgs args)
        {
            try
            {
                int paymentID = int.Parse(approvalPaymentID.Value);
                EnsureConnectedToFidesic();
                FidesicController.UpdateApprovalStatus(paymentID, ApprovalType.Payment, ApprovalStatus.Disapprove);
            }
            catch (System.Exception exception)
            {
                DisplayError(exception);
            }
        }

        #endregion
        //------\\ Methods //-----------------------------------------------//
    }
}