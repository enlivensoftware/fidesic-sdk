﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using Fidesic;
using Fidesic.Data;
using Fidesic.Web;

namespace FidesicSDK
{
    public partial class APICustomersService : System.Web.UI.Page
    {
        #region Properties
        private LoginByAPIController _fidesicController = null;
        public LoginByAPIController FidesicController
        {
            get
            {
                if (_fidesicController == null)
                {
                    _fidesicController = new LoginByAPIController();
                }

                return _fidesicController;
            }
        }
        #region logins
        public class LoginInfo
        {
            public string name;
            public string apiKey;
            public string apiPW;

            public LoginInfo() { }
            public LoginInfo(string _name, string _apiKey, string _apiPW)
            {
                name = _name;
                apiKey = _apiKey;
                apiPW = _apiPW;
            }
        }
        private List<LoginInfo> _agencies;
        public List<LoginInfo> agencies
        {
            get
            {
                if (_agencies == null)
                {
                    _agencies = new List<LoginInfo>()
                    {
                        new LoginInfo("The World Online","jyJEjoxVZCBkFluU","9EH6gsojKuh43tNssk2U"),
                        new LoginInfo("The QB Online","zGUBPLqSzaLVgwCf","BEtNltqHrlVrHfhfbklx")                        
                    };
                }
                return _agencies;
            }
        }
        private LoginInfo _currentLogin;
        protected LoginInfo currentLogin
        {
            get
            {
                if (_currentLogin == null || _currentLogin.name != loginList.Value)
                {
                    _currentLogin = agencies.First(a => a.name == loginList.Value);
                }
                return _currentLogin;
            }
        }
        #endregion

        public FidesicUser CurrentUser;
        public List<Customer> GetAllCustomers;
        public decimal? CustomerBalance;
        public Customer customer;
        public int newCustomerID;
        public int mergedCustomerID;
        public string mergedMessage = "";
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                foreach (var a in agencies)
                {
                    loginList.Items.Add(new ListItem { Text = a.name, Value = a.name });
                }
            }
        }

        public void EnsureConnectedToFidesic()
        {
            try
            {
                if (!FidesicController.ConnectedToFidesic
                    || FidesicController.CurrentUser == null
                    || FidesicController.CurrentUser.CompanyName != loginList.Value)
                {
                    bool allowSwitch = (FidesicController.ConnectedToFidesic && FidesicController.CurrentUser != null && FidesicController.CurrentUser.CompanyName != loginList.Value);

                    FidesicController.ConnectToFidesic(currentLogin.apiKey, currentLogin.name, currentLogin.apiPW, allowSwitch);
                }
            }
            catch (NotConnectedToFidesicException) { FidesicController.ConnectToFidesic(currentLogin.apiKey, currentLogin.name, currentLogin.apiPW); }
            catch (FidesicSessionExpiredException) { FidesicController.ConnectToFidesic(currentLogin.apiKey, currentLogin.name, currentLogin.apiPW); }
        }

        public void DisplayError(System.Exception exception)
        {
            DisplayError(exception.ToString());
        }
        public void DisplayError(string errorMessage)
        {
            errorPlaceholder.Visible = true;
            errorLiteral.Text = errorMessage;
        }


        #region "Event Handlers"

        protected void btnLogin_Click(object sender, EventArgs args)
        {
            try
            {
                EnsureConnectedToFidesic();
                DisplayError("Successfully logged in: " + DateTime.Now.ToString());
                //DisplayError("Logged in as: " + CurrentUser.Email);
            }
            catch (System.Exception exception)
            {
                DisplayError(exception);
            }
        }


        protected void btnGetCustomerBalance_Click(object sender, EventArgs args)
        {
            try
            {
                EnsureConnectedToFidesic();
                CustomerBalance = FidesicController.GetCustomerBalance(balanceCustomerNumber.Value);

            }
            catch (System.Exception exception)
            {
                DisplayError(exception);
            }
        }

        protected void btnGetAllCustomers_Click(object sender, EventArgs args)
        {
            try
            {
                EnsureConnectedToFidesic();
                GetAllCustomers = FidesicController.GetAllCustomers();
                
            }
            catch (System.Exception exception)
            {
                DisplayError(exception);
            }
        }

        protected void btnAddCustomer_Click(object sender, EventArgs args)
        {
            try
            {
                int id = 0;
                int.TryParse(customerID.Value, out id);

                Customer newCustomer = new Customer()
                {
                    CompanyName = company.Value,
                    CustomerNumber = customerNumber.Value,
                    Email = emailAddress.Value,
                    AddressInfo = new Address()
                    {
                        Address1 = address1.Value,
                        Address2 = address2.Value,
                        City = City.Value,
                        State = State.Value,
                        Zip = Zip.Value
                    },
                    FirstName = firstName.Value,
                    LastName = lastName.Value,
                    PhoneNumber = phoneNumber.Value,
                    UserID=id
                };

                EnsureConnectedToFidesic();
                if(id > 0)
                    FidesicController.UpdateCustomer(newCustomer);
                else
                    newCustomerID = FidesicController.AddCustomer(newCustomer);

                if (newCustomerID != 0)
                    customerID.Value = newCustomerID.ToString();
            }
            catch (System.Exception exception)
            {
                DisplayError(exception);
            }
        }
        

        protected void btnGetCustomer_Click(object sender, EventArgs args)
        {
            try
            {
                int id = 0;
                int.TryParse(customerID.Value, out id);
                EnsureConnectedToFidesic();
                customer = FidesicController.GetCustomer(id);

                if (customer != null)
                {
                    customerID.Value = customer.UserID.ToString();
                    company.Value = customer.CompanyName;
                    customerNumber.Value = customer.CustomerNumber;
                    emailAddress.Value = customer.Email;
                    if (customer.AddressInfo != null)
                    {
                        address1.Value = customer.AddressInfo.Address1;
                        address2.Value = customer.AddressInfo.Address2;
                        City.Value = customer.AddressInfo.City;
                        State.Value = customer.AddressInfo.State;
                        Zip.Value = customer.AddressInfo.Zip;
                    }
                    firstName.Value = customer.FirstName;
                    lastName.Value = customer.LastName;
                    phoneNumber.Value = customer.PhoneNumber;
                }
            }
            catch (System.Exception exception)
            {
                DisplayError(exception);
            }
        }

        protected void btnDeleteCustomer_Click(object sender, EventArgs args)
        {
            try
            {
                EnsureConnectedToFidesic();
                FidesicController.DeleteCustomer(1);

            }
            catch (System.Exception exception)
            {
                DisplayError(exception);
            }
        }

        protected void btnMergeCustomer_Click(object sender, EventArgs args)
        {
            try
            {
                string oldCustomerNumber = oldCust.Value;
                string newCustomerNumber = newCust.Value;

                if (string.IsNullOrWhiteSpace(oldCustomerNumber) || string.IsNullOrWhiteSpace(newCustomerNumber))
                    throw new Exception("Either old or new customer number is empty.");

                EnsureConnectedToFidesic();
                try
                {
                    mergedCustomerID = FidesicController.MergeCustomers(oldCustomerNumber, newCustomerNumber);
                }
                catch (Exception ex)
                {
                    if(ex.Message.StartsWith("Could not find old customer"))
                        mergedMessage = oldCustomerNumber + " has been merged into " + newCustomerNumber + ". <br>Fidesic Contact UserID: " + mergedCustomerID.ToString();
                    else
                        throw ex;
                }
            }
            catch (System.Exception exception)
            {
                DisplayError(exception);
            }
        }
        #endregion
    }
}