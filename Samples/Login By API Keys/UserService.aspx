﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="UserService.aspx.cs" Inherits="FidesicSDK.APIUserService" %>
<%@ Import Namespace="Fidesic.Data" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
    <head runat="server">
        <title>User Service Runner</title>
    </head>
    
    <body>
        <a href="../Index.html">Back</a><br>
        <form id="form1" runat="server">
            <div style="float:right"><%=FidesicController.ConnectedToFidesic && FidesicController.CurrentUser != null ? FidesicController.CurrentUser.CompanyName + " : " + FidesicController.CurrentUser.UserID : "Not logged in." %></div>
            <div class="runner get_profile">
                <asp:PlaceHolder runat="server" ID="errorPlaceholder" Visible="false" EnableViewState="false">
                    <div class="error_container">
                        <p class="error_message">
                            <asp:Literal runat="server" ID="errorLiteral" />
                        </p>
                    </div>
                </asp:PlaceHolder>
                
                
                <fieldset>
                    <legend>Get Profile</legend>
                     
                    <div class="field email">
                        <select id="loginList" runat="server" />
                    </div>
                </fieldset>

                <% if (CurrentUser != null) { %>
                    <div class="result_pane">
                        <strong>First Name:</strong> <input type="text" id="firstName" runat="server"/><br>
                        <strong>Middle:</strong> <input type="text" id="middleName" runat="server"/><br>
                        <strong>Last Name:</strong> <input type="text" id="lastName" runat="server" /><br>
                        <strong>Email:</strong> <input type="text" id="emailAddress" runat="server"/><br>
                        <strong>Company:</strong> <input type="text" id="company" runat="server"/><br>
                        <strong>Phone:</strong> <input type="text" id="phoneNumber" runat="server" /><br>
                        <input runat="server" type="button" id="updateProfile" value="Update Profile" onserverclick="btnUpdateProfile_Click" />
                    </div>
                <% } %>
                <fieldset>
                    <legend>Get user Permissions</legend>
                     
                    <div class="submit_field">
                        <input runat="server" type="button" id="Button4" value="Get User Permissions" onserverclick="btnGetPermissions_Click" />
                    </div>
                </fieldset>

                <% if (UserPermissions != null) { %>
                    <div class="result_pane">
                        <h3>Payables</h3>
                        <strong>PayablesAccess:</strong> <%=UserPermissions.PayablesAccess %><br>
                        <strong>PayablesPay:</strong> <%=UserPermissions.PayablesPay %><br>
                        <strong>PayablesViewInvoices:</strong> <%=UserPermissions.PayablesViewInvoices %><br>
                        <strong>PayablesViewPayments:</strong> <%=UserPermissions.PayablesViewPayments %><br>
                        <h3>Receivables</h3>
                        <strong>ReceivablesAccess:</strong> <%=UserPermissions.ReceivablesAccess %><br>
                        <strong>ReceivablesSendInvoice:</strong> <%=UserPermissions.ReceivablesSendInvoice %><br>
                        <strong>ReceivablesViewInvoices:</strong> <%=UserPermissions.ReceivablesViewInvoices %><br>
                        <strong>ReceivablesViewPayments:</strong> <%=UserPermissions.ReceivablesViewPayments %><br>
                        <strong>ReceivablesViewCustomers:</strong> <%=UserPermissions.ReceivablesViewCustomers %><br>
                        <h3>Banking</h3>
                        <strong>BankingAccess:</strong> <%=UserPermissions.BankingAccess %><br>
                        <strong>BankingManageAccounts:</strong> <%=UserPermissions.BankingManageAccounts %><br>
                        <strong>BankingViewAll:</strong> <%=UserPermissions.BankingViewAll %><br>
                    </div>
                <% } %>
            </div>
            
            <div class="runner create_user">
                <fieldset>
                    <legend>Create User</legend>

                    <div class="field first_name">
                        <label for="<%= txtFirstName.ClientID %>">First Name:</label>
                        <input runat="server" type="text" id="txtFirstName" class="text" />
                    </div>

                    <div class="field last_name">
                        <label for="<%= txtLastName.ClientID %>">Last Name:</label>
                        <input runat="server" type="text" id="txtLastName" class="text" />
                    </div>

                    <div class="field company_name">
                        <label for="<%= txtCompanyName.ClientID %>">Company Name:</label>
                        <input runat="server" type="text" id="txtCompanyName" class="text" />
                    </div>

                    <div class="field email">
                        <label for="<%= txtEmail2.ClientID %>">Email:</label>
                        <input runat="server" type="text" id="txtEmail2" class="text" />
                    </div>

                    <div class="field password">
                        <label for="<%= txtPassword2.ClientID %>">Password:</label>
                        <input runat="server" type="password" id="txtPassword2" class="text password" />
                    </div>

                    <div class="field secret_question">
                        <label for="<%= ddlSecretQuestion.ClientID %>">Secret Question:</label>
                        <select runat="server" id="ddlSecretQuestion" class="select">
                            <option value="-1">Please select a Secret Question...</option>
                        </select>
                    </div>

                    <div class="field secret_answer">
                        <label for="<%= txtSecretAnswer.ClientID %>">Secret Answer:</label>
                        <input runat="server" type="text" id="txtSecretAnswer" class="text" />
                    </div>

                    <div class="submit_field">
                        <input runat="server" type="button" id="btnCreateUser" value="Create User" onserverclick="btnCreateUser_Click" />
                    </div>
                </fieldset>
                <fieldset>
                    <div class="submit_field">
                        <input runat="server" type="button" id="btnListAccounts" value="List Accounts" onserverclick="btnListAccounts_Click" />
                    </div>
                    <%if (list != null && list.Count > 0)
                      { %>
                      Accounts:<br>
                      <%foreach(var x in list){ %>
                        <input type="radio" name="accountID" value="<%=x.UserID %>" />  <%=x.CompanyName %> : <%=x.LoginName %> <%=x.PendingInvoiceApprovals > 0 ? x.PendingInvoiceApprovals + " invoice approvals" : "" %> <%=x.PendingPaymentApprovals > 0 ? x.PendingPaymentApprovals + " payment approvals" : "" %><br>
                      <%} %>
                        <input runat="server" type="button" id="Button1" value="Switch Accounts" onserverclick="btnSwitchAccounts_Click" />
                    <%} %>
                </fieldset>
            </div>

            <fieldset>
                <legend>Lost Password?</legend>
                     
                    <div class="field email">
                        <label for="<%= LostPasswordEmail.ClientID %>">Email:</label>
                        <input runat="server" type="text" id="LostPasswordEmail" class="text" />
                    </div>

                     <div class="submit_field">
                        <input runat="server" type="button" id="Button2" value="Lost Password?" onserverclick="btnLostPassword_Click" />
                    </div>
            </fieldset>

            <fieldset>
                <legend>Lost Password Reset</legend>
                     
                <div class="field email">
                    <label for="<%= LostPasswordResetEmail.ClientID %>">Email:</label>
                    <input runat="server" type="text" id="LostPasswordResetEmail" class="text" />
                </div>
                <div class="field email">
                    <label for="<%= resetCode.ClientID %>">resetCode:</label>
                    <input runat="server" type="text" id="resetCode" class="text" />
                </div>
                <div class="field email">
                    <label for="<%= newPassword.ClientID %>">new password:</label>
                    <input runat="server" type="text" id="newPassword" class="text" />
                </div>

                <div class="submit_field">
                    <input runat="server" type="button" id="Button3" value="Reset Lost Password" onserverclick="btnLostPasswordReset_Click" />
                </div>
            </fieldset>

        </form>
    </body>
</html>
