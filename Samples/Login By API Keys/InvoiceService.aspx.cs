﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using Fidesic;
using Fidesic.Data;

namespace FidesicSDK
{
    public partial class APIInvoiceService : System.Web.UI.Page
    {
        #region Properties
        //------// Properties \\--------------------------------------------\\
        private LoginByAPIController _fidesicController = null;
        public LoginByAPIController FidesicController
        {
            get
            {
                if (_fidesicController == null)
                {
                    _fidesicController = new LoginByAPIController();
                }

                return _fidesicController;
            }
        }
        //------\\ Properties //--------------------------------------------//

        #region logins
        public class LoginInfo
        {
            public string name;
            public string apiKey;
            public string apiPW;

            public LoginInfo() { }
            public LoginInfo(string _name, string _apiKey, string _apiPW)
            {
                name = _name;
                apiKey = _apiKey;
                apiPW = _apiPW;
            }
        }
        private List<LoginInfo> _agencies;
        public List<LoginInfo> agencies
        {
            get
            {
                if (_agencies == null)
                {
                    _agencies = new List<LoginInfo>()
                    {
                        new LoginInfo("The World Online","jyJEjoxVZCBkFluU","9EH6gsojKuh43tNssk2U"),
                        new LoginInfo("The QB Online","zGUBPLqSzaLVgwCf","BEtNltqHrlVrHfhfbklx")
                    };
                }
                return _agencies;
            }
        }
        private LoginInfo _currentLogin;
        protected LoginInfo currentLogin
        {
            get
            {
                if (_currentLogin == null || _currentLogin.name != loginList.Value)
                {
                    _currentLogin = agencies.First(a => a.name == loginList.Value);
                }
                return _currentLogin;
            }
        }
        #endregion


        //------// Fields \\------------------------------------------------\\
        public FidesicUser CurrentUser;
        public List<Invoice> GetAllSentInvoices;
        public List<Invoice> GetAPSyncInvoices;
        public List<Invoice> GetAllReceivedInvoices;
        public List<ItemTemplate> GetAllInvoiceItemTemplates;
        public List<VendorLocation> GetVendorLocations;
        public List<InvoiceCategory> GetInvoiceCategories;
        public List<InvoiceTerm> GetInvoiceTerms;
        public int? InvoiceID = -1;
        public bool ExpenseAdded = false;
        public bool InvoiceVoided = false;
        public bool MarkedAsExported = false;
        public int? LocationID = -1;
        public int ItemTemplateID = -1;
        public bool ItemDeleted = false;
        public int InvoiceTermID = -1;
        public bool TermDeleted = false;
        //------\\ Fields //------------------------------------------------//



        //------// Methods \\-----------------------------------------------\\
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                foreach (var a in agencies)
                {
                    loginList.Items.Add(new ListItem { Text = a.name, Value = a.name });
                }
            }
        }

        public void EnsureConnectedToFidesic()
        {
            try
            {
                if (!FidesicController.ConnectedToFidesic
                    || FidesicController.CurrentUser == null
                    || FidesicController.CurrentUser.CompanyName != loginList.Value)
                {
                    bool allowSwitch = (FidesicController.ConnectedToFidesic && FidesicController.CurrentUser != null && FidesicController.CurrentUser.CompanyName != loginList.Value);

                    FidesicController.ConnectToFidesic(currentLogin.apiKey, currentLogin.name, currentLogin.apiPW, allowSwitch);
                }
            }
            catch (NotConnectedToFidesicException) { FidesicController.ConnectToFidesic(currentLogin.apiKey, currentLogin.name, currentLogin.apiPW); }
            catch (FidesicSessionExpiredException) { FidesicController.ConnectToFidesic(currentLogin.apiKey, currentLogin.name, currentLogin.apiPW); }
        }

        public void DisplayError(System.Exception exception)
        {
            DisplayError(exception.ToString());
        }
        public void DisplayError(string errorMessage)
        {
            errorPlaceholder.Visible = true;
            errorLiteral.Text = errorMessage;
        }


        #region "Event Handlers"

        protected void btnLogin_Click(object sender, EventArgs args)
        {
            try
            {
                EnsureConnectedToFidesic();
                DisplayError("Successfully logged in: " + DateTime.Now.ToString());
                //DisplayError("Logged in as: " + CurrentUser.Email);
            }
            catch (System.Exception exception)
            {
                DisplayError(exception);
            }
        }


        protected void btnGetAllSentInvoices_Click(object sender, EventArgs args)
        {
            try
            {
                EnsureConnectedToFidesic();
                GetAllSentInvoices = FidesicController.GetSentInvoicesWithTotals(1, 100, null, null, customerName.Value ?? null).Invoices;
            }
            catch (System.Exception exception)
            {
                DisplayError(exception);
            }
        }

        protected void btnGetAPSync_Click(object sender, EventArgs args)
        {
            try
            {
                EnsureConnectedToFidesic();
                GetAPSyncInvoices = FidesicController.SyncAPInvoices();
            }
            catch (System.Exception exception)
            {
                DisplayError(exception);
            }
        }

        protected void btnVoidInvoice_Click(object sender, EventArgs args)
        {
            try
            {
                int voidId = int.Parse(voidInvoiceID.Value);
                EnsureConnectedToFidesic();
                InvoiceVoided = FidesicController.VoidInvoice(voidId, "reason");
            }
            catch (System.Exception exception)
            {
                DisplayError(exception);
            }
        }

        protected void btnGetAllReceivedInvoices_Click(object sender, EventArgs args)
        {
            try
            {
                EnsureConnectedToFidesic();
                //var x = FidesicController.GetReceivedInvoicesWithTotals(1, 40, DateTime.Parse("12/1/10 4:00"), DateTime.Parse("1/1/2032"), null);
                //GetAllReceivedInvoices = x.Invoices;
                GetAllReceivedInvoices = FidesicController.GetReceivedInvoices(1, 100, null, null, vendorName.Value ?? null);
                
            }
            catch (System.Exception exception)
            {
                DisplayError(exception);
            }
        }


        protected void btnSubmitExpense_Click(object sender, EventArgs args)
        {
            try
            {
                //These are all of the required and optional fields that can be utilized. Anything else will be ignored.
                decimal.TryParse(expenseAmount.Value, out decimal ExpenseAmount);

                Invoice newInvoice = new Invoice
                {
                    InvoiceNumber = expenseNumber.Value,
                    Amount = ExpenseAmount,
                    Description = expenseDesc.Value,
                    Status = InvoiceStatus.Sent,
                    Memo = expenseMemo.Value, //optional
                    Term = expenseTerm.Value, // optional
                    LineItems = new List<InvoiceItem>() //Optionally Line items could be added here.
                };

                for (var j = 1; j <= 250; j++)
                {
                    InvoiceItem i = new InvoiceItem
                    {
                        Description = j.ToString() + ": Bill for ISSUANCE OF AMEL FOR WARD SEAN & LEWIS NOEL V",
                        UnitPrice = 1000,
                        Quantity = 1,
                        LineNumber = j
                        //ItemCode = "FEL"
                    };

                    newInvoice.LineItems.Add(i);
                }
                
                //for vendorLocation please see the VendorLocationService for usage info.
                //newInvoice.VendorLocation = new VendorLocation();
                //newInvoice.VendorLocation.LocationID = FidesicLocationID;

                //if LocationID is not known, display name can be passed in. It will try to match 
                //internally. However, display name is not guaranteed to be unique. LocationID is better 
                //newInvoice.VendorLocation.LocationName = displayName;
                EnsureConnectedToFidesic();
                ExpenseAdded = FidesicController.SubmitExpense(newInvoice);
            }
            catch (System.Exception exception)
            {
                DisplayError(exception);
            }
        }
        protected void btnAddInvoice_Click(object sender, EventArgs args)
        {
            try
            {
                //These are all of the required and optional fields that can be utilized. Anything else will be ignored.
                decimal.TryParse(invoiceAmount.Value, out decimal InvoiceAmount);
                DateTime.TryParse(invoiceDate.Value, out DateTime InvoiceDate);
                DateTime.TryParse(dueDate.Value, out DateTime DueDate);

                Invoice newInvoice = new Invoice
                {
                    InvoiceNumber = invoiceNumber.Value,
                    Amount = InvoiceAmount,
                    InvoiceDate = InvoiceDate,
                    DueDate = DueDate,
                    Status = InvoiceStatus.Sent,
                    Customer = new Customer()
                    {
                        CompanyName = custName.Value,
                        CustomerNumber = custNum.Value,
                        FirstName = custFirstName.Value,
                        LastName = custLastName.Value,
                        Email = custEmail.Value,
                        PhoneNumber = custPhone.Value,
                        AddressInfo = new Address()
                        {
                            Address1 = address1.Value,
                            Address2 = address2.Value,
                            City = City.Value,
                            State = State.Value,
                            Zip = Zip.Value,
                            Country = Country.Value
                        },
                    },
                    Description = invoiceDesc.Value,
                    Memo = invoiceMemo.Value, // optional
                    Term = invoiceTerm.Value, // optional
                    LineItems = new List<InvoiceItem>() //Optionally Line items could be added here.
                };

                for (var j = 1; j <= 250; j++)
                {
                    InvoiceItem i = new InvoiceItem
                    {
                        Description = j.ToString() + ": Bill for ISSUANCE OF AMEL FOR WARD SEAN & LEWIS NOEL V",
                        UnitPrice = 1000,
                        Quantity = 1,
                        LineNumber = j,
                        //ItemCode = "FEL"
                        RevenueCode = "REN",
                        RevenueName = "RENT"
                    };

                    newInvoice.LineItems.Add(i);
                }

                newInvoice.Category = new InvoiceCategory()
                {
                    CategoryName = "RENT",
                    InvoiceCode = "REN"
                };

                //for vendorLocation please see the VendorLocationService for usage info.
                //newInvoice.VendorLocation = new VendorLocation()
                //{
                //    LocationID = 627,
                //    LocationCode = "PHC",
                //    LocationName = "Port Harcourt International Airport Omagwa"
                //};

                //if fidesicLocationID is not known, display name can be passed in. It will try to match 
                //internally. However, display name is not guaranteed to be unique. LocationID is better 
                //newInvoice.VendorLocation.LocationName = displayName;
                EnsureConnectedToFidesic();
                InvoiceID = FidesicController.AddInvoice(newInvoice);
            }
            catch (System.Exception exception)
            {
                DisplayError(exception);
            }
        }

        protected void btnMarkInvoiceAsExported_Click(object sender, EventArgs args)
        {
            try
            {
                EnsureConnectedToFidesic();
                MarkedAsExported = FidesicController.MarkInvoiceAsExported(exportedInvoiceID.Value);
            }
            catch (System.Exception exception)
            {
                DisplayError(exception);
            }
        }
        protected void btnGetVendorLocations_Click(object sender, EventArgs args)
        {
            try
            {
                EnsureConnectedToFidesic();
                GetVendorLocations = FidesicController.GetVendorLocations();
            }
            catch (System.Exception exception)
            {
                DisplayError(exception);
            }
        }
        protected void btnAddVendorLocation_Click(object sender, EventArgs args)
        {
            try
            {
                VendorLocation location = new VendorLocation
                {
                    LocationCode = LocationCode.Value,
                    LocationName = LocationName.Value
                };

                EnsureConnectedToFidesic();
                LocationID = FidesicController.AddVendorLocation(location);
            }
            catch (System.Exception exception)
            {
                DisplayError(exception);
            }
        }
        protected void btnGetInvoiceCategories_Click(object sender, EventArgs args)
        {
            try
            {
                EnsureConnectedToFidesic();
                GetInvoiceCategories = FidesicController.GetInvoiceCategories();
            }
            catch (System.Exception exception)
            {
                DisplayError(exception);
            }
        }

        protected void btnGetLineItemTemplates_Click(object sender, EventArgs args)
        {
            try
            {
                EnsureConnectedToFidesic();
                GetAllInvoiceItemTemplates = FidesicController.GetAllItemTemplates();
            }
            catch (System.Exception exception)
            {
                DisplayError(exception);
            }
        }
        protected void btnAddLineItemTemplate_Click(object sender, EventArgs args)
        {
            try
            {
                ItemTemplate item = new ItemTemplate
                {
                    Description = Description.Value,
                    ItemCode = ItemCode.Value,
                    Name = Name.Value,
                    UnitPrice = decimal.Parse(UnitPrice.Value)
                };
                EnsureConnectedToFidesic();
                ItemTemplateID = FidesicController.AddItemTemplate(item);
            }
            catch (System.Exception exception)
            {
                DisplayError(exception);
            }
        }
        protected void btnDeleteLineItemTemplate_Click(object sender, EventArgs args)
        {
            try
            {
                int deleteID = int.Parse(deleteItemID.Value);
                EnsureConnectedToFidesic();
                FidesicController.DeleteItemTemplate(deleteID);
                ItemDeleted = true;
            }
            catch (System.Exception exception)
            {
                DisplayError(exception);
            }
        }

        protected void btnGetInvoiceTerms_Click(object sender, EventArgs args)
        {
            try
            {
                EnsureConnectedToFidesic();
                GetInvoiceTerms = FidesicController.GetInvoiceTermOptions();
            }
            catch (System.Exception exception)
            {
                DisplayError(exception);
            }
        }

        protected void btnAddInvoiceTerm_Click(object sender, EventArgs args)
        {
            byte.TryParse(dueTypeID.Value, out byte DueTypeID);
            int.TryParse(termDueDate.Value, out int DueDateDays);
            byte.TryParse(discountTypeID.Value, out byte DiscountTypeID);
            int.TryParse(termDiscountDate.Value, out int DiscountDateDays);
            decimal.TryParse(discountPercentage.Value, out decimal DiscountPercentage);

            InvoiceTerm term = new InvoiceTerm
            {
                Description = termName.Value,
                DueTypeID = DueTypeID,
                DueDateDays = DueDateDays,
                DiscountTypeID = DiscountTypeID,
                DiscountDateDays = DiscountDateDays,
                DiscountPercentage = DiscountPercentage
            };

            try
            {
                EnsureConnectedToFidesic();
                InvoiceTermID = FidesicController.AddTerm(term);
            }
            catch (System.Exception exception)
            {
                DisplayError(exception);
            }
        }

        protected void btnDeleteInvoiceTerm_Click(object sender, EventArgs args)
        {
            try
            {
                EnsureConnectedToFidesic();
                FidesicController.DeleteTerm(termToDelete.Value);
                TermDeleted = true;
            }
            catch (System.Exception exception)
            {
                DisplayError(exception);
            }
        }

        #endregion
        //------\\ Methods //-----------------------------------------------//
    }
}