﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PaymentService.aspx.cs" Inherits="FidesicSDK.APIPaymentService" %>
<%@ Import Namespace="Fidesic.Data" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
    <head runat="server">
        <title>Payment Service Runner</title>
    </head>
    
    <body>
        <a href="../Index.html">Back</a><br>
        <form id="form1" runat="server">
            <div style="float:right;" class="submit_field">
                <input runat="server" type="button" id="btnResetLogin" value="Log out" onserverclick="btnResetLogin_Click" />
            </div>
            <div class="runner login">
                <asp:PlaceHolder runat="server" ID="errorPlaceholder" Visible="false" EnableViewState="false">
                    <div class="error_container">
                        <p class="error_message">
                            <asp:Literal runat="server" ID="errorLiteral" />
                        </p>
                    </div>
                </asp:PlaceHolder>
                
                
                <fieldset>
                    <legend>Agency</legend>
                     
                    <div class="field email">
                        <select id="loginList" runat="server" />
                    </div>
                    
                </fieldset>
            </div>


            <fieldset runat="server" id="updatePaymentStatusFieldset" visible="false" enableviewstate="false">
                    <legend>Update Payment Status</legend>

                    <div class="field PaymentID">
                        <label for="<%= txtPaymentID.ClientID %>">Payment ID:</label>
                        <input runat="server" type="text" id="txtPaymentID" class="text" />
                    </div>

                    <div class="field txtMerchantReferenceNumber">
                        <label for="<%= txtMerchantReferenceNumber.ClientID %>">Merchant ReferenceNumber:</label>
                        <input runat="server" type="text" id="txtMerchantReferenceNumber" class="text" />
                    </div>

                    <div class="successful">
                        <label>Successful:</label>
                        <asp:RadioButtonList runat="server" ID="successUpdate" CssClass="radio_list">
                            <asp:ListItem Value="True">true</asp:ListItem>
                            <asp:ListItem Value="False">false</asp:ListItem>
                        </asp:RadioButtonList>
                    </div>
                    <div class="field txtRejectionDescription">
                        <label for="<%= txtRejectionDescription.ClientID %>">Rejection Description:</label>
                        <input runat="server" type="text" id="txtRejectionDescription" class="text" />
                    </div>

                    <div class="field txtRejectionCode">
                        <label for="<%= txtRejectionCode.ClientID %>">Routing Number:</label>
                        <input runat="server" type="text" id="txtRejectionCode" class="text" />
                    </div>

                    <div class="submit_field">
                        <input runat="server" type="button" id="btnUpdatePaymentStatus" value="Update Payment Status" onserverclick="btnUpdatePaymentStatus_Click" />
                    </div>
                </fieldset>
           <fieldset runat="server" id="GetPaymentFieldSet" visible="false" enableviewstate="false">
                <legend>Get Payment</legend>

                <div class="field PaymentID">
                    <label for="<%= txtGetPaymentID.ClientID %>">Payment ID:</label>
                    <input runat="server" type="text" id="txtGetPaymentID" class="text" />
                </div>


                <div class="submit_field">
                    <input runat="server" type="button" id="btnGetPayment" value="Get Payment" onserverclick="btnGetPayment_Click" />
                </div>

                <%if (GetPayment != null)
                  {%>
                    <strong>Payment ID: </strong><%= GetPayment.PaymentID.ToString()%><br>
                    <strong>Payment Date: </strong><%= GetPayment.ProcessingDate.ToString()%><br>
                    <strong>Status: </strong><%= GetPayment.Status.ToString()%><br>
                    <strong>Memo: </strong><%= GetPayment.Memo%><br>
                    <strong>PaymentNumber: </strong><%= GetPayment.PaymentNumber%><br>
                    <strong>Reference Number: </strong><%= GetPayment.ReferenceNumber.ToString()%><br>
                    <strong>Value Date: </strong><%= GetPayment.DueDate.ToString()%><br>
                    <strong>Amount: </strong><%= GetPayment.Amount.ToString()%><br>
                <%} %>
            </fieldset>
           <fieldset runat="server" id="GetReceivedPaymentsFieldSet" visible="false" enableviewstate="false">
                <legend>Get Received Payments</legend>

                <div class="submit_field">
                    <input runat="server" type="button" id="btnGetAllReceivedPayments" value="Get Received Payments" onserverclick="btnGetAllReceivedPayments_Click" />
                </div>

                <% if (GetAllReceivedPayments != null)
                    { %>
                    <% if (GetAllReceivedPayments.Count == 0)
                        { %>
                        <p class="empty">You do not have any received payments.</p>
                    <% } else { %>
                        <% foreach (Payment pmnt in GetAllReceivedPayments)
                            { %>
                            <strong>Payment ID: </strong><%= pmnt.PaymentID.ToString()%><br>
                            <strong>Created Date: </strong><%= pmnt.CreatedDate.ToString()%><br>
                            <strong>Payment Date: </strong><%= pmnt.ProcessingDate.ToString()%><br>
                            <strong>Payer Name: </strong><%= pmnt.PayerName.ToString()%><br>
                            <strong>Status: </strong><%= pmnt.Status.ToString()%><br>
                            <strong>Memo: </strong><%= pmnt.Memo%><br>
                            <strong>PaymentNumber: </strong><%= pmnt.PaymentNumber%><br>
                            <strong>Reference Number: </strong><%= pmnt.ReferenceNumber.ToString()%><br>
                            <strong>Value Date: </strong><%= pmnt.DueDate.ToString()%><br>
                            <strong>Amount: </strong><%= pmnt.Amount.ToString() %><br>
                            <br>
                        <% } %>
                    <% } %>
                <% } %>
            </fieldset>
            <fieldset runat="server" id="GetSentPayments" visible="false" enableviewstate="false">
                <legend>Get Sent Payments</legend>

                <div class="submit_field">
                    <input runat="server" type="button" id="Button2" value="Get Sent Payments" onserverclick="btnGetAllSentPayments_Click" />
                </div>

                <% if (GetAllSentPayments != null)
                    { %>
                    <% if (GetAllSentPayments.Count == 0)
                        { %>
                        <p class="empty">You do not have any sent payments.</p>
                    <% } else { %>
                        <% foreach (Payment pmnt in GetAllSentPayments)
                            { %>
                            <strong>Payment ID: </strong><%= pmnt.PaymentID.ToString()%><br>
                            <strong>Payment Date: </strong><%= pmnt.ProcessingDate.ToString()%><br>
                            <strong>Payee Name: </strong><%= pmnt.PayeeName.ToString()%><br>
                            <strong>Status: </strong><%= pmnt.Status.ToString()%><br>
                            <strong>Memo: </strong><%= pmnt.Memo%><br>
                            <strong>PaymentNumber: </strong><%= pmnt.PaymentNumber%><br>
                            <strong>Reference Number: </strong><%= pmnt.ReferenceNumber.ToString()%><br>
                            <strong>Value Date: </strong><%= pmnt.DueDate.ToString()%><br>
                            <strong>Amount: </strong><%= pmnt.Amount.ToString() %><br>
                        <% } %>
                    <% } %>
                <% } %>
            </fieldset>
            <fieldset runat="server" id="ProcessNonmemberPaymentFieldSet" visible="true" enableviewstate="false">
                <legend>Process Nonmember Payment</legend>
                     <% if (PaymentID != 0)
                        { %>
                     Payment ID = <%=PaymentID %>       
                     <%} %>
                            
                    <div class="field amount">
                        <label for="<%= txtAmount.ClientID %>">Amount:</label>
                        <input runat="server" type="text" id="txtAmount" class="text" />
                    </div>

                    <div class="field name">
                        <label for="<%= txtName.ClientID %>">Name:</label>
                        <input runat="server" type="text" id="txtName" class="text" />
                    </div>
                    <div class="field name">
                        <label for="<%= txtCardNumber.ClientID %>">CardNumber:</label>
                        <input runat="server" type="text" id="txtCardNumber" class="text" />
                    </div>
                    <div class="field name">
                        <label for="<%= txtExpire.ClientID %>">Expire Date:</label>
                        <input runat="server" type="text" id="txtExpire" class="text" />
                    </div>
                    <div class="field name">
                        <label for="<%= txtAddress1.ClientID %>">Address:</label>
                        <input runat="server" type="text" id="txtAddress1" class="text" />
                    </div>
                    <div class="field name">
                        <label for="<%= txtAddress2.ClientID %>">Address 2:</label>
                        <input runat="server" type="text" id="txtAddress2" class="text" />
                    </div>
                    <div class="field name">
                        <label for="<%= txtCity.ClientID %>">City:</label>
                        <input runat="server" type="text" id="txtCity" class="text" />
                    </div>
                    <div class="field name">
                        <label for="<%= txtState.ClientID %>">State:</label>
                        <input runat="server" type="text" id="txtState" class="text" />
                    </div>
                    <div class="field name">
                        <label for="<%= txtZip.ClientID %>">Zip:</label>
                        <input runat="server" type="text" id="txtZip" class="text" />
                    </div>
                    <div class="field name">
                        <label for="<%= txtEmailAddress.ClientID %>">Email Address:</label>
                        <input runat="server" type="text" id="txtEmailAddress" class="text" />
                    </div>


                <div class="submit_field">
                    <input runat="server" type="button" id="btnProcessNonmemberPayment" value="Process Nonmember Payment" onserverclick="btnProcessNonmemberPayment_Click" />
                </div>
            </fieldset>
            <fieldset runat="server" id="Fieldset1" visible="true" enableviewstate="false">
                <legend>Process Member Payment</legend>
                    
                <div class="field amount">
                    <label for="<%= txtInvoice.ClientID %>">InvoiceID:</label>
                    <input runat="server" type="text" id="txtInvoice" class="text" />
                </div>

                <div class="submit_field">
                    <input runat="server" type="button" id="Button1" value="Process Member Payment" onserverclick="btnProcessPayment_Click" />
                </div>
            </fieldset>
            <fieldset runat="server" id="SendPayablesPaymentFieldset" visible="true" enableviewstate="false">
                <legend>Send Payables Payment</legend>
                 
                <div class="field name">
                    <label for="<%= txtPayableVendor.ClientID %>">Vendor Number:</label>
                    <input runat="server" type="text" id="txtPayableVendor" class="text" />
                </div>

                <div class="field name">
                    <label for="<%= txtPayableCheckNumber.ClientID %>">Check Number:</label>
                    <input runat="server" type="text" id="txtPayableCheckNumber" class="text" />
                </div>

                <div class="field name">
                    <label for="<%= txtPayableInvoiceNumber1.ClientID %>">Invoice Number:</label>
                    <input runat="server" type="text" id="txtPayableInvoiceNumber1" class="text" />
                </div>
                <div class="field amount">
                    <label for="<%= txtPayableAmount1.ClientID %>">Amount:</label>
                    <input runat="server" type="text" id="txtPayableAmount1" class="text" />
                </div>
                <div class="field name">
                    <label for="<%= txtPayableInvoiceNumber2.ClientID %>">Invoice Number:</label>
                    <input runat="server" type="text" id="txtPayableInvoiceNumber2" class="text" />
                </div>
                <div class="field amount">
                    <label for="<%= txtPayableAmount2.ClientID %>">Amount:</label>
                    <input runat="server" type="text" id="txtPayableAmount2" class="text" />
                </div>

                <div class="submit_field">
                    <input runat="server" type="button" id="btnSendPayablesPayment" value="Send Payables Payment" onserverclick="btnSendPayablesPayment_Click" />
                </div>
                <div>
                    <% if (PaymentID > 0)
                        { %>
                        <strong>Payment ID: </strong><%= PaymentID.ToString()%><br>
                    <%}%>
                </div>
            </fieldset>
            <fieldset runat="server" id="SendPayablesPaymentByVendorNumberFieldset" visible="true" enableviewstate="false">
                <legend>Send Payables Payment By Vendor Number</legend>

                <div class="field name">
                    <label for="<%= txtMemo.ClientID %>">Memo:</label>
                    <input runat="server" type="text" id="txtMemo" class="text" />
                </div>

                <div class="field name">
                    <label for="<%= txtVendorName.ClientID %>">Vendor Name:</label>
                    <input runat="server" type="text" id="txtVendorName" class="text" />
                </div>

                <div class="field name">
                    <label for="<%= txtVendorNumber.ClientID %>">Vendor Number:</label>
                    <input runat="server" type="text" id="txtVendorNumber" class="text" />
                </div>

                <div class="field amount">
                    <label for="<%= txtVendorAmount.ClientID %>">Amount:</label>
                    <input runat="server" type="text" id="txtVendorAmount" class="text" />
                </div>

                <div class="field name">
                    <label for="<%= txtPayableAccount.ClientID %>">Bank Account:</label>
                    <input runat="server" type="text" id="txtPayableAccount" class="text" />
                </div>

                <div class="submit_field">
                    <input runat="server" type="button" id="btnSendPayablesPaymentByVendorNumber" value="Send Payables Payment By Vendor Number" onserverclick="btnSendPayablesPaymentByVendorNumber_Click" />
                </div>
                <div>
                    <% if (PaymentID > 0)
                        { %>
                        <strong>Payment ID: </strong><%= PaymentID.ToString()%><br>
                    <%}%>
                </div>
            </fieldset>
            <fieldset>
                <legend>Void Payment</legend>
                PaymentID: <input type="text" id="voidPaymentID" runat="server" />
                <div class="submit_field">
                    <input runat="server" type="button" value="Void Payment" onserverclick="btnVoidPayment_Click"  />
                </div>

                <span><%=PaymentVoided ? "Success" : "" %></span>
            </fieldset>
        </form>
    </body>
</html>
