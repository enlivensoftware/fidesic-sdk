﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using Fidesic;
using Fidesic.Data;

namespace FidesicSDK
{
    public partial class APIPaymentService : System.Web.UI.Page
    {
        #region Properties
        //------// Properties \\--------------------------------------------\\
        private LoginByAPIController _fidesicController = null;
        public LoginByAPIController FidesicController
        {
            get
            {
                if (_fidesicController == null)
                {
                    _fidesicController = new LoginByAPIController();
                }

                return _fidesicController;
            }
        }
        //------\\ Properties //--------------------------------------------//

        #region logins
        public class LoginInfo
        {
            public string name;
            public string apiKey;
            public string apiPW;

            public LoginInfo() { }
            public LoginInfo(string _name, string _apiKey, string _apiPW)
            {
                name = _name;
                apiKey = _apiKey;
                apiPW = _apiPW;
            }
        }
        private List<LoginInfo> _agencies;
        public List<LoginInfo> agencies
        {
            get
            {
                if (_agencies == null)
                {
                    _agencies = new List<LoginInfo>()
                    {
                        new LoginInfo("The World Online","jyJEjoxVZCBkFluU","9EH6gsojKuh43tNssk2U"),
                        new LoginInfo("The QB Online","zGUBPLqSzaLVgwCf","BEtNltqHrlVrHfhfbklx")
                    };
                }
                return _agencies;
            }
        }
        private LoginInfo _currentLogin;
        protected LoginInfo currentLogin
        {
            get
            {
                if (_currentLogin == null || _currentLogin.name != loginList.Value)
                {
                    _currentLogin = agencies.First(a => a.name == loginList.Value);
                }
                return _currentLogin;
            }
        }
        #endregion

        //------// Fields \\------------------------------------------------\\
        public FidesicUser CurrentUser;
        public List<Payment> GetAllReceivedPayments;
        public List<Payment> GetAllSentPayments;
        public Payment GetPayment;
        public int PaymentID;
        public bool PaymentVoided = false;
        //------\\ Fields //------------------------------------------------//
        #endregion

        //------// Methods \\-----------------------------------------------\\
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                foreach (var a in agencies)
                {
                    loginList.Items.Add(new ListItem { Text = a.name, Value = a.name });
                }
            }
            if (FidesicController.LoggedInToEnvironment)
            {
                EnableNonLoginForms();
            }
        }

        public void EnsureConnectedToFidesic()
        {
            try
            {
                if (!FidesicController.ConnectedToFidesic
                    || FidesicController.CurrentUser == null
                    || FidesicController.CurrentUser.CompanyName != loginList.Value)
                {
                    bool allowSwitch = (FidesicController.ConnectedToFidesic && FidesicController.CurrentUser != null && FidesicController.CurrentUser.CompanyName != loginList.Value);

                    FidesicController.ConnectToFidesic(currentLogin.apiKey, currentLogin.name, currentLogin.apiPW, allowSwitch);
                }
            }
            catch (NotConnectedToFidesicException) { FidesicController.ConnectToFidesic(currentLogin.apiKey, currentLogin.name, currentLogin.apiPW); }
            catch (FidesicSessionExpiredException) { FidesicController.ConnectToFidesic(currentLogin.apiKey, currentLogin.name, currentLogin.apiPW); }
        }

        public void DisplayError(System.Exception exception)
        {
            DisplayError(exception.ToString());
        }

        public void DisplayError(string errorMessage)
        {
            errorPlaceholder.Visible = true;
            errorLiteral.Text = errorMessage;
        }

        #region "Event Handlers"

        protected void btnLogin_Click(object sender, EventArgs args)
        {
            try
            {
                EnsureConnectedToFidesic();
                DisplayError("Successfully logged in: " + DateTime.Now.ToString());
                EnableNonLoginForms();
            }
            catch (System.Exception exception)
            {
                DisplayError(exception);
            }
        }
        protected void btnResetLogin_Click(object sender, EventArgs args)
        {
            try
            {
                FidesicController.DeleteFidesicConnectCookie();
                DisplayError("Successfully logged out: " + DateTime.Now.ToString());
                DisableNonLoginForms();
            }
            catch (System.Exception exception)
            {
                DisplayError(exception);
            }
        }

        private void EnableNonLoginForms()
        {
            updatePaymentStatusFieldset.Visible = true;
            GetReceivedPaymentsFieldSet.Visible = true;
            GetPaymentFieldSet.Visible = true;
            GetSentPayments.Visible = true;
        }
        private void DisableNonLoginForms()
        {
            updatePaymentStatusFieldset.Visible = false;
            GetReceivedPaymentsFieldSet.Visible = false;
            GetPaymentFieldSet.Visible = false;
        }

        protected void btnUpdatePaymentStatus_Click(object sender, EventArgs args)
        {
            try
            {
                Payment payment = new Payment();
                string rejectionDescription = null;
                string rejectionCode = null;
                int paymentID = 0;
                try
                { 
                    paymentID = int.Parse(txtPaymentID.Value.Trim());
                }
                catch{}
                string merchantReferenceNumber = txtMerchantReferenceNumber.Value.Trim();
                bool success = bool.Parse(successUpdate.SelectedValue);
                if (success)
                {
                    rejectionDescription = txtRejectionDescription.Value.Trim();
                    rejectionCode = txtRejectionCode.Value.Trim();
                }
                if (paymentID == 0)
                {
                    throw new Exception("Unable to parse payment id.");
                }
                EnsureConnectedToFidesic();
                FidesicController.UpdatePaymentStatus(paymentID, merchantReferenceNumber, success, rejectionDescription, rejectionCode );

                updatePaymentStatusFieldset.Visible = true;
            }
            catch (System.Exception exception)
            {
                DisplayError(exception);
            }
        }
        protected void btnGetAllReceivedPayments_Click(object sender, EventArgs args)
        {
            try
            {
                EnsureConnectedToFidesic();
                GetAllReceivedPayments = FidesicController.GetReceivedPayments(1,20,null,null,null);
            }
            catch (System.Exception exception)
            {
                DisplayError(exception);
            }
        }
        protected void btnGetAllSentPayments_Click(object sender, EventArgs args)
        {
            try
            {
                EnsureConnectedToFidesic();
                GetAllSentPayments = FidesicController.GetSentPayments(1, 20, null, null, null);
            }
            catch (System.Exception exception)
            {
                DisplayError(exception);
            }
        }
        protected void btnGetPayment_Click(object sender, EventArgs args)
        {
            try
            {
                EnsureConnectedToFidesic();
                int paymentID = 0;
                try
                {
                    paymentID = int.Parse(txtGetPaymentID.Value.Trim());
                }
                catch { }
                GetPayment = FidesicController.GetPayment(paymentID);
            }
            catch (System.Exception exception)
            {
                DisplayError(exception);
            }
        }
        protected void btnProcessPaymentWithUnsavedCreditCard_Click(object sender, EventArgs args)
        {
            try
            {
                EnsureConnectedToFidesic();
                //this invoice should actually be a filled in object based on pulling the invoice in from another call
                Invoice invoice = new Invoice();
                Payment payment = new Payment()
                {
                    Memo = "",
                    Amount = decimal.Parse(txtAmount.Value),
                    PayeeUserID = invoice.VendorID,
                    Remittance = new List<Remittance>(){
                        new Remittance(){
                            Amount = invoice.UnpaidAmount,
                            InvoiceID = invoice.InvoiceID,
                            InvoiceNumber = invoice.InvoiceNumber
                        }},
                    ProcessingDate = DateTime.Now,
                    PaymentOption = PaymentOption.Credit
                };
                CreditCardAccount paymentAccount = new CreditCardAccount()
                {
                    AccountHolderName = txtName.Value,
                    AccountName = txtName.Value,
                    AccountNumber = txtCardNumber.Value,
                    Address = new Address()
                    {
                        Address1 = txtAddress1.Value,
                        Address2 = txtAddress2.Value,
                        City = txtCity.Value,
                        State = txtState.Value,
                        Zip = txtZip.Value
                    },
                    CardNumber = txtCardNumber.Value,
                    ExpirationDate = DateTime.Parse(txtExpire.Value)
                };


                int PaymentID = FidesicController.ProcessPaymentWithUnsavedCreditCard(paymentAccount, payment);
            }
            catch (System.Exception exception)
            {
                DisplayError(exception);
            }
        }
        protected void btnProcessNonmemberPayment_Click(object sender, EventArgs args)
        {
            try
            {
                EnsureConnectedToFidesic();
                
                Payment payment = new Payment()
                {
                    Memo = "",
                    Amount = decimal.Parse(txtAmount.Value)
                };
                CreditCardAccount paymentAccount = new CreditCardAccount()
                {
                    AccountHolderName = txtName.Value,
                    AccountName = txtName.Value,
                    AccountNumber = txtCardNumber.Value,
                    Address = new Address()
                    {
                        Address1 = txtAddress1.Value,
                        Address2 = txtAddress2.Value,
                        City = txtCity.Value,
                        State = txtState.Value,
                        Zip = txtZip.Value
                    },
                    CardNumber = txtCardNumber.Value,
                    ExpirationDate = DateTime.Parse(txtExpire.Value)
                };
                Customer customer = new Customer()
                    {
                        Email = txtEmailAddress.Value,
                        CompanyName = txtName.Value
                    };


                int PaymentID = FidesicController.ProcessNonMemberPayment(customer, paymentAccount, payment);
            }
            catch (System.Exception exception)
            {
                DisplayError(exception);
            }
        }

        protected void btnProcessPayment_Click(object sender, EventArgs args)
        {
            try
            {
                EnsureConnectedToFidesic();


                var invoice = FidesicController.GetInvoice(int.Parse(txtInvoice.Value));
                Payment payment = new Payment()
                {
                    Memo = "",
                    Amount = decimal.Parse(txtAmount.Value),
                    PayeeUserID = invoice.VendorID,
                    PayerUserID = invoice.CustomerID,
                    ProcessingDate = DateTime.Now,
                    DueDate = null,
                    PaymentOption = PaymentOption.Credit
                };

                FidesicController.ProcessPayment(payment);
            }
            catch (System.Exception exception)
            {
                DisplayError(exception);
            }
        }

        protected void btnSendPayablesPayment_Click(object sender, EventArgs args)
        {
            try
            {
                EnsureConnectedToFidesic();
                var vendor = FidesicController.GetVendor(txtPayableVendor.Value);
                
                Payment payment = new Payment()
                {
                    Memo = "test",
                    CheckNumber = txtPayableCheckNumber.Value,
                    Amount = decimal.Parse(txtPayableAmount1.Value) + decimal.Parse(txtPayableAmount2.Value),
                    PayeeUserID = vendor.UserID,
                    ProcessingDate = DateTime.Now,
                    DueDate = null,
                    PaymentOption = PaymentOption.PaperCheck,
                    DepositOption = DepositOption.CashReceipt,

                    Remittance = new List<Remittance>()
                    {
                        new Remittance() {
                            Amount = decimal.Parse(txtPayableAmount1.Value),
                            InvoiceNumber = txtPayableInvoiceNumber1.Value,
                        },
                        new Remittance() {
                            Amount = decimal.Parse(txtPayableAmount2.Value),
                            InvoiceNumber = txtPayableInvoiceNumber2.Value,
                        },
                    }
                };
                PaymentID = (int)FidesicController.SendPayablesPayment(payment);
            }
            catch (System.Exception exception)
            {
                DisplayError(exception);
            }
        }
        protected void btnSendPayablesPaymentByVendorNumber_Click(object sender, EventArgs args)
        {
            try
            {
                EnsureConnectedToFidesic();

                Payment payment = new Payment()
                {
                    Memo = txtMemo.Value,
                    Amount = decimal.Parse(txtVendorAmount.Value),
                    //    Remittance = new List<Remittance>()
                    //{
                    //    new Remittance() {
                    //        Amount = decimal.Parse(txtAmount.Value),
                    //        InvoiceID = invoice.InvoiceID,
                    //        InvoiceNumber = invoice.InvoiceNumber,
                    //    },
                    //},
                    ProcessingDate = DateTime.Now,
                    DueDate = null,
                    PaymentOption = PaymentOption.PaperCheck,
                    DepositOption = DepositOption.CashReceipt
                };
                BankAccount account = new BankAccount()
                {
                    AccountName = txtPayableAccount.Value
                };
                Vendor vendor = new Vendor()
                {
                    CompanyName = txtVendorName.Value,
                    VendorNumber = txtVendorNumber.Value
                };
                PaymentID = (int)FidesicController.SendPayablesPaymentByVendorNumber(payment, vendor, account);
            }
            catch (System.Exception exception)
            {
                DisplayError(exception);
            }
        }

        protected void btnVoidPayment_Click(object sender, EventArgs args)
        {
            try
            {
                int voidId = int.Parse(voidPaymentID.Value);
                EnsureConnectedToFidesic();
                PaymentVoided = FidesicController.CancelPayment(voidId, "Voided via Fidesic SDK");
            }
            catch (System.Exception exception)
            {
                DisplayError(exception);
            }
        }
        #endregion
        //------\\ Methods //-----------------------------------------------//
    }
}