﻿
<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="InvoiceService.aspx.cs" Inherits="FidesicSDK.APIInvoiceService" %>
<%@ Import Namespace="Fidesic.Data" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
    <head runat="server">
        <title>Invoice Service Runner</title>
    </head>
    
    <body>
        <a href="../Index.html">Back</a><br>
        <form id="form1" runat="server">
            <div class="runner login">
                <asp:PlaceHolder runat="server" ID="errorPlaceholder" Visible="false" EnableViewState="false">
                    <div class="error_container">
                        <p class="error_message">
                            <asp:Literal runat="server" ID="errorLiteral" />
                        </p>
                    </div>
                </asp:PlaceHolder>
                
                
                <fieldset>
                    <legend>Agency</legend>
                     
                    <div class="field email">
                        <select id="loginList" runat="server" />
                    </div>
                    
                </fieldset>
            </div>


            <div class="runner btnGetAllSentInvoices">
                <fieldset>
                    <legend>Get Sent Invoices</legend>

                    <div class="submit_field">
                        Filter by Customer Name: <input type="text" runat="server" id="customerName" />
                        <input runat="server" type="button" id="btnGetAllSentInvoices" value="Get Sent Invoices" onserverclick="btnGetAllSentInvoices_Click" />
                    </div>

                    <% if (GetAllSentInvoices != null)
                       { %>
                        <% if (GetAllSentInvoices.Count == 0)
                           { %>
                            <p class="empty">You do not have any sent invoices.</p>
                        <% } else { %>
                            <% foreach (Invoice inv in GetAllSentInvoices)
                               { %>
                                <strong>Invoice ID: </strong><%= inv.InvoiceID.ToString() %><br>
                                <strong>Invoice Date: </strong><%= inv.InvoiceDate.ToString() %><br>
                                <strong>Status: </strong><%= inv.Status.ToString() %><br>
                                <strong>Memo: </strong><%= inv.Memo %><br>
                                <strong>InvoiceNumber: </strong><%= inv.InvoiceNumber %><br>
                                <strong>Due Date: </strong><%= inv.DueDate.ToString() %><br>
                                <strong>Amount: </strong><%= inv.Amount.ToString() %><br>
                                <strong>Customer Name: </strong><%= inv.Customer.CompanyName.ToString() %><br><br>
                            <% } %>
                        <% } %>
                    <% } %>
                </fieldset>

                <fieldset>
                    <legend>Get Received Invoices</legend>

                    <div class="submit_field">
                        Filter by Vendor Name: <input type="text" runat="server" id="vendorName" />
                        <input runat="server" type="button" id="btnGetAllReceivedInvoices" value="Get Received Invoices" onserverclick="btnGetAllReceivedInvoices_Click" />
                    </div>

                    <% if (GetAllReceivedInvoices != null)
                       { %>
                        <% if (GetAllReceivedInvoices.Count == 0)
                           { %>
                            <p class="empty">You do not have any received invoices.</p>
                        <% } else { %>
                            <% foreach (Invoice inv in GetAllReceivedInvoices)
                               { %>
                                <strong>Invoice ID: </strong><%= inv.InvoiceID.ToString() %><br>
                                <strong>Invoice Date: </strong><%= inv.InvoiceDate.ToString() %><br>
                                <strong>Status: </strong><%= inv.Status.ToString() %><br>
                                <strong>Memo: </strong><%= inv.Memo %><br>
                                <strong>InvoiceNumber: </strong><%= inv.InvoiceNumber %><br>
                                <strong>Due Date: </strong><%= inv.DueDate.ToString() %><br>
                                <strong>Amount: </strong><%= inv.Amount.ToString() %><br>
                                <strong>Vendor Name: </strong><%= inv.Vendor.CompanyName.ToString() %><br><br>
                            <% } %>
                        <% } %>
                    <% } %>
                </fieldset>
                <fieldset>
                    <legend>Add Invoice</legend>

                    Invoice Number: <input type="text" id="invoiceNumber" runat="server" /><br>
                    Amount: <input type="text" id="invoiceAmount" runat="server" /><br>
                    Invoice Date: <input type="text" id="invoiceDate" runat="server" /><br>
                    Due Date: <input type="text" id="dueDate" runat="server" /><br>
                    Customer Name: <input type="text" id="custName" runat="server" /><br>
                    Customer Number: <input type="text" id="custNum" runat="server" /><br>
                    First Name: <input type="text" id="custFirstName" runat="server" /><br>
                    Last Name: <input type="text" id="custLastName" runat="server" /><br>
                    Email: <input type="text" id="custEmail" runat="server" /><br>
                    Phone #: <input type="text" id="custPhone" runat="server" /><br>
                    Address:
                    <div>
                        <input type="text" placeholder="Address 1" id="address1" runat="server" /><br>
                        <input type="text" placeholder="Address 2" id="address2" runat="server" /><br>
                        <input type="text" placeholder="City" id="City" runat="server" />
                        <input type="text" placeholder="State" id="State" runat="server" />
                        <input type="text" placeholder="Zip" id="Zip" runat="server" />
                        <input type="text" placeholder="Country" id="Country" runat="server" />
                    </div>
                    Description: <input type="text" id="invoiceDesc" runat="server" /><br>
                    Memo (Optional): <input type="text" id="invoiceMemo" runat="server" /><br>
                    Term (Optional): <input type="text" id="invoiceTerm" runat="server" /><br>

                    <div class="submit_field">
                        <input runat="server" type="button" value="Add Invoice" onserverclick="btnAddInvoice_Click" />
                    </div>

                    <% if (InvoiceID > -1) { %>
                        <span><%=InvoiceID > 0 ? "Success" : "Fail" %></span>
                    <% } %>
                </fieldset>
                <fieldset>
                    <legend>Submit Expense</legend>
                    
                    Expense Number: <input type="text" id="expenseNumber" runat="server" /><br>
                    Amount: <input type="text" id="expenseAmount" runat="server" /><br>
                    Description: <input type="text" id="expenseDesc" runat="server" /><br>
                    Memo (Optional): <input type="text" id="expenseMemo" runat="server" /><br>
                    Term (Optional): <input type="text" id="expenseTerm" runat="server" /><br>
                    <div class="submit_field">
                        <input runat="server" type="button" value="Submit Expense" onserverclick="btnSubmitExpense_Click" />
                    </div>

                    <span><%=ExpenseAdded ? "Success" : "" %></span>
                </fieldset>
                <fieldset>
                    <legend>Void Invoice</legend>
                    InvoiceID: <input type="text" id="voidInvoiceID" runat="server" />
                    <div class="submit_field">
                        <input runat="server" type="button" value="Void Invoice" onserverclick="btnVoidInvoice_Click"  />
                    </div>

                    <span><%=InvoiceVoided ? "Success" : "" %></span>
                </fieldset>
                <fieldset>
                    <legend>Mark Invoice as Exported</legend>
                    InvoiceID: <input type="text" id="exportedInvoiceID" runat="server" />
                    <div class="submit_field">
                        <input runat="server" type="button" value="Mark Invoice as Exported" onserverclick="btnMarkInvoiceAsExported_Click"  />
                    </div>

                    <span><%=MarkedAsExported ? "Success" : "" %></span>
                </fieldset>
                <fieldset>
                    <legend>Get Item Templates</legend>

                    <div class="submit_field">
                        <input runat="server" type="button" value="Get Item Invoices" onserverclick="btnGetLineItemTemplates_Click" />
                    </div>
                    <% if (GetAllInvoiceItemTemplates != null)
                       { %>
                        <% if (GetAllInvoiceItemTemplates.Count == 0)
                           { %>
                            <p class="empty">You do not have any item templates.</p>
                        <% } else { %>
                            <% foreach (ItemTemplate t in GetAllInvoiceItemTemplates)
                               { %>
                                <strong>Item Template ID: </strong><%= t.ItemTemplateID %><br>
                                <strong>Name: </strong><%= t.Name %><br>
                                <strong>Description: </strong><%= t.Description %><br>
                                <strong>Unit Price: </strong><%= t.UnitPrice %><br>
                                <strong>Item Code: </strong><%= t.ItemCode %><br>
                                <strong>GLCode: </strong><%= t.GLCode %><br>
                                <br>
                            <% } %>
                        <% } %>
                    <% } %>
                </fieldset>
                <fieldset>
                    <legend>Add Item Template</legend>
                    <label>Name: </label><input type="text" runat="server" id="Name" /><br>
                    <label>Description: </label><input type="text" runat="server" id="Description" /><br>
                    <label>Unit Price: </label><input type="text" runat="server" id="UnitPrice" /><br>
                    <label>Item Code: </label><input type="text" runat="server" id="ItemCode" /><br>
                    <label>GLCode: </label><input type="text" runat="server" id="GLCode" /><br>
                    <div class="submit_field">
                        <input runat="server" type="button" value="Add Item Invoice" onserverclick="btnAddLineItemTemplate_Click" />
                    </div>

                    <% if (ItemTemplateID > -1) { %>
                        <span><%=ItemTemplateID > 0 ? "Success" : "Fail" %></span>
                    <% } %>
                </fieldset>
                <fieldset>
                    <legend>Delete Item Template</legend>
                    Item Template ID: <input type="text" id="deleteItemID" runat="server" />
                    <div class="submit_field">
                        <input runat="server" type="button" value="Delete Item Template" onserverclick="btnDeleteLineItemTemplate_Click"  />
                    </div>

                    <span><%=ItemDeleted ? "Success" : "" %></span>
                </fieldset>
            </div>

            <div class="runner btnGetAPSyncInvoices">
                <fieldset>
                    <legend>Get AP Sync Invoices</legend>

                    <div class="submit_field">
                        <input runat="server" type="button" value="Get AP Sync Invoices" onserverclick="btnGetAPSync_Click" />
                    </div>

                    <% if (GetAPSyncInvoices != null)
                       { %>
                        <% if (GetAPSyncInvoices.Count == 0)
                           { %>
                            <p class="empty">You do not have any sent invoices.</p>
                        <% } else { %>
                            <% foreach (Invoice inv in GetAPSyncInvoices)
                               { %>
                                <strong>Invoice ID: </strong><%= inv.InvoiceID.ToString() %><br>
                                <strong>Invoice Date: </strong><%= inv.InvoiceDate.ToString() %><br>
                                <strong>Status: </strong><%= inv.Status.ToString() %><br>
                                <strong>Memo: </strong><%= inv.Memo %><br>
                                <strong>InvoiceNumber: </strong><%= inv.InvoiceNumber %><br>
                                <strong>Due Date: </strong><%= inv.DueDate.ToString() %><br>
                                <strong>Amount: </strong><%= inv.Amount.ToString() %><br>
                                <strong>GLs:</strong><%foreach(InvoiceGLCode gl in inv.GLCodes) { %><br>
                                    &nbsp;<strong>Code: </strong><%=gl.FullGLCode %><br>
                                    &nbsp;<strong>Description: </strong><%=gl.FriendlyName %><br>
                                    &nbsp;<strong>Class: </strong><%=gl.Class %><br>
                                    &nbsp;<strong>Department: </strong><%=gl.Department %><br>
                                <%} %><br>
                            <% } %>
                        <% } %>
                    <% } %>
                </fieldset>

            </div>
            <fieldset>
                <legend>Get Vendor Locations</legend>

                <div class="submit_field">
                    <input id="Button1" runat="server" type="button" value="Get Vendor Locations" onserverclick="btnGetVendorLocations_Click" />
                </div>
                <% if (GetVendorLocations != null)
                    { %>
                    <% if (GetVendorLocations.Count == 0)
                        { %>
                        <p class="empty">You do not have any vendor locations.</p>
                    <% } else { %>
                        <% foreach (VendorLocation t in GetVendorLocations)
                            { %>
                            <strong>Location ID: </strong><%= t.LocationID %><br>
                            <strong>Location Code: </strong><%= t.LocationCode %><br>
                            <strong>Location Name: </strong><%= t.LocationName %><br>
                            <br>
                        <% } %>
                    <% } %>
                <% } %>
            </fieldset>
            <fieldset>
                <legend>Add Vendor Location</legend>
                Location Code: <input type="text" id="LocationCode" runat="server" /><br>
                Location Name: <input type="text" id="LocationName" runat="server" /><br>
                <div class="submit_field">
                    <input runat="server" type="button" value="Add Vendor Location" onserverclick="btnAddVendorLocation_Click"  />
                </div>

                <% if (LocationID > -1) { %>
                    <span><%=LocationID > 0 ? "Success" : "Fail" %></span>
                <% } %>
            </fieldset>

            <fieldset>
                <legend>Get Invoice Categories</legend>

                <div class="submit_field">
                    <input id="Button2" runat="server" type="button" value="Get Invoice Categories" onserverclick="btnGetInvoiceCategories_Click" />
                </div>
                <% if (GetInvoiceCategories != null)
                    { %>
                    <% if (GetInvoiceCategories.Count == 0)
                        { %>
                        <p class="empty">You do not have any invoice categories.</p>
                    <% } else { %>
                        <% foreach (InvoiceCategory t in GetInvoiceCategories)
                            { %>
                            <strong>Category ID: </strong><%= t.CategoryID %><br>
                            <strong>Category Code: </strong><%= t.InvoiceCode %><br>
                            <strong>Category Name: </strong><%= t.CategoryName %><br>
                            <br>
                        <% } %>
                    <% } %>
                <% } %>
            </fieldset>
            <fieldset>
                <legend>Get Invoice Terms</legend>

                <input id="Button3" runat="server" type="button" value="Get Terms" onserverclick="btnGetInvoiceTerms_Click"  />
                <% if (GetInvoiceTerms != null)
                    { %>
                    <% if (GetInvoiceTerms.Count == 0)
                        { %>
                        <p class="empty">You do not have any invoice terms.</p>
                    <% } else { %>
                        <br><strong>NOTE: For Due/Discount Type ID: 1 = Net, 2 = On Day, 3 = EOM</strong><br><br>
                        <% foreach (InvoiceTerm t in GetInvoiceTerms)
                            { %>
                            <strong>Term Name: </strong><%= t.Description %><br>
                            <strong>Term ID: </strong><%= t.TermOptionID %><br>
                            <strong>Due Type ID: </strong><%= t.DueTypeID %><br>
                            <strong>Term Due Date: </strong><%= t.DueDateDays %><br>
                            <strong>Discount Type ID: </strong><%= t.DiscountTypeID %><br>
                            <strong>Term Discount Date: </strong><%= t.DiscountDateDays %><br>
                            <strong> Discount Percentage: </strong><%= t.DiscountPercentage %><br>
                            <br>
                        <% } %>
                    <% } %>
                <% } %>
            </fieldset>
            <fieldset>
                <legend>Add Invoice Term</legend>
                <strong>NOTE: For Due/Discount Type ID: 1 = Net, 2 = On Day, 3 = EOM</strong><br><br>
                Term Name: <input type="text" id="termName" runat="server" /><br>
                Due Type ID: <input type="text" id="dueTypeID" runat="server" /><br>
                Term Due Date: <input type="text" id="termDueDate" runat="server" /><br>
                Discount Type ID (Optional): <input type="text" id="discountTypeID" runat="server" /><br>
                Term Discount Date (Optional): <input type="text" id="termDiscountDate" runat="server" /><br>
                Discount Percentage (Optional): <input type="text" id="discountPercentage" runat="server" /><br>
                <div class="submit_field">
                    <input runat="server" type="button" value="Add Invoice Term" onserverclick="btnAddInvoiceTerm_Click"  />
                </div>

                <% if (InvoiceTermID > -1) { %>
                    <span><%=InvoiceTermID > 0 ? "Success" : "Fail" %></span>
                <% } %>
            </fieldset>
            <fieldset>
                <legend>Delete Invoice Term</legend>
                Term Name: <input type="text" id="termToDelete" runat="server" />
                <div class="submit_field">
                    <input runat="server" type="button" value="Delete Invoice Term" onserverclick="btnDeleteInvoiceTerm_Click"  />
                </div>

                <span><%=TermDeleted ? "Success" : "" %></span>
            </fieldset>
        </form>
    </body>
</html>
