﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PaymentAccountService.aspx.cs" Inherits="FidesicSDK.APIPaymentAccountService" %>
<%@ Import Namespace="Fidesic.Data" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
    <head runat="server">
        <title>Payment Account Service Runner</title>
    </head>
    
    <body>
        <a href="../Index.html">Back</a><br>
        <form id="form1" runat="server">
            <div class="runner login">
                <asp:PlaceHolder runat="server" ID="errorPlaceholder" Visible="false" EnableViewState="false">
                    <div class="error_container">
                        <p class="error_message">
                            <asp:Literal runat="server" ID="errorLiteral" />
                        </p>
                    </div>
                </asp:PlaceHolder>
                
                
                <fieldset>
                    <legend>Agency</legend>
                     
                    <div class="field email">
                        <select id="loginList" runat="server" />
                    </div>
                    
                </fieldset>
            </div>


            <div class="runner bank_accounts">
                <fieldset>
                    <legend>Get All Bank Accounts</legend>

                    <div class="submit_field">
                        <input runat="server" type="button" id="btnGetAllBankAccounts" value="Get All Bank Accounts" onserverclick="btnGetAllBankAccounts_Click" />
                    </div>

                    <% if (GetAllBankAccounts_Accounts != null) { %>
                        <% if (GetAllBankAccounts_Accounts.Count == 0) { %>
                            <p class="empty">You do not have any Bank Accounts setup.</p>
                        <% } else { %>
                            <% foreach(BankAccount account in GetAllBankAccounts_Accounts) { %>
                                <strong>Payment Account ID: </strong><%= account.PaymentAccountID.ToString() %><br>
                                <strong>Account Name: </strong><%= account.AccountName %><br>
                                <strong>Account Number: </strong><%= account.AccountNumber %><br>
                                <strong>Bank Name: </strong><%= account.BankName %><br>
                                <strong>Routing Number: </strong><%= account.RoutingNumber %><br>
                                <strong>Type: </strong><%= account.Type.ToString() %><br>
                            <% } %>
                        <% } %>
                    <% } %>
                </fieldset>

                
                <fieldset>
                    <legend>Get Bank Account By Name</legend>

                    <div class="field account_name">
                        <label for="<%= txtBankAccountName.ClientID %>">Account Name:</label>
                        <input runat="server" type="text" id="txtBankAccountName" class="text" />
                    </div>

                    <div class="submit_field">
                        <input runat="server" type="button" id="btnGetBankAccountByName" value="Get Bank Account" onserverclick="btnGetBankAccountByName_Click" />
                    </div>

                    <% if (GetBankAccount_Account != null) { %>
                        <strong>Payment Account ID: </strong><%= GetBankAccount_Account.PaymentAccountID.ToString()%><br>
                        <strong>Account Name: </strong><%= GetBankAccount_Account.AccountName %><br>
                        <strong>Account Number: </strong><%= GetBankAccount_Account.AccountNumber %><br>
                        <strong>Bank Name: </strong><%= GetBankAccount_Account.BankName %><br>
                        <strong>Routing Number: </strong><%= GetBankAccount_Account.RoutingNumber%><br>
                        <strong>Type: </strong><%= GetBankAccount_Account.Type.ToString()%><br>
                    <% } %>
                </fieldset>


                <fieldset>
                    <legend>Add Bank Account</legend>

                    <div class="field account_name">
                        <label for="<%= txtBankAccountName2.ClientID %>">Account Name:</label>
                        <input runat="server" type="text" id="txtBankAccountName2" class="text" />
                    </div>

                    <div class="field account_number">
                        <label for="<%= txtBankAccountNumber.ClientID %>">Account Number:</label>
                        <input runat="server" type="text" id="txtBankAccountNumber" class="text" />
                    </div>

                    <div class="field routing_number">
                        <label for="<%= txtBankAccountRoutingNumber.ClientID %>">Routing Number:</label>
                        <input runat="server" type="text" id="txtBankAccountRoutingNumber" class="text" />
                    </div>

                    <div class="field account_type">
                        <label>Account Type:</label>
                        <asp:RadioButtonList runat="server" ID="accountType" CssClass="radio_list">
                            <asp:ListItem Value="Checking">Checking</asp:ListItem>
                            <asp:ListItem Value="Savings">Savings</asp:ListItem>
                        </asp:RadioButtonList>
                    </div>

                    <div class="submit_field">
                        <input runat="server" type="button" id="btnAddBankAccount" value="Add Bank Account" onserverclick="btnAddBankAccount_Click" />
                    </div>
                </fieldset>


                <fieldset runat="server" id="updateBankAccountFieldset" visible="false" enableviewstate="false">
                    <legend>Update Bank Account</legend>

                    <div class="field account_name">
                        <label for="<%= txtBankAccountName3.ClientID %>">Account Name:</label>
                        <input runat="server" type="text" id="txtBankAccountName3" class="text" />
                    </div>

                    <div class="field account_number">
                        <label for="<%= txtBankAccountNumber2.ClientID %>">Account Number:</label>
                        <input runat="server" type="text" id="txtBankAccountNumber2" class="text" />
                    </div>

                    <div class="field routing_number">
                        <label for="<%= txtBankAccountRoutingNumber2.ClientID %>">Routing Number:</label>
                        <input runat="server" type="text" id="txtBankAccountRoutingNumber2" class="text" />
                    </div>

                    <div class="field account_type">
                        <label>Account Type:</label>
                        <asp:RadioButtonList runat="server" ID="accountType2" CssClass="radio_list">
                            <asp:ListItem Value="Checking">Checking</asp:ListItem>
                            <asp:ListItem Value="Savings">Savings</asp:ListItem>
                        </asp:RadioButtonList>
                    </div>

                    <div class="submit_field">
                        <input runat="server" type="button" id="btnUpdateBankAccount" value="Update Bank Account" onserverclick="btnUpdateBankAccount_Click" />
                    </div>
                </fieldset>
            </div>


            <div class="runner credit_card_accounts">
                <fieldset>
                    <legend>Get All Credit Card Accounts</legend>

                    <div class="submit_field">
                        <input runat="server" type="button" id="btnGetAllCreditCardAccounts" value="Get All Credit Card Accounts" onserverclick="btnGetAllCreditCardAccounts_Click" />
                    </div>

                    <% if (GetAllCreditCardAccounts_Accounts != null) { %>
                        <% if (GetAllCreditCardAccounts_Accounts.Count == 0) { %>
                            <p class="empty">You do not have any Credit Card Accounts setup.</p>
                        <% } else { %>
                            <% foreach(CreditCardAccount account in GetAllCreditCardAccounts_Accounts) { %>
                                <strong>Payment Account ID: </strong><%= account.PaymentAccountID.ToString() %><br>
                                <strong>Account Name: </strong><%= account.AccountName %><br>
                                <strong>Account Number: </strong><%= account.AccountNumber %><br>
                            <% } %>
                        <% } %>
                    <% } %>
                </fieldset>

                
                <fieldset>
                    <legend>Get Credit Card Account By ID</legend>

                    <div class="field account_name">
                        <label for="<%= txtCreditCardAccountID.ClientID %>">Account ID:</label>
                        <input runat="server" type="text" id="txtCreditCardAccountID" class="text" />
                    </div>

                    <div class="submit_field">
                        <input runat="server" type="button" id="btnGetCreditCardAccountByID" value="Get Credit Card Account by ID" onserverclick="btnGetCreditCardAccountByID_Click" />
                    </div>

                    <% if (GetCreditCardAccount_Account != null) { %>
                        <strong>Payment Account ID: </strong><%= GetCreditCardAccount_Account.PaymentAccountID.ToString()%><br>
                        <strong>Account Name: </strong><%= GetCreditCardAccount_Account.AccountName%><br>
                        <strong>Account Number: </strong><%= GetCreditCardAccount_Account.AccountNumber%><br>
                    <% } %>
                </fieldset>


                <fieldset>
                    <legend>Add Credit Card Account</legend>

                    <div class="field account_name">
                        <label for="<%= txtCreditCardAccountName2.ClientID %>">Account Name:</label>
                        <input runat="server" type="text" id="txtCreditCardAccountName2" class="text" />
                    </div>

                    <div class="field account_number">
                        <label for="<%= txtCreditCardAccountNumber.ClientID %>">Account Number:</label>
                        <input runat="server" type="text" id="txtCreditCardAccountNumber" class="text" />
                    </div>

                    <div class="field card_name">
                        <label for="<%= txtCreditCardCardName.ClientID %>">Name On Card:</label>
                        <input runat="server" type="text" id="txtCreditCardCardName" class="text" />
                    </div>

                    <div class="field card_number">
                        <label for="<%= txtCreditCardCardName.ClientID %>">Card Number:</label>
                        <input runat="server" type="text" id="txtCreditCardCardNumber" class="text" />
                    </div>

                    <div class="field expiration">
                        <label for="<%= txtCreditCardExpirationMonth.ClientID %>">Expiration:</label>
                        <input runat="server" type="text" id="txtCreditCardExpirationMonth" class="text" />
                        <input runat="server" type="text" id="txtCreditCardExpirationYear" class="text" />
                    </div>

                    <div class="field address1">
                        <label for="<%= txtCreditCardAddress1.ClientID %>">Address:</label>
                        <input runat="server" type="text" id="txtCreditCardAddress1" class="text" />
                    </div>

                    <div class="field address2">
                        <label for="<%= txtCreditCardAddress2.ClientID %>">Address 2:</label>
                        <input runat="server" type="text" id="txtCreditCardAddress2" class="text" />
                    </div>

                    <div class="field city">
                        <label for="<%= txtCreditCardCity.ClientID %>">City:</label>
                        <input runat="server" type="text" id="txtCreditCardCity" class="text" />
                    </div>

                    <div class="field state">
                        <label for="<%= txtCreditCardState.ClientID %>">State:</label>
                        <input runat="server" type="text" id="txtCreditCardState" class="text" />
                    </div>

                    <div class="field zipcode">
                        <label for="<%= txtCreditCardZip.ClientID %>">Zipcode:</label>
                        <input runat="server" type="text" id="txtCreditCardZip" class="text" />
                    </div>

                    <div class="submit_field">
                        <input runat="server" type="button" id="btnAddCreditCardAccount" value="Add Credit Card Account" onserverclick="btnAddCreditCardAccount_Click" />
                    </div>
                </fieldset>


                <fieldset runat="server" id="updateCreditCardAccountFieldset" visible="false" enableviewstate="false">
                    <legend>Update Credit Card Account</legend>

                    <div class="field account_name">
                        <label for="<%= txtCreditCardAccountName3.ClientID %>">Account Name:</label>
                        <input runat="server" type="text" id="txtCreditCardAccountName3" class="text" />
                    </div>

                    <div class="field account_number">
                        <label for="<%= txtCreditCardAccountNumber2.ClientID %>">Account Number:</label>
                        <input runat="server" type="text" id="txtCreditCardAccountNumber2" class="text" />
                    </div>

                    <div class="field card_name">
                        <label for="<%= txtCreditCardCardName2.ClientID %>">Card Name:</label>
                        <input runat="server" type="text" id="txtCreditCardCardName2" class="text" />
                    </div>

                    <div class="field card_number">
                        <label for="<%= txtCreditCardCardNumber2.ClientID %>">Card Number:</label>
                        <input runat="server" type="text" id="txtCreditCardCardNumber2" class="text" />
                    </div>

                    <div class="field expiration">
                        <label for="<%= txtCreditCardExpirationMonth2.ClientID %>">Expiration:</label>
                        <input runat="server" type="text" id="txtCreditCardExpirationMonth2" class="text" />
                        <input runat="server" type="text" id="txtCreditCardExpirationYear2" class="text" />
                    </div>

                    <div class="field address1">
                        <label for="<%= txtCreditCardAddress1_2.ClientID %>">Address:</label>
                        <input runat="server" type="text" id="txtCreditCardAddress1_2" class="text" />
                    </div>

                    <div class="field address2">
                        <label for="<%= txtCreditCardAddress2_2.ClientID %>">Address 2:</label>
                        <input runat="server" type="text" id="txtCreditCardAddress2_2" class="text" />
                    </div>

                    <div class="field city">
                        <label for="<%= txtCreditCardCity2.ClientID %>">City:</label>
                        <input runat="server" type="text" id="txtCreditCardCity2" class="text" />
                    </div>

                    <div class="field state">
                        <label for="<%= txtCreditCardState2.ClientID %>">State:</label>
                        <input runat="server" type="text" id="txtCreditCardState2" class="text" />
                    </div>

                    <div class="field zipcode">
                        <label for="<%= txtCreditCardZip2.ClientID %>">Zipcode:</label>
                        <input runat="server" type="text" id="txtCreditCardZip2" class="text" />
                    </div>

                    <div class="submit_field">
                        <input runat="server" type="button" id="btnUpdateCreditCardAccount" value="Update Credit Card Account" onserverclick="btnUpdateCreditCardAccount_Click" />
                    </div>
                </fieldset>

                <fieldset>
                    <legend>Update Default Payment Account ID</legend>

                    <div class="field account_name">
                        <label for="<%= txtPaymentAccountID.ClientID %>">Account ID:</label>
                        <input runat="server" type="text" id="txtPaymentAccountID" class="text" />
                    </div>

                    <div class="submit_field">
                        <input runat="server" type="button" id="Button1" value="Update Default Payment Account ID" onserverclick="btnSetDefaultPaymentAccount_Click" />
                    </div>

                    <% if (GetCreditCardAccount_Account != null) { %>
                        <strong>Payment Account ID: </strong><%= GetCreditCardAccount_Account.PaymentAccountID.ToString()%><br>
                        <strong>Account Name: </strong><%= GetCreditCardAccount_Account.AccountName%><br>
                        <strong>Account Number: </strong><%= GetCreditCardAccount_Account.AccountNumber%><br>
                    <% } %>
                </fieldset>
            </div>
        </form>
    </body>
</html>
