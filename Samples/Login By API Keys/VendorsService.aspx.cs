﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using Fidesic;
using Fidesic.Data;
using Fidesic.Web;

namespace FidesicSDK
{
    public partial class APIVendorsService : System.Web.UI.Page
    {
        #region Properties
        //------// Properties \\--------------------------------------------\\
        private LoginByAPIController _fidesicController = null;
        public LoginByAPIController FidesicController
        {
            get
            {
                if (_fidesicController == null)
                {
                    _fidesicController = new LoginByAPIController();
                }

                return _fidesicController;
            }
        }
        //------\\ Properties //--------------------------------------------//

        #region logins
        public class LoginInfo
        {
            public string name;
            public string apiKey;
            public string apiPW;

            public LoginInfo() { }
            public LoginInfo(string _name, string _apiKey, string _apiPW)
            {
                name = _name;
                apiKey = _apiKey;
                apiPW = _apiPW;
            }
        }
        private List<LoginInfo> _agencies;
        public List<LoginInfo> agencies
        {
            get
            {
                if (_agencies == null)
                {
                    _agencies = new List<LoginInfo>()
                    {
                        new LoginInfo("The World Online","jyJEjoxVZCBkFluU","9EH6gsojKuh43tNssk2U"),
                        new LoginInfo("The QB Online","zGUBPLqSzaLVgwCf","BEtNltqHrlVrHfhfbklx")
                    };
                }
                return _agencies;
            }
        }
        private LoginInfo _currentLogin;
        protected LoginInfo currentLogin
        {
            get
            {
                if (_currentLogin == null || _currentLogin.name != loginList.Value)
                {
                    _currentLogin = agencies.First(a => a.name == loginList.Value);
                }
                return _currentLogin;
            }
        }
        #endregion

        //------// Fields \\------------------------------------------------\\
        public FidesicUser CurrentUser;
        public List<Vendor> GetAllVendors;
        public Vendor vendor;
        public int newVendorID;
        //------\\ Fields //------------------------------------------------//



        //------// Methods \\-----------------------------------------------\\
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                foreach (var a in agencies)
                {
                    loginList.Items.Add(new ListItem { Text = a.name, Value = a.name });
                }
            }
        }

        public void EnsureConnectedToFidesic()
        {
            try
            {
                if (!FidesicController.ConnectedToFidesic
                    || FidesicController.CurrentUser == null
                    || FidesicController.CurrentUser.CompanyName != loginList.Value)
                {
                    bool allowSwitch = (FidesicController.ConnectedToFidesic && FidesicController.CurrentUser != null && FidesicController.CurrentUser.CompanyName != loginList.Value);

                    FidesicController.ConnectToFidesic(currentLogin.apiKey, currentLogin.name, currentLogin.apiPW, allowSwitch);
                }
            }
            catch (NotConnectedToFidesicException) { FidesicController.ConnectToFidesic(currentLogin.apiKey, currentLogin.name, currentLogin.apiPW); }
            catch (FidesicSessionExpiredException) { FidesicController.ConnectToFidesic(currentLogin.apiKey, currentLogin.name, currentLogin.apiPW); }
        }

        public void DisplayError(System.Exception exception)
        {
            DisplayError(exception.ToString());
        }
        public void DisplayError(string errorMessage)
        {
            errorPlaceholder.Visible = true;
            errorLiteral.Text = errorMessage;
        }


        #region "Event Handlers"

        protected void btnLogin_Click(object sender, EventArgs args)
        {
            try
            {
                EnsureConnectedToFidesic();
                DisplayError("Successfully logged in: " + DateTime.Now.ToString());
                //DisplayError("Logged in as: " + CurrentUser.Email);
            }
            catch (System.Exception exception)
            {
                DisplayError(exception);
            }
        }


        protected void btnGetAllVendors_Click(object sender, EventArgs args)
        {
            try
            {
                EnsureConnectedToFidesic();
                GetAllVendors = FidesicController.GetAllVendors();
                
            }
            catch (System.Exception exception)
            {
                DisplayError(exception);
            }
        }

        protected void btnAddVendor_Click(object sender, EventArgs args)
        {
            try
            {
                int id = 0;
                int.TryParse(vendorID.Value, out id);

                Vendor newVendor = new Vendor()
                {
                    CompanyName = company.Value,
                    VendorNumber = vendorNumber.Value,
                    Email = emailAddress.Value,
                    AddressInfo = new Address()
                    {
                        Address1 = address1.Value,
                        Address2 = address2.Value,
                        City = City.Value,
                        State = State.Value,
                        Zip = Zip.Value
                    },
                    FirstName = firstName.Value,
                    LastName = lastName.Value,
                    PhoneNumber = phoneNumber.Value,
                    UserID=id
                };

                EnsureConnectedToFidesic();
                if(id > 0)
                    FidesicController.UpdateVendor(newVendor);
                else
                    newVendorID = FidesicController.AddVendor(newVendor);

                if (newVendorID != 0)
                    vendorID.Value = newVendorID.ToString();
            }
            catch (System.Exception exception)
            {
                DisplayError(exception);
            }
        }
        

        protected void btnGetVendor_Click(object sender, EventArgs args)
        {
            try
            {
                int id = 0;
                int.TryParse(vendorID.Value, out id);
                EnsureConnectedToFidesic();
                vendor = FidesicController.GetVendor(id);

                if (vendor != null)
                {
                    vendorID.Value = vendor.UserID.ToString();
                    company.Value = vendor.CompanyName;
                    vendorNumber.Value = vendor.VendorNumber;
                    emailAddress.Value = vendor.Email;
                    if (vendor.AddressInfo != null)
                    {
                        address1.Value = vendor.AddressInfo.Address1;
                        address2.Value = vendor.AddressInfo.Address2;
                        City.Value = vendor.AddressInfo.City;
                        State.Value = vendor.AddressInfo.State;
                        Zip.Value = vendor.AddressInfo.Zip;
                    }
                    firstName.Value = vendor.FirstName;
                    lastName.Value = vendor.LastName;
                    phoneNumber.Value = vendor.PhoneNumber;
                }
            }
            catch (System.Exception exception)
            {
                DisplayError(exception);
            }
        }
        protected void btnGetVendorNumber_Click(object sender, EventArgs args)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(VendorNumberSearch.Value)) throw new Exception("Please enter a Vendor Number to search for.");
                EnsureConnectedToFidesic();
                vendor = FidesicController.GetVendor(VendorNumberSearch.Value);

                if (vendor != null)
                {
                    vendorID.Value = vendor.UserID.ToString();
                    company.Value = vendor.CompanyName;
                    vendorNumber.Value = vendor.VendorNumber;
                    emailAddress.Value = vendor.Email;
                    if (vendor.AddressInfo != null)
                    {
                        address1.Value = vendor.AddressInfo.Address1;
                        address2.Value = vendor.AddressInfo.Address2;
                        City.Value = vendor.AddressInfo.City;
                        State.Value = vendor.AddressInfo.State;
                        Zip.Value = vendor.AddressInfo.Zip;
                    }
                    firstName.Value = vendor.FirstName;
                    lastName.Value = vendor.LastName;
                    phoneNumber.Value = vendor.PhoneNumber;
                }
            }
            catch (System.Exception exception)
            {
                DisplayError(exception);
            }
        }

        protected void btnDeleteVendor_Click(object sender, EventArgs args)
        {
            try
            {
                int id = 0;
                int.TryParse(vendorID.Value, out id);
                EnsureConnectedToFidesic();
                FidesicController.DeleteVendor(id);

            }
            catch (System.Exception exception)
            {
                DisplayError(exception);
            }
        }
        protected void btnDeleteVendorNumber_Click(object sender, EventArgs args)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(VendorNumberSearch.Value)) throw new Exception("Please enter a Vendor Number to search for.");
                EnsureConnectedToFidesic();
                FidesicController.DeleteVendor(VendorNumberSearch.Value);

            }
            catch (System.Exception exception)
            {
                DisplayError(exception);
            }
        }


        #endregion
        //------\\ Methods //-----------------------------------------------//
    }
}