﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ExamplePayInvoice.aspx.cs" Inherits="FidesicSDK.APIExamplePayInvoice" %>
<%@ Import Namespace="Fidesic.Data" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
    <head id="Head1" runat="server">
        <title>Invoice Service Runner</title>
    </head>
    
    <body>
        <a href="../Index.html">Back</a><br>
        <form id="form1" runat="server">
            <div class="runner login">
                <asp:PlaceHolder runat="server" ID="errorPlaceholder" Visible="false" EnableViewState="false">
                    <div class="error_container">
                        <p class="error_message">
                            <asp:Literal runat="server" ID="errorLiteral" />
                        </p>
                    </div>
                </asp:PlaceHolder>
                
                
                <fieldset>
                    <legend>Agency</legend>
                     
                    <div class="field email">
                        <select id="loginList" runat="server" />
                    </div>
                    
                </fieldset>
            </div>


            <div class="runner btnGetAllSentInvoices">
                <fieldset>
                    <legend>Get Invoice by ID (Provided by Customer. Make sure you have selected agency above)</legend>

                    <div class="submit_field">
                        <input type="text" id="invoiceID" runat="server" />
                        <input runat="server" type="button" id="btnGetInvoice" value="Get Invoice by ID" onserverclick="btnGetInvoice_Click" />
                    </div>
                    <div class="submit_field">
                        <input type="text" id="invoiceNumber" runat="server" />
                        <input runat="server" type="button" id="btnGetInvoiceNumber" value="Get Invoice by Invoice Number" onserverclick="btnGetInvoiceByNumber_Click" />
                    </div>

                    <% if (invoice != null)
                       { %>
                       <span>
                        <h3>Teller should verify the customer and amount:</h3>
                        <strong>Invoice ID: </strong><%= invoice.InvoiceID.ToString() %><br>
                        <strong>Customer: </strong><%= invoice.Customer.CompanyName.ToString() %><br>        
                        <strong>Amount: </strong><%= invoice.Amount.ToString() %><br>
                        <strong>Unpaid Amount: </strong><%= invoice.UnpaidAmount.ToString() %><br>
                        <strong>InvoiceNumber: </strong><%= invoice.InvoiceNumber %><br>
                        <strong>Invoice Date: </strong><%= invoice.InvoiceDate.ToString() %><br>
                        <strong>Status: </strong><%= invoice.Status.ToString() %><br>
                        <strong>Memo: </strong><%= invoice.Memo %><br>
                        <strong>Due Date: </strong><%= invoice.DueDate.ToString() %><br>
                        <strong>Category: </strong><%= invoice.Category != null ? invoice.Category.CategoryName : "" %><br>
                        <strong>Can Approve: </strong><%= invoice.canApprove.ToString() %><br>
                        </span>
                        <span>
                            <% if (invoice.Fees != null && invoice.Fees.Count > 0)
                               { %>
                            <%foreach (InvoiceFee fee in invoice.Fees)
                              { %><p>
                              BranchSortCode: <%=fee.BranchSortCode %> | 
                              BankCode: <%=fee.BankCode %> | AccountNumber: <%=fee.AccountNumber %> | 
                              AccountName: <%=fee.AccountName %> | FeeAmount: <%=fee.InvoiceFeeAmount %> | 
                              PartnerCode: <%=fee.PartnerCode %> | PartnerName: <%=fee.PartnerName %>
                              </p>
                            <%}
                               }%>
                        </span>
                        <span>
                            <% if (invoice.Attachments != null && invoice.Attachments.Count > 0)
                               { %>
                            <%foreach (var att in invoice.Attachments)
                              { %><p>
                              <a href="<%=att.URL %>"><%=att.FileName %></a>
                              </p>
                            <%}
                               }%>
                        </span>
                        <br>
                        <%if (string.IsNullOrEmpty(paymentSuccessMessage))
                          { %>
                        <%if (invoice.UnpaidAmount > 0)
                          { %>
                          <input type="hidden" id="invoiceIDtoPay" runat="server"/>
                        Value Date: <input type="text" runat="server" id="valueDate" />
                        Deposit Option: 
                        <select id="deposit" runat="server"> 
                            <option value="Teller">Teller</option>
                            <option value="CashReceipt">CashReceipt</option>
                            <option value="PointOfSale">PointOfSale</option>
                        </select><br>
                        
                        <input runat="server" type="button" id="btnPayInvoice" value="Pay Invoice" onserverclick="btnPayInvoice_Click" />
                        <%}
                          else
                          {%>
                        Invoice is already fully paid.
                        <%}
                          }
                          else{%>
                        <%=paymentSuccessMessage %>
                        <%} %>
                    <% } %>
                </fieldset>

                <fieldset>
                    <legend>For demo purposes only, to get an invoice id (customer will have this and bring to teller)- Get All Sent Invoices</legend>

                    <div class="submit_field">
                        <input runat="server" type="button" id="btnGetAllSentInvoices" value="Get All Sent Invoices" onserverclick="btnGetAllSentInvoices_Click" />
                    </div>

                    <% if (GetAllSentInvoices != null)
                       { %>
                        <% if (GetAllSentInvoices.Count == 0)
                           { %>
                            <p class="empty">You do not have any sent invoices.</p>
                        <% } else { %>
                            <% foreach (Invoice inv in GetAllSentInvoices)
                               { %>
                                <strong>Invoice ID: </strong><%= inv.InvoiceID.ToString() %><br>
                                <strong>Invoice Date: </strong><%= inv.InvoiceDate.ToString() %><br>
                                <strong>Status: </strong><%= inv.Status.ToString() %><br>
                                <strong>Memo: </strong><%= inv.Memo %><br>
                                <strong>InvoiceNumber: </strong><%= inv.InvoiceNumber %><br>
                                <strong>Due Date: </strong><%= inv.DueDate.ToString() %><br>
                                <strong>Amount: </strong><%= inv.Amount.ToString() %><br>
                                <strong>Unpaid Amount: </strong><%= inv.UnpaidAmount.ToString() %><br>
                                <hr />
                            <% } %>
                        <% } %>
                    <% } %>
                </fieldset>
            </div>


        </form>
    </body>
</html>
