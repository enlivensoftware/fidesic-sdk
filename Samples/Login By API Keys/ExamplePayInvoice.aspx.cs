﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using Fidesic;
using Fidesic.Data;
using Fidesic.Web;

namespace FidesicSDK
{
    public partial class APIExamplePayInvoice : System.Web.UI.Page
    {
        #region Properties
        //------// Properties \\--------------------------------------------\\
        private LoginByAPIController _fidesicController = null;
        public LoginByAPIController FidesicController
        {
            get
            {
                if (_fidesicController == null)
                {
                    _fidesicController = new LoginByAPIController();
                }

                return _fidesicController;
            }
        }
        //------\\ Properties //--------------------------------------------//

        #region logins
        public class LoginInfo
        {
            public string name;
            public string apiKey;
            public string apiPW;

            public LoginInfo() { }
            public LoginInfo(string _name, string _apiKey, string _apiPW)
            {
                name = _name;
                apiKey = _apiKey;
                apiPW = _apiPW;
            }
        }
        private List<LoginInfo> _agencies;
        public List<LoginInfo> agencies
        {
            get
            {
                if (_agencies == null)
                {
                    _agencies = new List<LoginInfo>()
                    {
                        new LoginInfo("The World Online","jyJEjoxVZCBkFluU","9EH6gsojKuh43tNssk2U"),
                        new LoginInfo("The QB Online","zGUBPLqSzaLVgwCf","BEtNltqHrlVrHfhfbklx")
                    };
                }
                return _agencies;
            }
        }
        private LoginInfo _currentLogin;
        protected LoginInfo currentLogin
        {
            get
            {
                if (_currentLogin == null || _currentLogin.name != loginList.Value)
                {
                    _currentLogin = agencies.First(a => a.name == loginList.Value);
                }
                return _currentLogin;
            }
        }
        #endregion


        //------// Fields \\------------------------------------------------\\
        public FidesicUser CurrentUser;
        public List<Invoice> GetAllSentInvoices;
        public Invoice invoice;
        public List<Invoice> GetAllReceivedInvoices;
        public string paymentSuccessMessage = null;
        //------\\ Fields //------------------------------------------------//



        //------// Methods \\-----------------------------------------------\\
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                foreach (var a in agencies)
                {
                    loginList.Items.Add(new ListItem { Text = a.name, Value = a.name });
                }
            }
        }

        public void EnsureConnectedToFidesic()
        {
            try
            {
                if (!FidesicController.ConnectedToFidesic
                    || FidesicController.CurrentUser == null
                    || FidesicController.CurrentUser.CompanyName != loginList.Value)
                {
                    bool allowSwitch = (FidesicController.ConnectedToFidesic && FidesicController.CurrentUser != null && FidesicController.CurrentUser.CompanyName != loginList.Value);

                    FidesicController.ConnectToFidesic(currentLogin.apiKey, currentLogin.name, currentLogin.apiPW, allowSwitch);
                }
            }
            catch (NotConnectedToFidesicException) { FidesicController.ConnectToFidesic(currentLogin.apiKey, currentLogin.name, currentLogin.apiPW); }
            catch (FidesicSessionExpiredException) { FidesicController.ConnectToFidesic(currentLogin.apiKey, currentLogin.name, currentLogin.apiPW); }
        }

        public void DisplayError(DevDefined.OAuth.Framework.OAuthException exception)
        {
            DisplayError(exception.ToString());
        }
        public void DisplayError(System.Exception exception)
        {
            DisplayError(exception.ToString());
        }
        public void DisplayError(string errorMessage)
        {
            errorPlaceholder.Visible = true;
            errorLiteral.Text = errorMessage;
        }


        #region "Event Handlers"

        protected void btnLogin_Click(object sender, EventArgs args)
        {
            try
            {
                EnsureConnectedToFidesic();
                DisplayError("Successfully logged in: " + DateTime.Now.ToString());
                //DisplayError("Logged in as: " + CurrentUser.Email);
            }
            catch (System.Exception exception)
            {
                DisplayError(exception);
            }
        }


        protected void btnGetAllSentInvoices_Click(object sender, EventArgs args)
        {
            try
            {
                EnsureConnectedToFidesic();
                GetAllSentInvoices = FidesicController.GetAllSentInvoices();
            }
            catch (System.Exception exception)
            {
                DisplayError(exception);
            }
        }
        protected void btnGetInvoice_Click(object sender, EventArgs args)
        {
            try
            {
                int id = 0;
                if (int.TryParse(invoiceID.Value, out id))
                {
                    EnsureConnectedToFidesic();
                    invoice = FidesicController.GetInvoice(id);
                    valueDate.Value = DateTime.Now.AddDays(3).ToShortDateString();
                    invoiceIDtoPay.Value = invoice.InvoiceID.ToString();
                }
                else
                {
                    throw new Exception("Invalid invoice id");
                }
            }
            catch (System.Exception exception)
            {
                DisplayError(exception);
            }
        }
        protected void btnGetInvoiceByNumber_Click(object sender, EventArgs args)
        {
            try
            {
                string number = invoiceNumber.Value.Trim();
                if (number.Length > 0)
                {
                    EnsureConnectedToFidesic();
                    invoice = FidesicController.GetSentInvoiceByInvoiceNumber(number);
                    valueDate.Value = DateTime.Now.AddDays(3).ToShortDateString();
                    invoiceIDtoPay.Value = invoice.InvoiceID.ToString();
                    if (invoice.Customer != null)
                    {

                    }
                }
                else
                {
                    throw new Exception("Invalid invoice number");
                }
            }
            catch (DevDefined.OAuth.Framework.OAuthException exception)
            {
                DisplayError(exception.Message);
            }
            catch (System.Exception exception)
            {
                DisplayError(exception);
            }
        }
        protected void btnPayInvoice_Click(object sender, EventArgs args)
        {
            try
            {
                int id = 0;
                DateTime dueDate = DateTime.Now.AddDays(3);
                DateTime.TryParse(valueDate.Value, out dueDate);

                if (int.TryParse(invoiceIDtoPay.Value, out id))
                {
                    EnsureConnectedToFidesic();
                    invoice = FidesicController.GetInvoice(id);
                    Payment p = new Payment()
                    {
                        Amount = invoice.UnpaidAmount,
                        PayerUserID = invoice.CustomerID,
                        Remittance = new List<Remittance>(){
                            new Remittance(){
                                Amount = invoice.UnpaidAmount,
                                InvoiceID = invoice.InvoiceID,
                                InvoiceNumber = invoice.InvoiceNumber
                            }},
                        ProcessingDate = DateTime.Now,
                        DueDate = dueDate,
                        PaymentOption = PaymentOption.PaperCheck,
                        DepositOption = (DepositOption)Enum.Parse(typeof(DepositOption), deposit.Value.ToString())
                    };
                    int? paymentID = FidesicController.AddPayment(p);
                    paymentSuccessMessage = paymentID == null || paymentID == 0 ? "Payment failed to insert." : "Payment inserted successfully: Fidesic ID is " + paymentID.ToString();
                    invoice = FidesicController.GetInvoice(id);
                }
                else
                {
                    throw new Exception("Invalid invoice id");
                }
            }
            catch (System.Exception exception)
            {
                DisplayError(exception);
            }
        }

        protected void btnPayInvoiceByCredit_Click(object sender, EventArgs args)
        {
            try
            {
                int id = 0;
                DateTime dueDate = DateTime.Now.AddDays(3);
                DateTime.TryParse(valueDate.Value, out dueDate);

                if (int.TryParse(invoiceIDtoPay.Value, out id))
                {
                    EnsureConnectedToFidesic();
                    invoice = FidesicController.GetInvoice(id);
                    Invoice i = new Invoice()
                    {
                        Amount = invoice.UnpaidAmount,
                        Customer = new Customer()
                        {
                            Email = FidesicController.CurrentUser.Email,
                            CompanyName = "Example Customer Name",
                            CustomerNumber = "UniqueCustomerNumber",
                            AddressInfo = new Address()
                            {
                                Address1 = "123 Street",
                                City = "CityName",
                                State = "StateName",
                                Zip = "PostalCode",
                                Country = "USA"
                            }
                        },
                        // Remittance shows what invoice is being paid and how much of the credit is
                        // being applied to the invoice. Credit can be applied to multiple invoices,
                        // but can not be applied for more than total credit amount.
                        Remittance = new List<Remittance>(){
                            new Remittance(){
                                Amount = invoice.UnpaidAmount,
                                InvoiceID = invoice.InvoiceID,
                                InvoiceNumber = invoice.InvoiceNumber
                            }},
                        InvoiceDate = DateTime.Now,
                        DueDate = dueDate,
                        InvoiceNumber = "CREDIT52009340",
                        Memo = "Credit for this reason!"
                    };
                    int? creditID = FidesicController.AddCreditMemo(i);
                    paymentSuccessMessage = creditID == null || creditID == 0 ? "Credit failed to insert." : "Credit inserted successfully: Fidesic ID is " + creditID.ToString();
                    invoice = FidesicController.GetInvoice(id);
                }
                else
                {
                    throw new Exception("Invalid invoice id");
                }
            }
            catch (System.Exception exception)
            {
                DisplayError(exception);
            }
        }

        protected void btnGetAllReceivedInvoices_Click(object sender, EventArgs args)
        {
            try
            {
                EnsureConnectedToFidesic();
                GetAllReceivedInvoices = FidesicController.GetAllReceivedInvoices();
            }
            catch (System.Exception exception)
            {
                DisplayError(exception);
            }
        }

        #endregion
        //------\\ Methods //-----------------------------------------------//
    }
}