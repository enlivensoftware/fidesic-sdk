﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using Fidesic;
using Fidesic.Data;
using Fidesic.Web;

namespace FidesicSDK
{
    public partial class APIApprovalService : System.Web.UI.Page
    {
        #region Properties
        //------// Properties \\--------------------------------------------\\
        private LoginByAPIController _fidesicController = null;
        public LoginByAPIController FidesicController
        {
            get
            {
                if (_fidesicController == null)
                {
                    _fidesicController = new LoginByAPIController();
                }

                return _fidesicController;
            }
        }
        //------\\ Properties //--------------------------------------------//


        #region logins
        public class LoginInfo
        {
            public string name;
            public string apiKey;
            public string apiPW;

            public LoginInfo() { }
            public LoginInfo(string _name, string _apiKey, string _apiPW)
            {
                name = _name;
                apiKey = _apiKey;
                apiPW = _apiPW;
            }
        }
        private List<LoginInfo> _agencies;
        public List<LoginInfo> agencies
        {
            get
            {
                if (_agencies == null)
                {
                    _agencies = new List<LoginInfo>()
                    {
                        new LoginInfo("The World Online","jyJEjoxVZCBkFluU","9EH6gsojKuh43tNssk2U"),
                        new LoginInfo("The QB Online","zGUBPLqSzaLVgwCf","BEtNltqHrlVrHfhfbklx")
                    };
                }
                return _agencies;
            }
        }
        private LoginInfo _currentLogin;
        protected LoginInfo currentLogin
        {
            get
            {
                if (_currentLogin == null || _currentLogin.name != loginList.Value)
                {
                    _currentLogin = agencies.First(a => a.name == loginList.Value);
                }
                return _currentLogin;
            }
        }
        #endregion

        //------// Fields \\------------------------------------------------\\
        public FidesicUser CurrentUser;
        public List<Payment> GetUnapprovedPayments;
        public List<Invoice> GetUnapprovedInvoices;
        public List<ItemTemplate> GetAllInvoiceItemTemplates;
        //------\\ Fields //------------------------------------------------//



        //------// Methods \\-----------------------------------------------\\
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                foreach (var a in agencies)
                {
                    loginList.Items.Add(new ListItem { Text = a.name, Value = a.name });
                }
            }
        }

        public void EnsureConnectedToFidesic()
        {
            try
            {
                if (!FidesicController.ConnectedToFidesic
                    || FidesicController.CurrentUser == null
                    || FidesicController.CurrentUser.CompanyName != loginList.Value)
                {
                    bool allowSwitch = (FidesicController.ConnectedToFidesic && FidesicController.CurrentUser != null && FidesicController.CurrentUser.CompanyName != loginList.Value);

                    FidesicController.ConnectToFidesic(currentLogin.apiKey, currentLogin.name, currentLogin.apiPW, allowSwitch);
                }
            }
            catch (NotConnectedToFidesicException) { FidesicController.ConnectToFidesic(currentLogin.apiKey, currentLogin.name, currentLogin.apiPW); }
            catch (FidesicSessionExpiredException) { FidesicController.ConnectToFidesic(currentLogin.apiKey, currentLogin.name, currentLogin.apiPW); }
        }

        public void DisplayError(System.Exception exception)
        {
            DisplayError(exception.ToString());
        }
        public void DisplayError(string errorMessage)
        {
            errorPlaceholder.Visible = true;
            errorLiteral.Text = errorMessage;
        }


        #region "Event Handlers"

        protected void btnLogin_Click(object sender, EventArgs args)
        {
            try
            {
                EnsureConnectedToFidesic();
                DisplayError("Successfully logged in: " + DateTime.Now.ToString());
                //DisplayError("Logged in as: " + CurrentUser.Email);
            }
            catch (System.Exception exception)
            {
                DisplayError(exception);
            }
        }


        protected void btnGetUnapprovedPayments_Click(object sender, EventArgs args)
        {
            try
            {
                EnsureConnectedToFidesic();
                GetUnapprovedPayments = FidesicController.GetUnapprovedPayments();
            }
            catch (System.Exception exception)
            {
                DisplayError(exception);
            }
        }
        protected void btnGetUnapprovedInvoices_Click(object sender, EventArgs args)
        {
            try
            {
                EnsureConnectedToFidesic();
                GetUnapprovedInvoices = FidesicController.GeUnapprovedInvoices();
            }
            catch (System.Exception exception)
            {
                DisplayError(exception);
            }
        }

        protected void btnApproveInvoice_Click(object sender, EventArgs args)
        {
            try
            {
                int invoiceID = int.Parse(approvalInvoiceID.Value);
                EnsureConnectedToFidesic();
                FidesicController.UpdateApprovalStatus(invoiceID, ApprovalType.Invoice, ApprovalStatus.Approve);
            }
            catch (System.Exception exception)
            {
                DisplayError(exception);
            }
        }
        protected void btnDisapproveInvoice_Click(object sender, EventArgs args)
        {
            try
            {
                int invoiceID = int.Parse(approvalInvoiceID.Value);
                EnsureConnectedToFidesic();
                FidesicController.UpdateApprovalStatus(invoiceID, ApprovalType.Invoice, ApprovalStatus.Disapprove);
            }
            catch (System.Exception exception)
            {
                DisplayError(exception);
            }
        }
        protected void btnApprovePayment_Click(object sender, EventArgs args)
        {
            try
            {
                int paymentID = int.Parse(approvalPaymentID.Value);
                EnsureConnectedToFidesic();
                FidesicController.UpdateApprovalStatus(paymentID, ApprovalType.Payment, ApprovalStatus.Approve);
            }
            catch (System.Exception exception)
            {
                DisplayError(exception);
            }
        }
        protected void btnDisapprovePayment_Click(object sender, EventArgs args)
        {
            try
            {
                int paymentID = int.Parse(approvalPaymentID.Value);
                EnsureConnectedToFidesic();
                FidesicController.UpdateApprovalStatus(paymentID, ApprovalType.Payment, ApprovalStatus.Disapprove);
            }
            catch (System.Exception exception)
            {
                DisplayError(exception);
            }
        }

        #endregion
        //------\\ Methods //-----------------------------------------------//
    }
}