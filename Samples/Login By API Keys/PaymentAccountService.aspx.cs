﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using Fidesic;
using Fidesic.Data;
using Fidesic.Web;

namespace FidesicSDK
{
    public partial class APIPaymentAccountService : System.Web.UI.Page
    {
        //------// Properties \\--------------------------------------------\\
        private LoginByAPIController _fidesicController = null;
        public LoginByAPIController FidesicController
        {
            get
            {
                if (_fidesicController == null)
                {
                    _fidesicController = new LoginByAPIController();
                }

                return _fidesicController;
            }
        }
        //------\\ Properties //--------------------------------------------//

        #region logins
        public class LoginInfo
        {
            public string name;
            public string apiKey;
            public string apiPW;

            public LoginInfo() { }
            public LoginInfo(string _name, string _apiKey, string _apiPW)
            {
                name = _name;
                apiKey = _apiKey;
                apiPW = _apiPW;
            }
        }
        private List<LoginInfo> _agencies;
        public List<LoginInfo> agencies
        {
            get
            {
                if (_agencies == null)
                {
                    _agencies = new List<LoginInfo>()
                    {
                        new LoginInfo("The World Online","jyJEjoxVZCBkFluU","9EH6gsojKuh43tNssk2U"),
                        new LoginInfo("The QB Online","zGUBPLqSzaLVgwCf","BEtNltqHrlVrHfhfbklx")
                    };
                }
                return _agencies;
            }
        }
        private LoginInfo _currentLogin;
        protected LoginInfo currentLogin
        {
            get
            {
                if (_currentLogin == null || _currentLogin.name != loginList.Value)
                {
                    _currentLogin = agencies.First(a => a.name == loginList.Value);
                }
                return _currentLogin;
            }
        }
        #endregion

        //------// Fields \\------------------------------------------------\\
        public FidesicUser CurrentUser;
        public List<BankAccount> GetAllBankAccounts_Accounts;
        public BankAccount GetBankAccount_Account;
        public List<CreditCardAccount> GetAllCreditCardAccounts_Accounts;
        public CreditCardAccount GetCreditCardAccount_Account;
        //------\\ Fields //------------------------------------------------//



        //------// Methods \\-----------------------------------------------\\
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                foreach (var a in agencies)
                {
                    loginList.Items.Add(new ListItem { Text = a.name, Value = a.name });
                }
            }
        }


        public void EnsureConnectedToFidesic()
        {
            try
            {
                if (!FidesicController.ConnectedToFidesic
                    || FidesicController.CurrentUser == null
                    || FidesicController.CurrentUser.CompanyName != loginList.Value)
                {
                    bool allowSwitch = (FidesicController.ConnectedToFidesic && FidesicController.CurrentUser != null && FidesicController.CurrentUser.CompanyName != loginList.Value);

                    FidesicController.ConnectToFidesic(currentLogin.apiKey, currentLogin.name, currentLogin.apiPW, allowSwitch);
                }
            }
            catch (NotConnectedToFidesicException) { FidesicController.ConnectToFidesic(currentLogin.apiKey, currentLogin.name, currentLogin.apiPW); }
            catch (FidesicSessionExpiredException) { FidesicController.ConnectToFidesic(currentLogin.apiKey, currentLogin.name, currentLogin.apiPW); }
        }


        public void DisplayError(System.Exception exception)
        {
            DisplayError(exception.ToString());
        }


        public void DisplayError(string errorMessage)
        {
            errorPlaceholder.Visible = true;
            errorLiteral.Text = errorMessage;
        }



        #region "Event Handlers"

        protected void btnLogin_Click(object sender, EventArgs args)
        {
            try
            {
                EnsureConnectedToFidesic();
            }
            catch (System.Exception exception)
            {
                DisplayError(exception);
            }
        }


        protected void btnGetAllBankAccounts_Click(object sender, EventArgs args)
        {
            try
            {
                EnsureConnectedToFidesic();
                GetAllBankAccounts_Accounts = FidesicController.GetAllBankAccounts();
            }
            catch (System.Exception exception)
            {
                DisplayError(exception);
            }
        }


        protected void btnGetBankAccountByName_Click(object sender, EventArgs args)
        {
            try
            {
                EnsureConnectedToFidesic();
                GetBankAccount_Account = FidesicController.GetBankAccountByName(txtBankAccountName.Value.Trim());


                updateBankAccountFieldset.Visible = true;

                txtBankAccountName3.Value = GetBankAccount_Account.AccountName;
                txtBankAccountNumber2.Value = GetBankAccount_Account.AccountNumber;
                txtBankAccountRoutingNumber2.Value = GetBankAccount_Account.RoutingNumber;
                accountType2.SelectedValue = GetBankAccount_Account.Type.ToString();
            }
            catch (System.Exception exception)
            {
                DisplayError(exception);
            }
        }


        protected void btnAddBankAccount_Click(object sender, EventArgs args)
        {
            try
            {
                BankAccount newAccount = new BankAccount();

                newAccount.AccountName = txtBankAccountName2.Value.Trim();
                newAccount.AccountNumber = txtBankAccountNumber.Value.Trim();
                newAccount.RoutingNumber = txtBankAccountRoutingNumber.Value.Trim();
                newAccount.Type = (BankAccountType)Enum.Parse(typeof(BankAccountType), accountType.SelectedValue);


                EnsureConnectedToFidesic();
                FidesicController.AddBankAccount(newAccount);
            }
            catch (System.Exception exception)
            {
                DisplayError(exception);
            }
        }


        protected void btnUpdateBankAccount_Click(object sender, EventArgs args)
        {
            try
            {
                BankAccount bankAccount = new BankAccount();

                bankAccount.AccountName = txtBankAccountName3.Value.Trim();
                bankAccount.AccountNumber = txtBankAccountNumber2.Value.Trim();
                bankAccount.RoutingNumber = txtBankAccountRoutingNumber2.Value.Trim();
                bankAccount.Type = (BankAccountType)Enum.Parse(typeof(BankAccountType), accountType2.SelectedValue);


                EnsureConnectedToFidesic();
                FidesicController.UpdateBankAccount(bankAccount);

                updateBankAccountFieldset.Visible = true;
            }
            catch (System.Exception exception)
            {
                DisplayError(exception);
            }
        }
        protected void btnSetDefaultPaymentAccount_Click(object sender, EventArgs args)
        {
            try
            {
                int paymentAccountID;
                if (Int32.TryParse(txtPaymentAccountID.Value.Trim(), out paymentAccountID))
                {
                    EnsureConnectedToFidesic();
                    FidesicController.SetDefaultPaymentAccount(paymentAccountID);
                }
                else
                {
                    throw new Exception("paymentAccount ID was invalid.");
                }

            }
            catch (System.Exception exception)
            {
                DisplayError(exception);
            }
        }


        protected void btnGetAllCreditCardAccounts_Click(object sender, EventArgs args)
        {
            try
            {
                EnsureConnectedToFidesic();
                GetAllCreditCardAccounts_Accounts = FidesicController.GetAllCreditCardAccounts();
            }
            catch (System.Exception exception)
            {
                DisplayError(exception);
            }
        }


        protected void btnGetCreditCardAccountByID_Click(object sender, EventArgs args)
        {
            try
            {
                int paymentAccountID;
                if (Int32.TryParse(txtCreditCardAccountID.Value.Trim(), out paymentAccountID))
                {
                    EnsureConnectedToFidesic();
                    GetCreditCardAccount_Account = FidesicController.GetCreditCardAccount(paymentAccountID);
                }
                else
                {
                    throw new Exception("invalid payment account ID");
                }

                updateCreditCardAccountFieldset.Visible = true;

                txtCreditCardAccountName3.Value = GetCreditCardAccount_Account.AccountName;
                txtCreditCardAccountNumber2.Value = GetCreditCardAccount_Account.AccountNumber;
                txtCreditCardCardName2.Value = GetCreditCardAccount_Account.AccountHolderName;
                txtCreditCardCardNumber2.Value = GetCreditCardAccount_Account.CardNumber;
                txtCreditCardExpirationMonth2.Value = GetCreditCardAccount_Account.ExpirationDate.Month.ToString();
                txtCreditCardExpirationYear2.Value = GetCreditCardAccount_Account.ExpirationDate.Year.ToString();

                if (GetCreditCardAccount_Account.Address != null)
                {
                    txtCreditCardAddress1_2.Value = GetCreditCardAccount_Account.Address.Address1;
                    txtCreditCardAddress2_2.Value = GetCreditCardAccount_Account.Address.Address2;
                    txtCreditCardCity2.Value = GetCreditCardAccount_Account.Address.City;
                    txtCreditCardState2.Value = GetCreditCardAccount_Account.Address.State;
                    txtCreditCardZip2.Value = GetCreditCardAccount_Account.Address.Zip;
                }
            }
            catch (System.Exception exception)
            {
                DisplayError(exception);
            }
        }


        protected void btnAddCreditCardAccount_Click(object sender, EventArgs args)
        {
            try
            {
                CreditCardAccount newAccount = new CreditCardAccount();

                newAccount.AccountName = txtCreditCardAccountName2.Value.Trim();
                newAccount.AccountNumber = txtCreditCardAccountNumber.Value.Trim();
                newAccount.AccountHolderName = txtCreditCardCardName.Value.Trim();
                newAccount.CardNumber = txtCreditCardCardNumber.Value.Trim();

                int expirationMonth, expirationYear;

                if (Int32.TryParse(txtCreditCardExpirationMonth.Value.Trim(), out expirationMonth) && Int32.TryParse(txtCreditCardExpirationYear.Value.Trim(), out expirationYear))
                {
                    newAccount.ExpirationDate = new DateTime(expirationYear, expirationMonth, 1);
                }


                newAccount.Address = new Address();
                newAccount.Address.Address1 = txtCreditCardAddress1.Value.Trim();
                newAccount.Address.Address2 = txtCreditCardAddress2.Value.Trim();
                newAccount.Address.City = txtCreditCardCity.Value.Trim();
                newAccount.Address.State = txtCreditCardState.Value.Trim();
                newAccount.Address.Zip = txtCreditCardZip.Value.Trim();
                
                
                EnsureConnectedToFidesic();
                FidesicController.AddCreditCardAccount(newAccount);
            }
            catch (System.Exception exception)
            {
                DisplayError(exception);
            }
        }


        protected void btnUpdateCreditCardAccount_Click(object sender, EventArgs args)
        {
            try
            {
                CreditCardAccount creditCard = new CreditCardAccount();

                creditCard.AccountName = txtCreditCardAccountName2.Value.Trim();
                creditCard.AccountNumber = txtCreditCardAccountNumber.Value.Trim();
                creditCard.AccountHolderName = txtCreditCardCardName.Value.Trim();
                creditCard.CardNumber = txtCreditCardCardNumber.Value.Trim();

                int expirationMonth, expirationYear;

                if (Int32.TryParse(txtCreditCardExpirationMonth.Value.Trim(), out expirationMonth) && Int32.TryParse(txtCreditCardExpirationYear.Value.Trim(), out expirationYear))
                {
                    creditCard.ExpirationDate = new DateTime(expirationYear, expirationMonth, 1);
                }


                creditCard.Address = new Address();
                creditCard.Address.Address1 = txtCreditCardAddress1.Value.Trim();
                creditCard.Address.Address2 = txtCreditCardAddress2.Value.Trim();
                creditCard.Address.City = txtCreditCardCity.Value.Trim();
                creditCard.Address.State = txtCreditCardState.Value.Trim();
                creditCard.Address.Zip = txtCreditCardZip.Value.Trim();


                EnsureConnectedToFidesic();
                FidesicController.UpdateCreditCardAccount(creditCard);

                updateCreditCardAccountFieldset.Visible = true;
            }
            catch (System.Exception exception)
            {
                DisplayError(exception);
            }
        }

        #endregion
        //------\\ Methods //-----------------------------------------------//
    }
}