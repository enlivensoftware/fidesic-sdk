﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;

using Fidesic;
using Fidesic.Data;

namespace FidesicSDK
{
    public partial class APIGLCodeService : System.Web.UI.Page
    {
        #region Properties
        //------// Properties \\--------------------------------------------\\
        private LoginByAPIController _fidesicController = null;
        public LoginByAPIController FidesicController
        {
            get
            {
                if (_fidesicController == null)
                {
                    _fidesicController = new LoginByAPIController();
                }

                return _fidesicController;
            }
        }
        //------\\ Properties //--------------------------------------------//

        #region logins
        public class LoginInfo
        {
            public string name;
            public string apiKey;
            public string apiPW;

            public LoginInfo() { }
            public LoginInfo(string _name, string _apiKey, string _apiPW)
            {
                name = _name;
                apiKey = _apiKey;
                apiPW = _apiPW;
            }
        }
        private List<LoginInfo> _agencies;
        public List<LoginInfo> agencies
        {
            get
            {
                if (_agencies == null)
                {
                    _agencies = new List<LoginInfo>()
                    {
                        new LoginInfo("The World Online","jyJEjoxVZCBkFluU","9EH6gsojKuh43tNssk2U"),
                        new LoginInfo("The QB Online","zGUBPLqSzaLVgwCf","BEtNltqHrlVrHfhfbklx")
                    };
                }
                return _agencies;
            }
        }
        private LoginInfo _currentLogin;
        protected LoginInfo currentLogin
        {
            get
            {
                if (_currentLogin == null || _currentLogin.name != loginList.Value)
                {
                    _currentLogin = agencies.First(a => a.name == loginList.Value);
                }
                return _currentLogin;
            }
        }
        #endregion


        //------// Fields \\------------------------------------------------\\
        public FidesicUser CurrentUser;
        public List<GLCode> GetGLCodes;
        public int GLCodeID = -1;
        public int ClassID = -1;
        public int DepartmentID = -1;
        //------\\ Fields //------------------------------------------------//



        //------// Methods \\-----------------------------------------------\\
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                foreach (var a in agencies)
                {
                    loginList.Items.Add(new ListItem { Text = a.name, Value = a.name });
                }
            }
        }

        public void EnsureConnectedToFidesic()
        {
            try
            {
                if (!FidesicController.ConnectedToFidesic
                    || FidesicController.CurrentUser == null
                    || FidesicController.CurrentUser.CompanyName != loginList.Value)
                {
                    bool allowSwitch = (FidesicController.ConnectedToFidesic && FidesicController.CurrentUser != null && FidesicController.CurrentUser.CompanyName != loginList.Value);

                    FidesicController.ConnectToFidesic(currentLogin.apiKey, currentLogin.name, currentLogin.apiPW, allowSwitch);
                }
            }
            catch (NotConnectedToFidesicException) { FidesicController.ConnectToFidesic(currentLogin.apiKey, currentLogin.name, currentLogin.apiPW); }
            catch (FidesicSessionExpiredException) { FidesicController.ConnectToFidesic(currentLogin.apiKey, currentLogin.name, currentLogin.apiPW); }
        }

        public void DisplayError(System.Exception exception)
        {
            DisplayError(exception.ToString());
        }
        public void DisplayError(string errorMessage)
        {
            errorPlaceholder.Visible = true;
            errorLiteral.Text = errorMessage;
        }


        #region "Event Handlers"

        protected void btnLogin_Click(object sender, EventArgs args)
        {
            try
            {
                EnsureConnectedToFidesic();
                DisplayError("Successfully logged in: " + DateTime.Now.ToString());
                //DisplayError("Logged in as: " + CurrentUser.Email);
            }
            catch (System.Exception exception)
            {
                DisplayError(exception);
            }
        }


        protected void btnGetGLCodes_Click(object sender, EventArgs args)
        {
            try
            {
                EnsureConnectedToFidesic();
                GetGLCodes = FidesicController.GetAllGLCodes(1,100);
            }
            catch (System.Exception exception)
            {
                DisplayError(exception);
            }
        }


        protected void btnDeleteGLCode_Click(object sender, EventArgs args)
        {
            try
            {
                var tmpGLCode = new GLCode()
                {
                    FullGLCode = FullGLCode.Value //Accounting Chart of Account Code
                };
                EnsureConnectedToFidesic();
                FidesicController.DeleteGLCode(tmpGLCode);
            }
            catch (System.Exception exception)
            {
                DisplayError(exception);
            }
        }

        protected void btnAddGLCode_Click(object sender, EventArgs args)
        {
            try
            {
                GLCode newGLCode = new GLCode()
                {
                    FriendlyName = addAccountDescription.Value, //Displayed name for users
                    FullGLCode = addAccountNumber.Value,        //Accounting Chart of Account Code
                    ExternalID = addExternalID.Value ?? null      //Internal Accounting Package ID. Ignore if does not exist.
                };


                EnsureConnectedToFidesic();
                GLCodeID = FidesicController.AddGLCode(newGLCode);
            }
            catch (System.Exception exception)
            {
                DisplayError(exception);
            }
        }
        protected void btnAddGLClass_Click(object sender, EventArgs args)
        {
            try
            {
                GLClass newGLClass = new GLClass()
                {
                    Value = addClassAccount.Value,
                    FullGLClass = addClassDesc.Value
                };


                EnsureConnectedToFidesic();
                ClassID = FidesicController.AddGLCLass(newGLClass);
            }
            catch (System.Exception exception)
            {
                DisplayError(exception);
            }
        }
        protected void btnAddGLDepartment_Click(object sender, EventArgs args)
        {
            try
            {
                GLDepartment newGLDepartment = new GLDepartment()
                {
                    Value = addDeptAccount.Value,
                    FullGLDepartment = addDeptDesc.Value
                };

                EnsureConnectedToFidesic();
                DepartmentID = FidesicController.AddGLDepartment(newGLDepartment);
            }
            catch (System.Exception exception)
            {
                DisplayError(exception);
            }
        }

        #endregion
        //------\\ Methods //-----------------------------------------------//
    }
}