﻿using Fidesic;
using Fidesic.Web;

namespace FidesicSDK
{
    public class LoginByUserNameController : FidesicWebController
    {
        //------// Properties \\--------------------------------------------\\
        
        //------\\ Properties //--------------------------------------------//



        //------// Constructors \\------------------------------------------\\
        public LoginByUserNameController()
            : base(BuildApiSettings(), true)
        { }
        //------\\ Constructors //------------------------------------------//



        //------// Methods \\-----------------------------------------------\\
        public static FidesicApiSettings BuildApiSettings()
        {
            FidesicApiSettings apiSettings = new FidesicApiSettings
            {
                //LOG IN VIA Fidesic User Credentials
                //NOTE: please contact Fidesic support for API keys for live environment
                ApiKey = "Lux1D25RBb7fUNcl",
                SecretKey = "nW903AxyewvrYpEBQmdU",
                DataFormat = OperationDataFormat.Json,
                ConsumerIsTrusted = true,
                //CHANGE THIS TO .Live for Production, .Staging for staging
                Environment = FidesicEnvironment.Staging
            };

            return apiSettings;
        }

        protected override void ClearFidesicSessionStorage()
        {
        //    throw new NotImplementedException();
        }

        protected override string DetermineEmailFromEnvironment()
        {
            return null;
        }

        protected override string DetermineCompanyNameFromEnvironment()
        {
            return null;
        }
        //------\\ Methods //-----------------------------------------------//
    }
}