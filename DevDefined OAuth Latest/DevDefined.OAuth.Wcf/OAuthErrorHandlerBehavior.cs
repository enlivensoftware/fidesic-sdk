﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.IdentityModel.Policy;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Channels;
using System.ServiceModel.Description;
using System.ServiceModel.Dispatcher;

using DevDefined.OAuth.Framework;

namespace DevDefined.OAuth.Wcf
{
    [AttributeUsage(AttributeTargets.Interface | AttributeTargets.Class)]
    public class OAuthErrorHandlerBehavior : Attribute, IServiceBehavior
    {
        //------// Properties \\--------------------------------------------\\
        public virtual string ApplicationName { get; set; }
        //------\\ Properties //--------------------------------------------//



        //------// Constructors \\------------------------------------------\\
        public OAuthErrorHandlerBehavior(string applicationName)
        {
            ApplicationName = applicationName;
        }
        //------\\ Constructors //------------------------------------------//



        //------// Methods \\-----------------------------------------------\\
        public void AddBindingParameters(ServiceDescription serviceDescription, ServiceHostBase serviceHostBase, System.Collections.ObjectModel.Collection<ServiceEndpoint> endpoints, BindingParameterCollection bindingParameters)
        {
            /* Do nothing */
        }


        public void ApplyDispatchBehavior(ServiceDescription serviceDescription, ServiceHostBase serviceHostBase)
        {
            IErrorHandler errorHandler = new OAuthErrorHandler(ApplicationName);

            foreach (ChannelDispatcherBase channelDispatcherBase in serviceHostBase.ChannelDispatchers)
            {
                ChannelDispatcher channelDispatcher = channelDispatcherBase as ChannelDispatcher;
                channelDispatcher.ErrorHandlers.Add(errorHandler);
            }    

            //foreach (ChannelDispatcher dispatcher in serviceHostBase.ChannelDispatchers)
            //{
            //    if (dispatcher.ErrorHandlers.Count > 0)
            //    {
            //        dispatcher.ErrorHandlers.Remove(dispatcher.ErrorHandlers[0]);
            //    }

            //    dispatcher.ErrorHandlers.Add(new OAuthErrorHandler(ApplicationName));
            //}
        }


        public void Validate(ServiceDescription serviceDescription, ServiceHostBase serviceHostBase)
        {
            /* Do nothing */
        }
        //------\\ Methods //-----------------------------------------------//
    }
}
