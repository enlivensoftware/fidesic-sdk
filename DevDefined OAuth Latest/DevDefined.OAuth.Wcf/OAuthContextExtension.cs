﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.ServiceModel.Description;
using System.ServiceModel.Security;
using System.ServiceModel.Web;
using System.Text;

using DevDefined.OAuth.Framework;

namespace DevDefined.OAuth.Wcf
{
    [DataContract]
    public class OAuthContextExtension : IExtension<InstanceContext>
    {
        //------// Properties \\--------------------------------------------\\
        [DataMember] public virtual IOAuthContext OAuthContext { get; set; }
        //------\\ Properties //--------------------------------------------//



        //------// Constructors \\------------------------------------------\\
        public OAuthContextExtension()
        { }
        //------\\ Constructors //------------------------------------------//



        //------// Methods \\-----------------------------------------------\\
        public void Attach(InstanceContext owner)
        { }


        public void Detach(InstanceContext owner)
        { }
        //------\\ Methods //-----------------------------------------------//
    }
}
