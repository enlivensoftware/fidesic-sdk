﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.ServiceModel.Description;
using System.ServiceModel.Dispatcher;
using System.Text;

namespace DevDefined.OAuth.Wcf
{
    [AttributeUsage(AttributeTargets.Method)]
    public class OAuthResourceAttribute : Attribute, IOperationBehavior
    {
        //------// Properties \\--------------------------------------------\\
        public virtual bool ProtectedResource { get; set; }
        public virtual string[] Roles { get; set; }
        //------\\ Properties //--------------------------------------------//



        //------// Constructors \\------------------------------------------\\
        public OAuthResourceAttribute()
        {
            ProtectedResource = true;
            Roles = new string[0];
        }


        public OAuthResourceAttribute(bool protectedResource)
        {
            ProtectedResource = protectedResource;
            Roles = new string[0];
        }


        public OAuthResourceAttribute(bool protectedResource, params string[] roles)
        {
            ProtectedResource = protectedResource;
            Roles = roles;
        }
        //------\\ Constructors //------------------------------------------//



        //------// Methods \\-----------------------------------------------\\
        public void AddBindingParameters(OperationDescription operationDescription, BindingParameterCollection bindingParameters)
        {
 	        /* Do nothing */
        }


        public void ApplyClientBehavior(OperationDescription operationDescription, ClientOperation clientOperation)
        {
 	        /* Do nothing */
        }


        public void ApplyDispatchBehavior(OperationDescription operationDescription, DispatchOperation dispatchOperation)
        {
            IOperationInvoker defaultInvoker = dispatchOperation.Invoker;
            dispatchOperation.Invoker = new AuthorizationOperationInvoker(defaultInvoker, this);
        }


        public void Validate(OperationDescription operationDescription)
        {
 	        /* Do nothing */
        }
        //------\\ Methods //-----------------------------------------------//
}
}