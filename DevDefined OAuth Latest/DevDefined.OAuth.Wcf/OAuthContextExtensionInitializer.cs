﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.ServiceModel.Description;
using System.ServiceModel.Dispatcher;
using System.ServiceModel.Security;
using System.ServiceModel.Web;
using System.Text;
using System.Threading;

using DevDefined.OAuth.Framework;

namespace DevDefined.OAuth.Wcf
{
    public class OAuthContextExtensionInitializer : IInstanceContextInitializer
    {
        //------// Properties \\--------------------------------------------\\
        private static int _initializerIndex = 0;
        //------\\ Properties //--------------------------------------------//



        //------// Constructors \\------------------------------------------\\
        public OAuthContextExtensionInitializer()
        { }
        //------\\ Constructors //------------------------------------------//



        //------// Methods \\-----------------------------------------------\\
        public void Initialize(InstanceContext instanceContext, Message message)
        {
            Interlocked.Increment(ref _initializerIndex);


            OAuthContextExtension oAuthContextExtension = new OAuthContextExtension() {
                OAuthContext = new OAuthContextBuilder().FromRequestMessage(OperationContext.Current.RequestContext.RequestMessage)
            };

            instanceContext.Extensions.Add(oAuthContextExtension);
        }
        //------\\ Methods //-----------------------------------------------//
    }
}
