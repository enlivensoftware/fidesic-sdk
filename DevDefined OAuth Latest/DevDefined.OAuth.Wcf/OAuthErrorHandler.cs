﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.IdentityModel.Policy;
using System.Net;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Channels;
using System.ServiceModel.Description;
using System.ServiceModel.Dispatcher;
using System.ServiceModel.Web;

using DevDefined.OAuth.Framework;

namespace DevDefined.OAuth.Wcf
{
    public class OAuthErrorHandler : IErrorHandler
    {
        //------// Properties \\--------------------------------------------\\
        public virtual string ApplicationName { get; set; }
        //------\\ Properties //--------------------------------------------//



        //------// Constructors \\------------------------------------------\\
        public OAuthErrorHandler(string applicationName)
        {
            ApplicationName = applicationName;
        }
        //------\\ Constructors //------------------------------------------//



        //------// Methods \\-----------------------------------------------\\
        public bool HandleError(Exception exception)
        {
            try
            {
                if (!EventLog.SourceExists(ApplicationName))
                {
                    EventLog.CreateEventSource(ApplicationName, "Application");
                }


                EventLog eventLog = new EventLog();
                eventLog.Source = ApplicationName;

                eventLog.WriteEntry(exception.ToString(), EventLogEntryType.Error);

                eventLog.Close();

            }
            catch
            {
                throw new Exception("error logging failed.");
            }
            return true;
        }


        public void ProvideFault(Exception exception, MessageVersion messageVersion, ref Message faultMessage)
        {
            if (exception is OAuthException)
            {
                OAuthException oAuthException = exception as OAuthException;

                if (oAuthException == null)
                {
                    oAuthException = new OAuthException("An unhandled error has occurred.", exception);
                }


                if (oAuthException != null)
                {
                    OAuthFault faultDetail = null;
                    HttpStatusCode statusCode = HttpStatusCode.InternalServerError;

                    if (oAuthException.Report != null)
                    {
                        faultDetail = new OAuthFault(oAuthException.Report.Problem, oAuthException.Report.ProblemAdvice);
                    }
                    else
                    {
                        faultDetail = new OAuthFault(oAuthException.Message, null);
                    }


                    if (String.Equals(faultDetail.Problem, "An Access Token does not exist for the client."))
                    {
                        if (String.IsNullOrWhiteSpace(faultDetail.Advice))
                        {
                            faultDetail.Advice = "A session of the user in context has expired or is no longer valid. Please re-authenticate.";
                        }

                        statusCode = HttpStatusCode.Forbidden;
                    }
                    else
                    {
                        statusCode = HttpStatusCode.InternalServerError;
                    }

                    FaultException<string> fault = new FaultException<string>(faultDetail.Advice ?? faultDetail.Problem, faultDetail.Problem);
                    faultMessage = Message.CreateMessage(MessageVersion.None, fault.CreateMessageFault(), faultDetail.Advice ?? String.Empty);
                    faultMessage.Properties[HttpResponseMessageProperty.Name] = new HttpResponseMessageProperty { StatusCode = statusCode, StatusDescription = faultDetail.Problem };
                }
            }
            else
            {
                string innerException = "fail";
                try
                {
                    innerException = exception.InnerException != null ? exception.InnerException.ToString() : exception.Message;
                }
                catch
                {
                    innerException = "new fail";
                }
                faultMessage = Message.CreateMessage(MessageVersion.None, new FaultCode(innerException), innerException, innerException);
                faultMessage.Properties[HttpResponseMessageProperty.Name] = new HttpResponseMessageProperty { StatusCode = HttpStatusCode.NotAcceptable, StatusDescription = innerException };
            }

        }
        //------\\ Methods //-----------------------------------------------//
    }
}

