﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.IdentityModel.Policy;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Description;

using Microsoft.ServiceModel.Web;

using DevDefined.OAuth;
using DevDefined.OAuth.Framework;
using DevDefined.OAuth.Provider;
using DevDefined.OAuth.Provider.Inspectors;
using DevDefined.OAuth.Storage;
using DevDefined.OAuth.Storage.Basic;
using DevDefined.OAuth.Testing;

namespace DevDefined.OAuth.Wcf
{
    public abstract class OAuthServiceHostFactory : ServiceHostFactory
    {
        //------// Properties \\--------------------------------------------\\
        private static TimeSpan? _tokenExpiration = null;
        public static TimeSpan TokenExpiration
        {
            get
            {
                if (!_tokenExpiration.HasValue)
                {
                    return new TimeSpan(0, 20, 0);
                }

                return _tokenExpiration.Value;
            }
            set { _tokenExpiration = value; }
        }

        protected abstract string ErrorLogApplicationName { get; }
        private static ITokenRepository<DevDefined.OAuth.Storage.Basic.AccessToken> _accessTokenRespository = null;
        public static ITokenRepository<DevDefined.OAuth.Storage.Basic.AccessToken> AccessTokenRepository
        {
            get
            {
                if (_accessTokenRespository == null)
                {
                    throw new InvalidOperationException("The Access Token Repository has not been provisioned.");
                }

                return _accessTokenRespository;
            }
            set { _accessTokenRespository = value; }
        }

        protected virtual void AssignOperationBehaviors(OperationDescription operation)
        {
            if (operation.Behaviors.Find<OAuthResourceAttribute>() == null)
            {
                operation.Behaviors.Add(new OAuthResourceAttribute());
            }
            if (operation.Faults.Find(operation.Name) == null)
            {
                FaultDescription item = new FaultDescription(operation.Name)
                {
                    DetailType = typeof(OAuthFault)
                };
                operation.Faults.Add(item);
            }
        } 

        private static ITokenRepository<DevDefined.OAuth.Storage.Basic.RequestToken> _requestTokenRespository = null;
        public static ITokenRepository<DevDefined.OAuth.Storage.Basic.RequestToken> RequestTokenRepository
        {
            get
            {
                if (_requestTokenRespository == null)
                {
                    throw new InvalidOperationException("The Request Token Repository has not been provisioned.");
                }

                return _requestTokenRespository;
            }
            set { _requestTokenRespository = value; }
        }


        private static ITokenStore _tokenStore = null;
        public static ITokenStore TokenStore
        {
            get
            {
                if (_tokenStore == null)
                {
                    throw new InvalidOperationException("The Token Store has not been provisioned.");
                }

                return _tokenStore;
            }
            set { _tokenStore = value; }
        }


        public static INonceStore _nonceStore = null;
        public static INonceStore NonceStore
        {
            get
            {
                if (_nonceStore == null)
                {
                    throw new InvalidOperationException("The Nonce Store has not been provisioned.");
                }

                return _nonceStore;
            }
            set { _nonceStore = value; }
        }


        public static IConsumerStore _consumerStore = null;
        public static IConsumerStore ConsumerStore
        {
            get
            {
                if (_consumerStore == null)
                {
                    throw new InvalidOperationException("The Consumer Store has not been provisioned.");
                }

                return _consumerStore;
            }
            set { _consumerStore = value; }
        }


        private static IOAuthProvider _oAuthProvider = null;
        public static IOAuthProvider OAuthProvider
        {
            get
            {
                if (_oAuthProvider == null)
                {
                    throw new InvalidOperationException("The OAuth Provider has not been provisioned.");
                }

                return _oAuthProvider;
            }
            set { _oAuthProvider = value; }
        }
        //------\\ Properties //--------------------------------------------//



        //------// Constructors \\------------------------------------------\\
        public OAuthServiceHostFactory()
        { }
        //------\\ Constructors //------------------------------------------//



        //------// Methods \\-----------------------------------------------\\
        protected override ServiceHost CreateServiceHost(Type serviceType, Uri[] baseAddresses)
        {
            WebServiceHost2 serviceHost = new WebServiceHost2(serviceType, true, baseAddresses);


            foreach (ServiceEndpoint endpoint in serviceHost.Description.Endpoints)
            {
                foreach (OperationDescription operation in endpoint.Contract.Operations)
                {
                    bool hasResourceBehavior = false;

                    foreach (IOperationBehavior behavior in operation.Behaviors)
                    {
                        if (behavior is OAuthResourceAttribute)
                        {
                            hasResourceBehavior = true;
                            break;
                        }
                    }


                    if (!hasResourceBehavior)
                    {
                        operation.Behaviors.Add(new OAuthResourceAttribute());
                    }
                }
            }


            return serviceHost;
        }
        //------\\ Methods //-----------------------------------------------//
    }
}