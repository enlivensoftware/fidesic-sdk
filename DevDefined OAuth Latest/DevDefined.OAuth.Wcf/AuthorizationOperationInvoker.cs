﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Claims;
using System.IdentityModel.Policy;
using System.Linq;
using System.Net;
using System.Security.Permissions;
using System.Security.Principal;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.ServiceModel.Description;
using System.ServiceModel.Dispatcher;
using System.ServiceModel.Security;
using System.ServiceModel.Web;
using System.Text;

using DevDefined.OAuth.Framework;
using DevDefined.OAuth.Provider;
using DevDefined.OAuth.Storage.Basic;

namespace DevDefined.OAuth.Wcf
{
    public class AuthorizationOperationInvoker : IOperationInvoker
    {
        //------// Properties \\--------------------------------------------\\
        public bool IsSynchronous
        {
            get { return _defaultInvoker.IsSynchronous; }
        }


        public virtual OAuthResourceAttribute ResourceAttribute { get; protected set; }


        private RequestContext _requestContext = null;
        public virtual RequestContext RequestContext
        {
            get
            {
                if (_requestContext == null)
                {
                    _requestContext = OperationContext.Current.RequestContext;
                }

                return _requestContext;
            }
        }


        public virtual IOAuthContext OAuthContext
        {
            get { return OperationContext.Current.InstanceContext.Extensions.Find<OAuthContextExtension>().OAuthContext; }
        }
        //------\\ Properties //--------------------------------------------//



        //------// Fields \\------------------------------------------------\\
        protected IOperationInvoker _defaultInvoker = null;
        //------\\ Fields //------------------------------------------------//



        //------// Constructors \\------------------------------------------\\
        public AuthorizationOperationInvoker(IOperationInvoker defaultInvoker, OAuthResourceAttribute resourceAttribute)
        {
            if (defaultInvoker == null) { throw new ArgumentNullException("defaultInvoker"); }
            if (resourceAttribute == null) { throw new ArgumentNullException("resourceAttribute"); }

            _defaultInvoker = defaultInvoker;
            ResourceAttribute = resourceAttribute;
        }
        //------\\ Constructors //------------------------------------------//



        //------// Methods \\-----------------------------------------------\\
        public object[] AllocateInputs()
        {
            return _defaultInvoker.AllocateInputs();
        }


        public object Invoke(object instance, object[] inputs, out object[] outputs)
        {
            // TODO: Perform authorization.

            if (ResourceAttribute.ProtectedResource || OAuthServiceHostFactory.AccessTokenRepository.GetToken(OAuthContext.Token) != null)
            {
                // Treat the resource as protected and enforce the Access Token requirement.

                IPrincipal principal = GeneratePrincipalForProtectedResource();

                if (principal == null)
                {
                    throw new OAuthException(OAuthContext, "A valid principal representing the user attempting to access the protected resource failed.", "Contact support.");
                }
                else
                {
                    var policies = new List<IAuthorizationPolicy> { new PrincipalAuthorizationPolicy(principal) };
                    var securityContext = new ServiceSecurityContext(policies.AsReadOnly());

                    if (RequestContext.RequestMessage.Properties.Security != null)
                    {
                        RequestContext.RequestMessage.Properties.Security.ServiceSecurityContext = securityContext;
                    }
                    else
                    {
                        RequestContext.RequestMessage.Properties.Security = new SecurityMessageProperty { ServiceSecurityContext = securityContext };
                    }
                }
            }
            else
            {
                // Treat the resource as unprotected and enforce the Request Token requirement only.

                try
                {
                    OAuthServiceHostFactory.OAuthProvider.AccessUnprotectedResourceRequest(OAuthContext);
                }
                catch (OAuthException oAuthException)
                {
                    ReplyToRequestWithError(oAuthException);
                    throw oAuthException;
                }
            }


            return _defaultInvoker.Invoke(instance, inputs, out outputs);
        }


        public IAsyncResult InvokeBegin(object instance, object[] inputs, AsyncCallback callback, object state)
        {
            return _defaultInvoker.InvokeBegin(instance, inputs, callback, state);
        }


        public object InvokeEnd(object instance, out object[] outputs, IAsyncResult result)
        {
            return _defaultInvoker.InvokeEnd(instance, out outputs, result);
        }


        protected virtual IPrincipal GeneratePrincipalForProtectedResource()
        {
            IPrincipal principal = null;

            OAuthServiceHostFactory.OAuthProvider.AccessProtectedResourceRequest(OAuthContext);
            AccessToken accessToken = OAuthServiceHostFactory.AccessTokenRepository.GetToken(OAuthContext.Token);


            if (accessToken == null)
            {
                OAuthException accessTokenNotFoundException = new OAuthException(OAuthContext, "The Access Token provided is not valid.", "Attempt to re-authenticate against the OAuth service.");
                ReplyToRequestWithError(accessTokenNotFoundException);

                throw accessTokenNotFoundException;
            }


            principal = new TokenPrincipal(new GenericIdentity(accessToken.UserName, "OAuth"), accessToken.Roles, accessToken);


            return principal;
        }



        protected virtual void ReplyToRequestWithError(OAuthException oAuthException)
        {
            FaultException fault = new FaultException(oAuthException.Report.ToString());

            Message reply = Message.CreateMessage(MessageVersion.None, fault.CreateMessageFault(), oAuthException.Report.ToString());
            reply.Properties[HttpResponseMessageProperty.Name] = new HttpResponseMessageProperty { StatusCode = HttpStatusCode.Forbidden, StatusDescription = oAuthException.Report.ToString() };

            //RequestContext.Reply(reply);
        }
        //------\\ Methods //-----------------------------------------------//
    }
}
