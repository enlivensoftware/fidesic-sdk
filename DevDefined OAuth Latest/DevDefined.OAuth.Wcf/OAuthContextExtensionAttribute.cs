﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.ServiceModel.Description;
using System.ServiceModel.Dispatcher;
using System.ServiceModel.Security;
using System.ServiceModel.Web;
using System.Text;

using DevDefined.OAuth.Framework;

namespace DevDefined.OAuth.Wcf
{
    [AttributeUsage(AttributeTargets.Interface | AttributeTargets.Class)]
    public class OAuthContextExtensionAttribute : Attribute, IContractBehavior
    {
        //------// Properties \\--------------------------------------------\\
        
        //------\\ Properties //--------------------------------------------//



        //------// Constructors \\------------------------------------------\\
        public OAuthContextExtensionAttribute()
        { }
        //------\\ Constructors //------------------------------------------//



        //------// Methods \\-----------------------------------------------\\
        public void AddBindingParameters(ContractDescription contractDescription, ServiceEndpoint endpoint, BindingParameterCollection bindingParameters)
        {
            /* Do nothing. */
        }


        public void ApplyClientBehavior(ContractDescription contractDescription, ServiceEndpoint endpoint, ClientRuntime clientRuntime)
        {
            /* Do nothing. */
        }


        public void ApplyDispatchBehavior(ContractDescription contractDescription, ServiceEndpoint endpoint, DispatchRuntime dispatchRuntime)
        {
            dispatchRuntime.InstanceContextInitializers.Add(new OAuthContextExtensionInitializer());
        }


        public void Validate(ContractDescription contractDescription, ServiceEndpoint endpoint)
        {
            /* Do nothing. */
        }
        //------\\ Methods //-----------------------------------------------//
    }
}
