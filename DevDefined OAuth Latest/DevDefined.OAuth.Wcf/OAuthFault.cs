﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Dispatcher;
using System.Text;

using DevDefined.OAuth.Framework;

namespace DevDefined.OAuth.Wcf
{
    [DataContract]
    public class OAuthFault
    {
        //------// Properties \\--------------------------------------------\\}
        [DataMember] public virtual string Problem { get; set; }
        [DataMember] public virtual string Advice { get; set; }
        //------\\ Properties //--------------------------------------------//



        //------// Constructors \\------------------------------------------\\
        public OAuthFault()
        { }


        public OAuthFault(string problem, string advice)
        {
            Problem = problem;
            Advice = advice;
        }
        //------\\ Constructors //------------------------------------------//



        //------// Methods \\-----------------------------------------------\\
        public override string ToString()
        {
            return "Problem: " + (Problem ?? String.Empty) + "\nAdvice: " + (Advice ?? String.Empty);
        }
        //------\\ Methods //-----------------------------------------------//
    }
}
