#region License

// The MIT License
//
// Copyright (c) 2006-2008 DevDefined Limited.
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

#endregion

using System;
using System.Net;
using DevDefined.OAuth.Framework;
using DevDefined.OAuth.Utility;
using System.Xml.Linq;
using System.Linq;
using System.ServiceModel;
using System.Xml;
using System.ServiceModel.Channels;
using System.IO;

namespace DevDefined.OAuth.Consumer
{
	public static class WebExceptionHelper
	{
        public static HttpStatusCode? LastResponseStatusCode { get; private set; }

        public static string LastResponseStatusDescription { get; private set; }

        public static HttpWebResponse LastResponse { get; private set; }

        public static bool TryWrapException(WebException error)
        {
            FaultException fault = ConvertWebExceptionIntoFault((WebException)error);
            if (fault != null)
                throw fault;
            return true;
        }
		/// <summary>never seems to catch oauth. no longer in use.
		/// Will attempt to wrap the exception, returning true if the exception was wrapped, or returning false if it was not (in which case
		/// the original exception should be thrown).
		/// </summary>
		/// <param name="requestContext"></param>
		/// <param name="webEx"></param>
		/// <param name="authException"></param>
		/// <returns><c>true</c>, if the authException should be throw, <c>false</c> if the original web exception should be thrown</returns>
		public static bool TryWrapException(IOAuthContext requestContext, WebException webEx, out OAuthException oAuthException, Action<string> responseBodyAction)
		{
			try
			{
				string content = webEx.Response.ReadToEnd();

				if (responseBodyAction != null)
				{
					responseBodyAction(content);
				}

                if (content.Contains(Parameters.OAuth_Problem))
                {
                    var report = new OAuthProblemReport(content);
                    oAuthException = new OAuthException(report.ProblemAdvice ?? report.Problem, webEx) { Context = requestContext, Report = report };
                    return true;
                }
			}
			catch
			{
			}
			oAuthException = new OAuthException();
			return false;
		}
        private static FaultException ConvertWebExceptionIntoFault(WebException wex)
        {
            if (wex.Response == null)
                return null;

            XmlDictionaryReader xdr = XmlDictionaryReader.CreateTextReader(
                wex.Response.GetResponseStream(),
                new XmlDictionaryReaderQuotas());

            Message msg = Message.CreateMessage(MessageVersion.None, "ParseFaultException", xdr);

            // If the start element of the message is "Fault" convert it into a FaultException
            //
            using (MessageBuffer msgBuffer = msg.CreateBufferedCopy(65536))
            using (Message msgCopy = msgBuffer.CreateMessage())
            using (XmlDictionaryReader reader = msgCopy.GetReaderAtBodyContents())
                if (reader.IsStartElement("Fault"))
                {
                    // Must make a copy for the converter
                    msg.Close();
                    msg = msgBuffer.CreateMessage();
                    return ConvertMessageToFault(msg);
                }

            return null;
        }

        private static FaultException ConvertMessageToFault(Message msg)
        {
            EnvelopeVersion ev = msg.Version.Envelope;
            var fault = MessageFault.CreateFault(msg, 65536);
            
            if (fault.HasDetail)
            {
                string faultName = fault.GetReaderAtDetailContents().Name;
                switch (faultName)
                {
                    case "ExceptionDetail": // handle the default WCF generated fault 
                        ExceptionDetail exDetail = fault.GetDetail<ExceptionDetail>();
                        return new FaultException<ExceptionDetail>(exDetail, fault.Reason, fault.Code);
                    case "string": // handle the default WCF generated fault 
                        var stringDetail = fault.GetDetail<string>();
                        return new FaultException<string>(stringDetail, fault.Reason, fault.Code);

                    default:
                        return new FaultException(fault.Reason, fault.Code);
                }
            }
            else if (fault.Reason != null && fault.Reason.ToString().Length > 0)
            {
                return new FaultException(fault.Reason, fault.Code);
            }
            return null;
        }
        private static string HandleWebException(WebException ex)
        {
            if (ex.Response is HttpWebResponse &&
                ex.Response != null)
            {
                SetLastResponse(ex.Response);

                using (
                    var reader =
                        new StreamReader(ex.Response.GetResponseStream()))
                {
                    var result = reader.ReadToEnd();
                    return result;
                }
            }

            throw ex;
        }
        private static void SetLastResponse(WebResponse response)
        {
            if (response is HttpWebResponse)
            {
                var httpResponse = (HttpWebResponse)response;
                LastResponseStatusCode = httpResponse.StatusCode;
                LastResponseStatusDescription = httpResponse.StatusDescription;
                LastResponse = httpResponse;
            }
        }
	}
}